# Kops

<https://kops.sigs.k8s.io/>

O Kops simplifica significativamente a configuração e o gerenciamento do cluster Kubernetes em comparação com a configuração manual de nós mestre e de trabalho. Ele gerencia Route53, Grupos de AutoScaling, ELBs para o servidor de API, grupos de segurança, inicialização mestre, inicialização de nó e atualizações contínuas para seu cluster. Como o kops é uma ferramenta de código aberto, seu uso é totalmente gratuito, mas você é responsável por pagar e manter a infraestrutura extra criada pelo kops para gerenciar seu cluster Kubernetes.

O kops não cria um cluser só na AWS mas tb em várias outras cloud como GCP, OpenStack Digital Ocean, Azure, Hetzner Spot Ocean.

>O Kops na verdade trabalha com o terraforms por baixo do capô. É possível inclusive exportar o código terraform que ele cria.

A Documentação oficial pode detalhar melhor <https://kops.sigs.k8s.io/>

## Instalação

O kops nada mais é do que um CLI <https://kops.sigs.k8s.io/getting_started/install/>.

```kops
curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops
sudo mv kops /usr/local/bin/kops
```

## Requisitos para aws

<https://kops.sigs.k8s.io/getting_started/aws/>

A primeira coisa necessária é ter um usuário com na aws com as permissões abaixo, mas geralmente vc vai fazer isso com um usuário cuja as permissões costumam ser administrador, mas caso contrário essas são as permissões necessárias.

- AmazonEC2FullAccess
- AmazonRoute53FullAccess
- AmazonS3FullAccess
- IAMFullAccess
- AmazonVPCFullAccess
- AmazonSQSFullAccess
- AmazonEventBridgeFullAccess

É necessário para o uso do kops exportar as variavies abaixo no seu terminal

```bash
export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXX
```

Um outro detalhe importante é ter um local para guardar o estado do cluster, assim como acontece com o terraforms.

Crie um bucket s3 ou use um que já existe, mas garante que esteja versionado

```bash
aws s3api create-bucket --bucket kops-states-company --region us-east-1 
aws s3api put-bucket-versioning --bucket kops-states-company --region us-east-1 --versioning-configuration Status=Enabled
```

Para não ter que ficar passando a flag --state e passar pode simplesmente exportar a variavel

```bash
export KOPS_STATE_STORE=s3://kops-states-company
```

## Vantagens e desvantagens

 A Kops gerenciará a maioria dos recursos da AWS necessários para executar um cluster Kubernetes e trabalhará com uma VPC nova ou existente. Ao contrário do EKS, o kops também criará seus nós mestres como instâncias do EC2, e você poderá acessar esses nós diretamente e fazer modificações. Com acesso aos nós mestres, você pode escolher qual camada de rede usar, escolher o tamanho das instâncias mestres e monitorar diretamente os nós mestres. Você também tem a opção de configurar um cluster com apenas um único mestre, o que pode ser desejável para ambientes de desenvolvimento e teste em que a alta disponibilidade não é um requisito. O Kops também suporta a geração de configuração do terraform para seus recursos em vez de criá-los diretamente, o que é um bom recurso se você usar o terraform.

 >O Kops é a maneira mais rápida de se ter um cluster totalmente funcional para produção.
