# Melhores Práticas

## Arquitetura

- Pense em multilocação, isolamento para ambiente diferente ou carga de trabalho diferente
  - Isolamento no nível da conta usando a organização da AWS
  - Isolamento na camada de rede, ou seja. VPC diferente e cluster diferente
  - Use grupos de nós diferentes (pool de nós) para finalidade/categoria diferente, por exemplo, crie grupos de nós dedicados para ferramentas operacionais, como ferramenta CI/CD, ferramenta de monitoramento, sistema de registro centralizado.
  - Namespace separado para carga de trabalho diferente

## Confiabilidade | [Princípios](https://docs.aws.amazon.com/wellarchitected/latest/reliability-pillar/design-principles.html)

- Recomendado usar VPC dedicada para EKS
  - [Arquitetura modular e escalável do Amazon EKS](https://docs.aws.amazon.com/quickstart/latest/amazon-eks-architecture/welcome.html)
  - Planeje sua VPC e CIDR de sub-rede, evite a complexidade de usar [vários CIDRs em uma VPC](https://aws.amazon.com/premiumsupport/knowledge-center/eks-multiple-cidr-ranges/) e [rede personalizada CNI](https://docs.aws.amazon.com/eks/latest/userguide/cni-custom-network.html)
- Entenda e verifique [Cota de serviço do EKS/Fargate](https://docs.aws.amazon.com/eks/latest/userguide/service-quotas.html) e outros serviços relacionados
- Implemente o [Cluster Autoscaler](https://aws.github.io/aws-eks-best-practices/cluster-autoscaling/) para ajustar automaticamente o tamanho de um cluster EKS para cima e para baixo com base nas demandas de agendamento.
- Considere o número de nós do trabalhador e a degradação do serviço se houver falha de nó/AZ.
  - Cuide do RTO.
  - Considere ter um nó de buffer.
- Considere não escolher um tipo de instância muito grande para reduzir o raio de explosão.
- Habilite o Horizontal Pod Autoscaler para usar a utilização da CPU ou métricas personalizadas para expandir os pods.
- Use a infraestrutura como código (arquivos e modelos de manifesto do Kubernetes para provisionar clusters/nós do EKS, etc.)
- Use [várias AZs](https://aws.amazon.com/blogs/containers/amazon-eks-cluster-multi-zone-auto-scaling-groups/). Distribua as réplicas do aplicativo para diferentes zonas de disponibilidade do nó do trabalhador para redundância
  - Cuidado com seus pods persistentes que usam EBS como PersistentVolume. Use anotação, por exemplo, `topology.kubernetes.io/zone=us-east-1c`
- Nós de trabalho altamente disponíveis e escaláveis ​​usando grupos de Auto Scaling, use grupo de nós
  - Considere usar [Grupos de nós gerenciados](https://docs.aws.amazon.com/eks/latest/userguide/managed-node-groups.html) para fácil configuração e alta disponibilidade de nós durante atualizações ou terminação
  - Considere usar o Fargate para não precisar gerenciar nós de trabalho. Mas cuidado com a limitação do Fargate.
- Considere separar o Node Group para suas funções de aplicativo e utilitário, por exemplo. Banco de dados de registro, plano de controle de malha de serviço
- Implante [aws-node-termination-handler](https://github.com/aws/aws-node-termination-handler). Ele detecta se o nó ficará indisponível/encerrado, como Interrupção de ponto, em seguida, certifique-se de que nenhum novo trabalho seja agendado lá e, em seguida, drene-o, removendo qualquer trabalho existente. [Tutorial](https://eksworkshop.com/beginner/150_spotworkers/deployhandler/) | [Comunicado](https://aws.amazon.com/about-aws/whats-new/2019/11/aws-supports-automated-draining-for-spot-instance-nodes-on-kubernetes/)
- Configure [Orçamentos de interrupção de pod](https://kubernetes.io/docs/concepts/workloads/pods/disruptions/) (PDBs) para limitar o número de pods de um aplicativo replicado que estão inativos simultaneamente de [interrupções voluntárias]( https://kubernetes.io/docs/concepts/workloads/pods/disruptions/#voluntary-and-involuntary-disruptions) por exemplo. durante a atualização, implantação contínua e [outro caso de uso](https://kubernetes.io/docs/tasks/run-application/configure-pdb/#think-about-how-your-application-reacts-to-disruptions).
- Use o AWS Backup para fazer backup de EFS e EBS
- Use o EFS para classe de armazenamento: o uso do EFS não requer o pré-provisionamento da capacidade e permite migrações de pod mais eficientes entre nós de trabalho (removendo armazenamento anexado ao nó)
- Instale o [Node Problem Detector](https://github.com/kubernetes/node-problem-detector) para fornecer dados acionáveis ​​para curar clusters.
- Evite erros de configuração, como o uso de antiafinidade, que faz com que o pod não possa ser reprogramado devido a falha do nó.
- Use sondas de vivacidade e prontidão
- Pratique a engenharia do caos, use [ferramentas disponíveis para automatizar](https://landscape.cncf.io/category=chaos-engineering&format=card-mode&grouping=category).
  - Mate pods aleatoriamente durante o teste
- Implementar o gerenciamento de falhas no nível de microsserviço, por exemplo, padrão de disjuntor, controlar e limitar chamadas de repetição (recuo exponencial), limitação, tornar os serviços sem estado sempre que possível
- Pratique como fazer upgrade do cluster e dos nós do trabalhador para a nova versão.
  - Pratique como drenar os nós do trabalhador.
- Pratique a engenharia do caos
- Utilizar ferramentas de CI/CD, automatizar e ter fluxo de processos (aprovação/revisão) para mudanças de infraestrutura. Considere implementar Gitops.
- Use solução multi-AZ para volume persistente, por exemplo. [Thanos+S3 for Prometheus](https://aws.amazon.com/blogs/opensource/improving-ha-and-long-term-storage-for-prometheus-using-thanos-on-eks-with-s3/ )

## Eficiência de Desempenho | [Princípios](https://docs.aws.amazon.com/wellarchitected/latest/performance-efficiency-pillar/design-principles.html)

- Informe o suporte da AWS se precisar pré-escalar o Plano de Controle (Nó mestre e Etcd) em caso de incremento de carga repentino
- Escolha o [tipo de instância do EC2](https://aws.amazon.com/ec2/instance-types/) correto para seu nó do trabalhador.
  - Entenda os [prós e contras](https://learnk8s.io/kubernetes-node-size) de usar muitas instâncias de nós pequenos ou poucas instâncias de nós grandes. Considere a sobrecarga do SO, o tempo necessário para extrair a imagem em uma nova instância quando ela é dimensionada, a sobrecarga do kubelet, a sobrecarga do pod do sistema etc.  
  - Entenda a limitação de densidade de pods ([número máximo de pods](https://github.com/awslabs/amazon-eks-ami/blob/master/files/eni-max-pods.txt) compatível com cada tipo de instância)
- Use grupos de nós single-AZ, se necessário. Normalmente, uma das melhores práticas é executar um microsserviço no Multi-AZ para disponibilidade, mas para algumas cargas de trabalho (como Spark) que precisam de latência de microssegundos, com alta operação de E/S de rede e transientes, é feito o envio para use single-AZ.
- Entenda a [limitação de desempenho do Fargate](https://github.com/aws/containers-roadmap/issues/715). Faça o teste de carga antes de ir para a produção.
- Certifique-se de que seu pod solicite os recursos necessários. Definir recursos `request` e `limit` como CPU, memória
- Detectar gargalo/latência em um microsserviço com o X-Ray ou [outros produtos de rastreamento/APM](https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category)
- Escolha o back-end de armazenamento certo. Use o [Amazon FSx for Lustre](https://aws.amazon.com/fsx/lustre/) e seu [CSI Driver](https://github.com/kubernetes-sigs/aws-fsx-csi-driver) se o seu contêiner persistente precisar de um sistema de arquivos de alto desempenho
- Monitore o consumo de recursos de pods e nós e o afunilamento de tecnologia. Você pode usar o CloudWatch, [CloudWatch Container Insight](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ContainerInsights.html) ou [outros produtos](<https://landscape.cncf.io/> category=monitoring&format=card-mode&grouping=category)
- Se necessário, inicie instâncias (nós de trabalho) em [Placement Groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html) para aproveitar a baixa latência sem diminuir a velocidade. Você pode usar este [modelo do CloudFormation](<https://github.com/aws-samples/aws-eks-deep-learning-benchmark/blob/master/blog-post-sample/eks_cluster/amazon-eks-nodegroup-placementgroup> .yaml) para adicionar novos grupos de nós com conectividade sem bloqueio, sem excesso de assinaturas e totalmente bi-seccional.
- Se necessário, configure a [política de gerenciamento de CPU do Kubernetes como 'static'](https://kubernetes.io/docs/tasks/administer-cluster/cpu-management-policies/#cpu-management-policies) para alguns pods que precisam CPUs exclusivas

## Otimização de custos

- Minimize os recursos desperdiçados (não utilizados) ao usar o EC2 como nó do trabalhador.
  - Escolha o tipo de instância do EC2 correto e use o dimensionamento automático de cluster.
  - Considere usar Fargate
  - Considere usar uma ferramenta como [kube-resource-report](https://github.com/hjacobs/kube-resource-report) para visualizar o custo de folga e dimensionar corretamente as solicitações para os contêineres em um pod.
- Use instâncias spot ou misture sob demanda e spot usando [Spot Fleet](<https://aws.amazon.com/blogs/aws/ec2-fleet-manage-thousands-of-on-demand-and-spot>- instance-with-one-request/). Considere usar instâncias spot para ambiente de teste/preparação.
- Usar instância reservada ou planos de economia
- Use grupos de nós de AZ único para carga de trabalho com alta operação de E/S de rede (por exemplo, Spark) para reduzir a comunicação entre AZ. Mas, por favor, valide se a execução do Single-AZ não comprometeria a disponibilidade do seu sistema.
- Considere os serviços gerenciados para ferramentas de suporte, como monitoramento, malha de serviço, registro cetralizado, para reduzir o esforço e o custo de sua equipe
- Marque todos os recursos da AWS quando possível e use rótulos para marcar os recursos do Kubernetes para que você possa analisar facilmente o custo.
- Considere usar Kubernetes de autogerenciamento (não usando EKS) para cluster sem HA. Você pode configurar usando [Kops](https://github.com/kubernetes/kops) para seu pequeno cluster k8s.
- Use Node Affinities usando nodeSelector para pod que requer um tipo de instância EC2 específico.

## Operação: [Princípios](https://docs.aws.amazon.com/wellarchitected/latest/operational-excellence-pillar/design-principles.html)

- Use a ferramenta IaC para provisionar o cluster EKS, como
  - CloudFormation.
    - [Implantação de referência](https://aws.amazon.com/quickstart/architecture/amazon-eks/)
    - [Implantar nós de autogerenciamento](https://amazon-eks.s3.us-west-2.amazonaws.com/cloudformation/2020-08-12/amazon-eks-nodegroup.yaml)
  - [Terraform](https://learn.hashicorp.com/tutorials/terraform/eks)
  - [Eksctl](https://eksctl.io/)
  - [AWS CDK](https://medium.com/faun/spawning-an-autoscaling-eks-cluster-52977aa8b467)
- Considere usar o gerenciador de pacotes como [Helm](https://docs.aws.amazon.com/eks/latest/userguide/helm.html) para ajudá-lo a instalar e gerenciar aplicativos.
- Automatize o gerenciamento de cluster e a implantação de aplicativos usando [GitOps](https://www.weave.works/technologies/gitops/). Você pode usar ferramentas como [Flux](https://fluxcd.io/) ou [outros](<https://github.com/weaveworks/awesome-gitops> | [workshop](<https://www.eksworkshop.com> /intermediário/260_weave_flux/)
- Use [ferramentas de CI/CD](https://landscape.cncf.io/category=continuous-integration-delivery&format=card-mode&grouping=category)
- Pratique fazer a atualização do EKS (rolling update), crie o runbook.
      - [GitHub - hellofresh/eks-rolling-update: EKS Rolling Update é um utilitário para atualizar a configuração de inicialização de nós de trabalho em um cluster EKS.](https://github.com/hellofresh/eks-rolling-update)
      - [Open Sourcing EKS Rolling Update: uma ferramenta para atualizar clusters do Amazon EKS](<https://engineering.hellofresh.com/open-sourcing-eks-rolling-update-a-tool-for-updating-amazon-eks-clusters> -5cef5b497a95)
- Monitoramento
  - Compreenda a saúde da sua carga de trabalho. Defina KPI/SLO e métricas/SLI e depois monitore através de seu painel e configure alertas
  - Entenda sua Saúde Operacional. Defina KPI e métricas como tempo médio para detectar um incidente (MTTD) e tempo médio para recuperação (MTTR) de um incidente.
  - Use o monitoramento detalhado usando o [Container Insights for EKS](https://docs.aws.amazon.com/en_pv/AmazonCloudWatch/latest/monitoring/Container-Insights-setup-EKS-quickstart.html) para detalhar o serviço, desempenho de cápsulas. Ele também fornece informações de diagnóstico e considera a visualização de métricas adicionais e níveis adicionais de granularidade quando ocorre um problema.
  - Monitorar [métricas do plano de controle usando o Prometheus](https://docs.aws.amazon.com/en_pv/eks/latest/userguide/prometheus.html)
  - [Monitoramento usando Prometheus & Grafana](https://www.eksworkshop.com/intermediate/240_monitoring/)
- Exploração madeireira
  - Considere o mecanismo DaemonSet vs Sidecar. DaemonSet é o nó do trabalhador EC2 preferível, mas você precisa usar o padrão Sidecar para Fargate.
  - [Registro do plano de controle](https://docs.aws.amazon.com/en_pv/eks/latest/userguide/control-plane-logs.html)
  - Você pode usar [pilha EFK](https://www.eksworkshop.com/intermediate/230_logging/) ou [FluentBit, Kinesis Data Firehouse, S3 e Athena](<https://aws.amazon.com/blogs/opensource> /centralized-container-logging-fluent-bit/)
- Rastreamento
  - Monitore a transação de grau fino usando o X-Ray [eksworkshop.com](https://eksworkshop.com/x-ray/). Também é bom monitorar a implantação do blu-green. [Outras ferramentas](https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category)
- Pratique a Engenharia do Caos, você pode automatizar usando [algumas ferramentas](https://landscape.cncf.io/category=chaos-engineering&format=card-mode&grouping=category)
- Configuração
  - Appmesh + EKS demonstração / laboratório: [GitHub - PaulMaddox/aws-appmesh-helm: AWS App Mesh ❤ K8s](https://github.com/PaulMaddox/aws-appmesh-helm)
  - Mapa da Nuvem AWS:
    - [AWS Cloud Map: Crie e mantenha facilmente mapas personalizados de seus aplicativos | Blog de notícias da AWS](https://aws.amazon.com/blogs/aws/aws-cloud-map-easily-create-and-maintain-custom-maps-of-your-applications/)
    - AWS CloudMap + Consul:
      - [AWS re:Invent 2018: NOVO LANÇAMENTO! Apresentando o AWS Cloud Map (CON366) - YouTube](https://youtu.be/fMGd9IUaotE?t=1982)
      - [Sincronização do Mapa da Nuvem AWS e do Catálogo do Cônsul - YouTube](https://www.youtube.com/watch?v=203K6JIpYcI)
      - [Gerenciando implantações de microsserviços na AWS com o HashiCorp Consul - YouTube](https://www.youtube.com/watch?v=gZLbF26hBps)

## Segurança | [Princípios](https://docs.aws.amazon.com/wellarchitected/latest/security-pillar/design-principles.html)

- Compreender o [modelo de responsabilidade compartilhada](https://aws.amazon.com/blogs/containers/introducing-cis-amazon-eks-benchmark/) para diferentes modos de operação do EKS (nós autogerenciados, grupo de nós gerenciados, Fargate )
- [Práticas recomendadas de segurança da AWS para EKS](https://aws.github.io/aws-eks-best-practices/)
- Integrando segurança em seu pipeline de contêineres | [workshop](https://container-devsecops.awssecworkshops.com/)
- Use [rede personalizada CNI](https://docs.aws.amazon.com/eks/latest/userguide/cni-custom-network.html), se seu pod precisar ter um grupo de segurança diferente com seus nós ou pods para ser colocado em sub-redes privadas, mas o nó está, na verdade, em sub-rede pública.
- [Log da API Cloudtrail EKS](https://docs.aws.amazon.com/en_pv/eks/latest/userguide/logging-using-cloudtrail.html)
  - Considere habilitar a entrega contínua de eventos do CloudTrail para um bucket do Amazon S3 [criando uma trilha](<https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-create-and-update-a-trail> .html) para registrar eventos < últimos 90 dias
- Use a política de rede para o tráfego Leste-Oeste: [Calico](https://docs.aws.amazon.com/en_pv/eks/latest/userguide/calico.html)
- Use [grupos de segurança para pods](https://aws.amazon.com/blogs/containers/introducing-security-groups-for-pods/) apenas para K8s > v1.17. Veja algumas [considerações](https://docs.aws.amazon.com/eks/latest/userguide/security-groups-for-pods.html)
- [Apresentando funções de IAM refinadas para contas de serviço | Blog de código aberto da AWS](https://aws.amazon.com/blogs/opensource/introducing-fine-grained-iam-roles-service-accounts/)
- [Use as chaves do AWS Key Management Service (KMS) para fornecer criptografia de envelope de segredos do Kubernetes armazenados](<https://aws.amazon.com/blogs/containers/using-eks-encryption-provider-support-for-defense-in> -profundidade/)

Packer for AMI build : [Configuração do Packer para criar uma AMI EKS personalizada](https://github.com/awslabs/amazon-eks-ami)
