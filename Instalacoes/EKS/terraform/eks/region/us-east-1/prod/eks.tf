module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.28"

  cluster_name                    = local.cluster_name
  cluster_version                 = var.k8s_version
  cluster_endpoint_private_access = var.cluster_endpoint_private_access
  cluster_endpoint_public_access  = var.cluster_endpoint_public_access

  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs

  cluster_enabled_log_types = var.cluster_enabled_log_types

  cluster_ip_family = "ipv4"

  vpc_id                   = local.vpc_id
  subnet_ids               = local.private_subnet_ids
  control_plane_subnet_ids = local.public_subnet_ids

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts        = "OVERWRITE"
      service_account_role_arn = module.vpc_cni_irsa.iam_role_arn
    }
  }

  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.eks.arn
    resources        = ["secrets"]
  }]

  cluster_tags = var.cluster_tags

  # Habilita o resource kubernetes_config_map
  # O recurso fornece mecanismos para injetar dados de configuração em contêineres, mantendo os contêineres independentes do Kubernetes. O Config Map pode ser usado para armazenar informações refinadas, como propriedades individuais ou informações refinadas, como arquivos de configuração inteiros ou blobs JSON.
  manage_aws_auth_configmap = true

  # Este bloco vai adicionar regras ao eks.
  cluster_security_group_additional_rules = {
    # Regra para todas as portas efemeras
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  # Regras para o segurity groups do workers
  node_security_group_ntp_ipv4_cidr_block = ["169.254.169.123/32"]
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  #
  # esta parte é muito importante entender bem
  eks_managed_node_group_defaults = {
    ami_type               = "AL2_x86_64"
    instance_types_default = ["t3.medium"]

    # attach_cluster_primary_security_group = true
    # vpc_security_group_ids                = [aws_security_group.additional.id]

    iam_role_attach_cni_policy   = true
    iam_role_additional_policies = [aws_iam_policy.additional_ecr_access.id]
  }

  # Este bloco utilizará o resource https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group
  eks_managed_node_groups = {
    infrastructure = {
      name            = "infrastructure-ng"
      use_name_prefix = true

      subnet_ids = local.private_subnet_ids

      min_size     = var.min_size_default
      max_size     = var.max_size_default
      desired_size = var.desired_size_default

      ami_id                     = data.aws_ami.eks_default.image_id
      enable_bootstrap_user_data = true
      # Pode ser passado alguns parâmetros aqui também. Esses ajustes foram retirados inclusive do 
      # https://kubernetes.io/docs/tasks/administer-cluster/reserve-compute-resources
      bootstrap_extra_args = join(" ", [
        "--container-runtime containerd -kubelet-extra-args",
        "--kube-reserved",
        "'cpu='${var.kubelet_reserve_cpu}',memory='${var.kubelet_reserve_memory}',ephemeral-storage='${var.kubelet_reserve_storage}",
        "--system-reserved",
        "'cpu='${var.system_reserve_cpu}',memory='${var.system_reserve_memory}',ephemeral-storage='${var.system_reserve_storage}",
        "--eviction-hard",
        "'memory.available<'${var.minimal_memory_available}',nodefs.available<'${var.minimal_storage_available}"
      ])

      pre_bootstrap_user_data = <<-EOT
      export CONTAINER_RUNTIME="containerd"
      export USE_MAX_PODS=false
      EOT

      post_bootstrap_user_data = <<-EOT
      echo "you are free little kubelet!"
      EOT

      capacity_type          = var.capacity_type_default
      force_update_version   = true
      instance_types_default = var.instance_types_default
      labels = {
        name = "infrastructure"
        type = "managed"
      }

      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }

      description = "EKS managed node infraestructure for general purpose"

      ebs_optimized           = true
      vpc_security_group_ids  = [aws_security_group.additional.id]
      disable_api_termination = false
      enable_monitoring       = true

      block_device_mappings = {
        xvda = {
          device_name = "/dev/xvda"
          ebs = {
            volume_size           = 30
            volume_type           = "gp3"
            iops                  = 3000
            throughput            = 150
            encrypted             = true
            kms_key_id            = aws_kms_key.ebs.arn
            delete_on_termination = true
          }
        }
      }

      create_iam_role          = true
      iam_role_name            = "eks-managed-ng-infrastructure"
      iam_role_use_name_prefix = false
      iam_role_description     = "EKS managed node group default role"
      iam_role_tags = {
        Purpose = "Protector of the kubelet"
      }
      iam_role_additional_policies = [
        "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      ]

      create_security_group          = true
      security_group_name            = "eks-managed-ng-infrastructure"
      security_group_use_name_prefix = false
      security_group_description     = "EKS managed node group complete example security group"

      security_group_tags = {
        Purpose = "Protector of the kubelet"
      }
    }
  }
}
