region      = "us-east-1"
environment = "production"
k8s_version = "1.23"
cluster_tags = {
  region = "us-east-1"
  cloud  = "aws"
  type   = "eks"
}

cluster_endpoint_private_access = true
cluster_endpoint_public_access  = true
cluster_enabled_log_types       = ["audit", "authenticator"]
instance_types_default                 = ["t3.medium", "t3.small"]
capacity_type_default           = "ON_DEMAND" # or SPOT
min_size_default                = 1
max_size_default                = 5
desired_size_default            = 1
kubelet_reserve_cpu             = "100m"
kubelet_reserve_memory          = "200Mi"
kubelet_reserve_storage         = "1Gi"
system_reserve_cpu              = "100m"
system_reserve_memory           = "200Mi"
system_reserve_storage          = "1Gi"
minimal_memory_available        = "200Mi"
minimal_storage_available       = "10%"

private_key_file        = "./files/key"
public_key_file         = "./files/key.pub"

