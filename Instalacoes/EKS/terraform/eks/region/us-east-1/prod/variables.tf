
variable "region" {
  description = "Region to create vpc and subnets"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}

variable "k8s_version" {
  description = "Kubernetes Version"
  type        = string
}

variable "cluster_endpoint_private_access" {
  description = "Acess private nodes"
  type        = bool
  default     = false
}

variable "cluster_endpoint_public_access" {
  description = "Acess public nodes"
  type        = bool
  default     = true
}

variable "cluster_enabled_log_types" {
  description = "Types of logs to control plane. Can be api, audit, authenticator, controllerManager and scheduler"
  type        = list(string)
  default     = []
}

variable "node_managed_instance_types" {
  description = "Map of EKS managed node group default configurations"
  type        = list(string)
  default     = ["t3.medium"]
}

variable "cluster_tags" {
  description = "A map of tags for the cluster"
  type        = map(string)
  default     = {}
}

variable "cluster_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "ami_type_default" {
  description = "Default ami for use in node groups if not declared"
  type        = string
  default     = "AL2_x86_64"
}

variable "instance_types_default" {
  description = "Default ami for use in node groups if not declared"
  type        = list(string)
  default     = ["t3.medium"]
}

variable "min_size_default" {
  description = "Minimal number of nodes to default group"
  type        = number
  default     = 1
}

variable "max_size_default" {
  description = "Maximum number of nodes to default group"
  type        = number
  default     = 5
}

variable "desired_size_default" {
  description = "Desidred number of nodes to default group"
  type        = number
  default     = 1
}

variable "capacity_type_default" {
  description = "type of nodes can be ON_DEMAND or SPOT"
  type        = string 
  default     = "ON_DEMAND"
}

variable "kubelet_reserve_cpu" {
  description = "Reserved cpu in host for kubelet"
  type        = string 
  default     = "200m"
}

variable "kubelet_reserve_memory" {
  description = "Reserved memory in host for kubelet"
  type        = string 
  default     = "200Mi"
}

variable "kubelet_reserve_storage" {
  description = "Reserved storage in host for kubelet"
  type        = string 
  default     = "1Gi"
}

variable "system_reserve_cpu" {
  description = "Reserved cpu in host for system services"
  type        = string 
  default     = "200m"
}

variable "system_reserve_memory" {
  description = "Reserved memory in host for system services"
  type        = string 
  default     = "200Mi"
}

variable "system_reserve_storage" {
  description = "Reserved storage in host for system services"
  type        = string 
  default     = "1Gi"
}

variable "minimal_memory_available" {
  description = "Minimal size of memory available in host to protect Thresholds"
  type        = string 
  default     = "200Mi"
}

variable "minimal_storage_available" {
  description = "Minimal size of storage available in host to protect Thresholds"
  type        = string 
  default     = "10%"
}

variable "private_key_file" {
  type        = string
  description = "Path para a chave privada que permitirá o acesso via ssh aos nodes"
  default     = "~/.ssh/id_rsa"
}

variable "public_key_file" {
  type        = string
  description = "Path para a chave publica que ficará autorizada dentro dos nodes"
  default     = "~/.ssh/id_rsa.pub"
}