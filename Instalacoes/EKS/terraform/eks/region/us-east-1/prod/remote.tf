data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket   = "company-tfstate"
    key      = "network/${var.region}/${var.environment}/terraform.tfstate"
    region   = "us-east-1"
  }
}