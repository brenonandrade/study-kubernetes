
locals {
  team         = "devops"
  project_name = "eks"

  vpc_id         = data.terraform_remote_state.network.outputs.vpc_id
  vpc_cidr_block = data.terraform_remote_state.network.outputs.vpc_cidr_block

  public_subnet_ids   = data.terraform_remote_state.network.outputs.public_subnet_ids
  private_subnet_ids  = data.terraform_remote_state.network.outputs.private_subnet_ids
  database_subnet_ids = data.terraform_remote_state.network.outputs.database_subnet_ids

  cluster_name = data.terraform_remote_state.network.outputs.cluster_name
  
  tags = {
    Team             = local.team
    Env              = var.environment
    System           = local.project_name
    CreationOrigin   = "terraform"
    repositoryOrigin = "network"
    State            = "eks/us-east-1/production/terraform.tfstate"
  }

  cluster_autoscaler_label_tags = merge([
    for name, group in module.eks.eks_managed_node_groups : {
      for label_name, label_value in coalesce(group.node_group_labels, {}) : "${name}|label|${label_name}" => {
        autoscaling_group = group.node_group_autoscaling_group_names[0],
        key               = "k8s.io/cluster-autoscaler/node-template/label/${label_name}",
        value             = label_value,
      }
    }
  ]...)

  cluster_autoscaler_taint_tags = merge([
    for name, group in module.eks.eks_managed_node_groups : {
      for taint in coalesce(group.node_group_taints, []) : "${name}|taint|${taint.key}" => {
        autoscaling_group = group.node_group_autoscaling_group_names[0],
        key               = "k8s.io/cluster-autoscaler/node-template/taint/${taint.key}"
        value             = "${taint.value}:${taint.effect}"
      }
    }
  ]...)

  cluster_autoscaler_asg_tags = merge(local.cluster_autoscaler_label_tags, local.cluster_autoscaler_taint_tags)
}