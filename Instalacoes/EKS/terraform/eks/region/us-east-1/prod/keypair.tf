resource "aws_key_pair" "sshkey" {
  key_name_prefix = local.cluster_name
  public_key      = file(var.public_key_file)
}