
variable "region" {
  description = "Region to create vpc and subnets"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}

variable "cidr_prefix" {
  description = "Prefix to cidr /16. Prefix.0.0/16"
  type        = string
}

variable "single_nat_gateway" {
  description = "Create a nat gateway for each subnet"
  type        = bool
  default     = false
}
