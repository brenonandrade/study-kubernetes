terraform {
  backend "s3" {
    bucket         = "company-tfstate"
    key            = "network/us-east-1/production/terraform.tfstate"
    encrypt        = true
    region         = "us-east-1"
    dynamodb_table = "company-dynamodb"
  }
}