
output "vpc_arn" {
  description = "arn of the vpc"
  value       = module.vpc.vpc_arn
}

output "vpc_id" {
  description = "ID of the vpc"
  value       = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  description = "CIDR block of the vpc"
  value       = module.vpc.vpc_cidr_block
}

output "azs" {
  description = "list of azs of the network"
  value       = toset(local.azs)
}

output "database_subnet_arns" {
  description = "arn of the database subnets"
  value       = toset(module.vpc.database_subnet_arns)
}

output "database_subnet_ids" {
  description = "IDs of the database subnets"
  value       = toset(module.vpc.database_subnets)
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value       = toset(module.vpc.database_subnets_cidr_blocks)
}

output "database_route_table_ids" {
  description = "ids of the route tables from database subnet"
  value       = toset(module.vpc.database_route_table_ids)
}

output "public_subnet_arns" {
  description = "arn of the database subnets"
  value       = toset(module.vpc.public_subnet_arns)
}

output "public_subnet_ids" {
  description = "IDs of the database subnets"
  value       = toset(module.vpc.public_subnets)
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = toset(module.vpc.public_subnets_cidr_blocks)
}

output "public_route_table_ids" {
  description = "ids of the route tables from public subnet"
  value       = toset(module.vpc.public_route_table_ids)
}

output "private_subnet_arns" {
  description = "arn of the database subnets"
  value       = toset(module.vpc.private_subnet_arns)
}

output "private_subnet_ids" {
  description = "IDs of the database subnets"
  value       = toset(module.vpc.private_subnets)
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = toset(module.vpc.private_subnets_cidr_blocks)
}

output "private_route_table_ids" {
  description = "ids of the route tables from private subnet"
  value       = toset(module.vpc.private_route_table_ids)
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = toset(module.vpc.nat_public_ips)
}

output "cluster_name" {
  description = "Kubernetes cluster name"
  value       = local.cluster_name
}