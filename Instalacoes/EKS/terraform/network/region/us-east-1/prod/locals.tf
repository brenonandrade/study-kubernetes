
locals {
  team         = "devops"
  project_name = "network"

  cluster_name = "${var.region}-${var.environment}-cluster"

  vpc_cidr    = "${var.cidr_prefix}.0.0/16"

  azs                  = ["${var.region}a", "${var.region}b", "${var.region}c"]

  private_subnet_cidrs = [
    "${var.cidr_prefix}.0.0/19", 
    "${var.cidr_prefix}.32.0/19", 
    "${var.cidr_prefix}.64.0/19"
  ]

  database_cidrs       = [
    "${var.cidr_prefix}.172.0/24", 
    "${var.cidr_prefix}.173.0/24", 
    "${var.cidr_prefix}.174.0/24"
  ]
  public_subnet_cidrs  = [
    "${var.cidr_prefix}.181.0/24", 
    "${var.cidr_prefix}.182.0/24", 
    "${var.cidr_prefix}.183.0/24"
  ]

  network_resources = toset(
    concat(
      [for resource in module.vpc.database_subnet_arns : resource],
      [for resource in module.vpc.private_subnet_arns : resource],
      [for resource in module.vpc.public_subnet_arns : resource]
    )
  )

  tags = {
    Team             = local.team
    Env              = var.environment
    System           = local.project_name
    CreationOrigin   = "terraform"
    repositoryOrigin = "network"
    State            = "network/us-east-1/production/terraform.tfstate"
  }
}
