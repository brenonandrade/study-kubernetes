module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"

  name = var.environment
  cidr = local.vpc_cidr

  azs             = local.azs
  public_subnets  = local.public_subnet_cidrs
  private_subnets = local.private_subnet_cidrs
  #   database_subnets        = local.database_subnet_cidrs

  map_public_ip_on_launch = true

  enable_dns_hostnames = true
  enable_dns_support   = true

  # nat gateway for external access in private (one per az)
  enable_nat_gateway     = true
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = true

  dhcp_options_domain_name         = "${var.region}.compute.internal"
  dhcp_options_domain_name_servers = ["AmazonProvidedDNS"]

  # public_subnet_tags = {
  #   "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  #   "kubernetes.io/role/elb"                      = "1"
  # }

  # private_subnet_tags = {
  #   "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  #   "kubernetes.io/role/internal-elb"             = "1"
  # }
}
