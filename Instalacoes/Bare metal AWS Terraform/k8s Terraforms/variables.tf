variable "region" {
  type        = string
  description = "AWS region in which to create the cluster."
  default     = "us-east-1"
}

variable "subnet_cidr_block" {
  type        = string
  description = "CIDR block for subnet"
  default     = "10.0.0.0/16"
}

variable "tags" {
  type        = map(string)
  description = "A set of tags to assign to the created resources."
  default     = {}
}