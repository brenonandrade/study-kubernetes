region = "us-east-2"
tags = {
  env       = "development"
  team      = "devops"
  terraform = "yes"
  tfsatate  = "no defined"
  project   = "k8s cluster"
}
