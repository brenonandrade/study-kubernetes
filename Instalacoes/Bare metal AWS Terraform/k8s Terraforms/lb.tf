resource "aws_lb" "api-k8s-local" {
  name               = "api-${var.cluster_name}"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.public01.id]
  tags = {
    KubernetesCluster                           = var.cluster_name
    Name                                        = "api.${var.cluster_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}