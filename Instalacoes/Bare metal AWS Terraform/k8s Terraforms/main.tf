module "cluster" {
  source = "./aws_k8s"
  #version    = "0.1"

  vpc_id    = aws_vpc.main.id
  subnet_id = aws_subnet.main.id
  cluster_name = "mycluster"
  tags         = var.tags
}
