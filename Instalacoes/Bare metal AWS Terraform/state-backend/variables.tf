variable "region" {
  type        = string
  description = "The default region that the project will run"
  default     = "us-east-1"
}

variable "project_name" {
  type        = string
  description = "The name of this project"
}

variable "acl" {
  type        = string
  description = "Permition acl"
  default     = "private"
}

variable "versioning" {
  type        = string
  description = "Versioning is Enabled, Disabled or Suspended"
  default     =  "Enabled"
}

variable "read_capacity" {
  type        = number
  description = "Read Capacity"
  default     = 20
}

variable "write_capacity" {
  type        = number
  description = "Write Capacity"
  default     = 20
}

variable "billing_mode" {
  type        = string
  description = "Billing Mode"
  default     = "PROVISIONED"
}

variable "enable_point_in_time_recovery" {
  type        = bool
  description = "Enable DynamoDB point-in-time recovery"
  default     = true
}

variable "enable_server_side_encryption" {
  type        = bool
  description = "Enable DynamoDB server-side encryption"
  default     = true
}

variable "tags" {
  type        = map(any)
  description = "Lista de tags que serão passados para todo o projeto"
  default     = {}
}