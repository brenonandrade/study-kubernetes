resource "aws_dynamodb_table" "dynamodb_lock_table" {
  name           = "${var.project_name}-dynamodb"
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  billing_mode   = var.billing_mode
  hash_key       = "LockID"
  server_side_encryption {
    enabled = var.enable_server_side_encryption
  }
  point_in_time_recovery {
    enabled = var.enable_point_in_time_recovery
  }
  lifecycle {
    ignore_changes = [
      billing_mode,
      read_capacity,
      write_capacity,
    ]
  }
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "${var.project_name}_dynamodb_state_lock_table"
  }
}