project_name                  = "company"
region                        = "us-east-1"
acl                           = "private"
versioning                    = "Enabled"
read_capacity                 = 20
billing_mode                  = "PROVISIONED"
enable_point_in_time_recovery = true
enable_server_side_encryption = true