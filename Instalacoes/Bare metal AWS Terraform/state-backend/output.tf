output "s3_bucket_domain_name" {
  value       = aws_s3_bucket.terraform_state.bucket_domain_name
  description = "S3 bucket domain name"
}

output "s3_bucket_id" {
  value       = aws_s3_bucket.terraform_state.id
  description = "S3 bucket ID"
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.terraform_state.arn
  description = "S3 bucket ARN"
}

output "dynamodb_table_name" {
  value       = aws_dynamodb_table.dynamodb_lock_table.name
  description = "DynamoDB table name"
}

output "dynamodb_table_id" {
  value       = aws_dynamodb_table.dynamodb_lock_table.id
  description = "DynamoDB table ID"
}

output "dynamodb_table_arn" {
  value       = aws_dynamodb_table.dynamodb_lock_table.arn
  description = "DynamoDB table ARN"
}

output "region" {
  value       = var.region
  description = "Region for bucket s3 to tfstate"
}