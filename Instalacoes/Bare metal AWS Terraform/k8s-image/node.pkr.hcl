packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "node" {
  profile    = "${var.aws_profile}"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"

  ami_name      = "ami-${var.project_name}"
  instance_type = "${var.instance_type}"
  region        = "${var.region}"

  source_ami = "${var.ami_id}"

  ssh_username          = "${var.ssh_username}"
  ssh_pty               = "${var.ssh_pty}"
  force_deregister      = "true"
  force_delete_snapshot = "true"
}

build {
  sources = ["source.amazon-ebs.node"]

  provisioner "file" {
    destination = "/tmp"
    source      = "./scripts"
  }

  provisioner "shell" {
    execute_command = "{{ .Vars }} sudo -E /bin/sh '{{ .Path }}'"

    scripts = [
      "./scripts/init.sh",
      "./scripts/install-requests.sh",
      "./scripts/install-containerd.sh",
      "./scripts/install-kubernetes.sh",
      "./scripts/clean.sh"
    ]
  }
}