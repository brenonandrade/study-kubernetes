#!/bin/bash

# Sanitize
set -eu

PATH="/usr/sbin:/sbin:/usr/bin:/bin:/usr/local/bin"

echo "########## Instalando containerd com o systemd de cgroups ##########"
DEBIAN_FRONTEND=noninteractive apt install -qq -y containerd apt-transport-https

echo "########## Instalando containerd com o systemd de cgroups ##########"
sudo mkdir /etc/containerd
sudo containerd config default > /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

echo "########## Reiniciando o containerd e ativando o serviço ##########"
sudo systemctl restart containerd
sudo systemctl enable containerd >/dev/null 2>&1

echo "########## Conferindo ########## "
sudo ctr --version