#!/bin/bash

# Sanitize
set -eu

PATH="/usr/sbin:/sbin:/usr/bin:/bin:/usr/local/bin"

echo "########## Instalando pacotes necessários ##########"
DEBIAN_FRONTEND=noninteractive apt-get install -y apt-transport-https ca-certificates curl vim bash-completion

echo "########## Download da chave publica do repositório ##########"
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "########## Adicionando o repo do kubernetes ##########"
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

echo "########## Update do repositório ##########"
DEBIAN_FRONTEND=noninteractive sudo apt-get update

echo "##########Install kubernetes binaries ##########"
DEBIAN_FRONTEND=noninteractive apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

echo "########## Install bash completion ##########"
sudo kubectl completion bash > /etc/bash_completion.d/kubectl

echo "########## Check ##########"
sudo kubectl version --client
