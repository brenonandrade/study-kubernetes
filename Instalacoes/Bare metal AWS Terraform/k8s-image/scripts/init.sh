#!/bin/bash

# Sanitize
set -eu

PATH="/usr/sbin:/sbin:/usr/bin:/bin:/usr/local/bin"

echo "########## Initialization ##########"

echo "########## Update e Upgrade do sistema ##########"
DEBIAN_FRONTEND=noninteractive apt-get update -q
DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq

echo "########## Desativando o swap ##########"
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a

# echo "########## Desativando o firewall caso exista ##########"
# if command -v ufw &> /dev/null
# then
#     sudo systemctl disable --now ufw
# fi

echo "########## Ativando modulos do kernel necessarios para o containerd ##########"
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
  overlay
  br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

echo "########## Ativando iptables e ip_foward ##########"

cat <<EOF | sudo tee /etc/sysctl.d/kubernetes.conf
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables  = 1
  net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system