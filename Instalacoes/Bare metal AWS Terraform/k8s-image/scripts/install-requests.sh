#!/bin/bash

# Sanitize
set -eu

PATH="/usr/sbin:/sbin:/usr/bin:/bin:/usr/local/bin"

echo "########## Update do sistema ##########"
DEBIAN_FRONTEND=noninteractive apt-get update -q

echo "########## Instalando pacotes extras ##########"
DEBIAN_FRONTEND=noninteractive apt-get install jq curl iputils-ping unzip vim -y -q


echo "########## Instalando o AWS CLI ##########"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

aws --version