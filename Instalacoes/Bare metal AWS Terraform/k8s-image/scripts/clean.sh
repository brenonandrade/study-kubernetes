#!/bin/bash

# Sanitize
set -eu
PATH="/usr/sbin:/sbin:/usr/bin:/bin:/usr/local/bin"

echo "########## Cleaning /tmp ########## "
sudo rm -fr /tmp/*

echo "*########## Cleaning APT files ########## "
DEBIAN_FRONTEND=noninteractive sudo apt-get autoremove -y -q
DEBIAN_FRONTEND=noninteractive sudo apt-get clean all -q
