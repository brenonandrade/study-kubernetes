variable "aws_profile" {
  description = "Profile to use on aws"
  type        = string
  default     = ""
}

variable "aws_access_key" {
  description = "AWS key"
  type        = string
  default     = ""
}

variable "aws_secret_key" {
  description = "AWS secret"
  type        = string
  default     = ""
}

variable "project_name" {
  description = "Project name to identificate the image"
  type        = string
  default     = "k8s-node"
}

variable "region" {
  description = "Region where will create the image"
  type        = string
  default     = "us-east-1"
}

variable "ssh_username" {
  description = "Name to acess ssh"
  type        = string
  default     = "ubuntu"
}

variable "ssh_port" {
  description = "Port to access ssh"
  type        = number
  default     = 22
}

variable "ssh_pty" {
  description = "PTY will be requested for the SSH connection"
  type        = bool
  default     = true
}

variable "instance_type" {
  description = "Instante type to create the image"
  type        = string
  default     = "t3.medium"
}

variable "ami_id" {
  description = "Image id"
  type        = string
  default     = "ami-051dcca84f1edfff1" #canonical oficial
}



#"ami-065b4e65cabba162e" ubuntu minimal