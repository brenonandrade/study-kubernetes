
variable "region" {
  description = "Region to create vpc and subnets"
  type        = string
  default     = "us-east-1"
}

variable "cluster_name" {
  description = "Cluster Name for tags"
  type        = string
  default     = "company-cluster"
  validation {
    condition = can(regex("^[0-9a-z-]+$", var.cluster_name))
    error_message = "Only lowercase letters, numbers and -"
  }
}

variable "cidr_prefix" {
  description = "Prefix to cidr /16. Prefix.0.0/16"
  type        = string
  default     = "10.0"
}

# variable "vpc_cidr_block" {
#   type = string
# }
# variable "vpc_name" {
#   type = string
# }
# variable "region" {
#   type = string
# }
# variable "private_subnet01_netnum" {
#   type = string
# }
# variable "public_subnet01_netnum" {
#   type = string
# }