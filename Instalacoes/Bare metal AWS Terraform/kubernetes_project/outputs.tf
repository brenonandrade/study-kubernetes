
output "cluster_name" {
  description = "Kubernetes cluster name"
  value       = local.cluster_name
}

output "vpc_id" {
  description = "Kubernetes cluster name"
  value       = local.vpc_id
}
