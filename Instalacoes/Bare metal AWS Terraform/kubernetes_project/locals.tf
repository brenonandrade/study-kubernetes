locals {
  env          = "production"
  team         = "devops"
  project_name = "kubernetes-cluster"

  vpc_id              = data.terraform_remote_state.remote.outputs.vpc_id
  vpc_cidr_block      = data.terraform_remote_state.remote.outputs.vpc_cidr_block

  public_subnet_ids   = data.terraform_remote_state.remote.outputs.public_subnet_ids
  private_subnet_ids  = data.terraform_remote_state.remote.outputs.private_subnet_ids
  database_subnet_ids = data.terraform_remote_state.remote.outputs.database_subnet_ids
  
  cluster_name = data.terraform_remote_state.remote.outputs.cluster_name

  tags = {
    Team             = local.team
    Env              = local.env
    System           = local.project_name
    CreationOrigin   = "terraform"
    repositoryOrigin = "kubernetes"
    State            = "kubernetes/us-east-1/production/terraform.tfstate"
  }
}


