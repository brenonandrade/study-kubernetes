terraform {
  backend "s3" {
    bucket         = "company-tfstate"
    key            = "kubernetes/us-east-1/production/terraform.tfstate"
    encrypt        = true
    region         = "us-east-1"
    dynamodb_table = "company-dynamodb"
  }
}