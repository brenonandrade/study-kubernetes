data "terraform_remote_state" "remote" {
  backend = "s3"
  config = {
    bucket   = "company-tfstate"
    key      = "network/us-east-1/production/terraform.tfstate"
    region   = "us-east-1"
  }
}

# data "terraform_remote_state" "remote" {
#   backend = "local"

#   config = {
#     path = "../network/terraform.tfstate"
#   }
# }