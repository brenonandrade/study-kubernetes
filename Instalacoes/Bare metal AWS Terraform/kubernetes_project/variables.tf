variable "region" {
  description = "Region to create vpc and subnets"
  type        = string
  default     = "us-east-1"
}

variable "master_instance_type" {
  type = string
  default = "t3.medium"
}

variable "worker_instance_type" {
  type = string
  default = "t3.small"
}

variable "num_master" {
  type = number
  description = "Number of master instances. 3 5 or 7"
  default = 1
  validation {
    condition     = var.num_master == 1 || var.num_master == 3 ||  var.num_master == 5 || var.num_master == 7
    error_message = "Need to be 3 for 1 fault master, 5 to 2 fault master o 7 for 3 fault master"
  }
}

variable "ami_id" {
  description = "Image id"
  type        = string
  default     = "ami-0304125a70a77be66" #Created by packer
}

#ami-0f269532597814855

variable "workers_min_size" {
  description = "Minimal number worker nodes instances"
  type = number
  default = 3
}

variable "workers_max_size" {
  description = "Maximum number nodes instances"
  type = number
  default = 10
}

variable "kubeconfig_dir" {
  type        = string
  description = "Directory on the local machine in which to save the kubeconfig file of the created cluster. The basename of the kubeconfig file will consist of the cluster name followed by \".conf\", for example, \"my-cluster.conf\". The directory may be specified as an absolute or relative path. The directory must exist, otherwise an error occurs. By default, the current working directory is used."
  default     = "."
}

variable "kubeconfig_file" {
  type        = string
  description = "**This is an optional variable with a default value of null**. The exact filename as which to save the kubeconfig file of the crated cluster on the local machine. The filename may be specified as an absolute or relative path. The parent directory of the filename must exist, otherwise an error occurs. If a file with the same name already exists, it will be overwritten. If this variable is set to a value other than null, the value of the \"kubeconfig_dir\" variable is ignored."
  default     = null
}

variable "allowed_ssh_cidr_blocks" {
  type        = list(string)
  description = "Lista de blocos CIDR a partir dos quais é permitido fazer conexões SSH com as instâncias do EC2 que formam os nós do cluster"
  default     = [" 0.0.0.0/0 "]
}

variable "pod_network_cidr_block" {
  type        = string
  description = "**This is an optional variable with a default value of null**. CIDR block for the Pod network of the cluster. If set, Kubernetes automatically allocates Pod subnet IP address ranges to the nodes (i.e. sets the \".spec.podCIDR\" field of the node objects). If null, the cluster is created without an explicitly determined Pod network IP address range, and the nodes are not allocated any Pod subnet IP address ranges (i.e. the \".spec.podCIDR\" field of the nodes is not set)."
  default     = null
}

variable "private_key_file" {
  type        = string
  description = "Path para a chave privada que permitirá o acesso via ssh aos nodes"
  default     = "~/.ssh/id_rsa"
}

variable "public_key_file" {
  type        = string
  description = "Path para a chave publica que ficará autorizada dentro dos nodes"
  default     = "~/.ssh/id_rsa.pub"
}

variable "tags" {
  type        = map(string)
  description = "A set of tags to assign to the created AWS resources. These tags will be assigned in addition to the default tags. The default tags include \"terraform-kubeadm:cluster\" which is assigned to all resources and whose value is the cluster name, and \"terraform-kubeadm:node\" which is assigned to the EC2 instances and whose value is the name of the Kubernetes node that this EC2 corresponds to."
  default     = {}
}