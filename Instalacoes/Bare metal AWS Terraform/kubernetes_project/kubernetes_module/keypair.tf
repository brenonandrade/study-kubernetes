# Will create key pair using your key int ./ssh

resource "aws_key_pair" "sshkey" {
  key_name_prefix = "${var.cluster_name}-"
  public_key      = file(var.public_key_file)
  tags            = var.tags
}