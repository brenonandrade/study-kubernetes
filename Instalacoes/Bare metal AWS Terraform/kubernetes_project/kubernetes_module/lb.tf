resource "aws_lb" "api_k8s_local" {
  name               = "api-${var.cluster_name}"
  internal           = false
  load_balancer_type = "network"
  subnets            = var.public_subnet_ids
  tags = {
    KubernetesCluster                           = var.cluster_name
    Name                                        = "api.${var.cluster_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_lb_listener" "k8s_api" {
  load_balancer_arn = aws_lb.api_k8s_local.arn
  port              = "6443"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.control_plane.arn
  }
}

resource "aws_lb_target_group" "control_plane" {
  name     = "control-plane"
  port     = 6443
  protocol = "TCP"
  vpc_id   = var.vpc_id
}
