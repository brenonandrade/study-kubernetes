# bastion node sec group and rules

resource "aws_security_group" "bastion_node" {
  name        = "sg_bastion_node"
  description = "Bastion security group"
  vpc_id      = var.vpc_id

  tags = {
    Name = "sg_bastion"
  }
}

resource "aws_security_group_rule" "allow_inbound_bastion" {
  security_group_id = aws_security_group.bastion_node.id
  
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_outbound_bastion" {
  security_group_id = aws_security_group.bastion_node.id

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

# workers node sec group and rules

resource "aws_security_group" "k8s_worker_nodes" {
  name        = "sg_k8s_workers_${var.cluster_name}"
  description = "Worker nodes security group"
  vpc_id      = var.vpc_id

  tags = {
    Name                                        = "${var.cluster_name}_nodes"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_security_group_rule" "allow_inbound_workers" {
  security_group_id = aws_security_group.k8s_worker_nodes.id

  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = [var.vpc_cidr_block]
}

resource "aws_security_group_rule" "allow_outbound_workers" {
  security_group_id = aws_security_group.k8s_worker_nodes.id

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

# Masters node sec group and rules
resource "aws_security_group" "k8s_master_nodes" {
  name        = "sg_k8s_masters_${var.cluster_name}"
  description = "Master nodes security group"
  vpc_id      = var.vpc_id
  tags = {
    Name                                        = "${var.cluster_name}_nodes"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_security_group_rule" "traffic_from_lb" {
  type              = "ingress"
  description       = "Allow API traffic from the load balancer"
  security_group_id = aws_security_group.k8s_master_nodes.id

  from_port         = 6443
  to_port           = 6443
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "traffic_from_workers_to_masters" {
  security_group_id        = aws_security_group.k8s_master_nodes.id

  type                     = "ingress"
  description              = "Traffic from the worker nodes to the master nodes is allowed"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  
  source_security_group_id = aws_security_group.k8s_worker_nodes.id
}

resource "aws_security_group_rule" "traffic_internal_masters" {
  security_group_id        = aws_security_group.k8s_master_nodes.id
  description              = "Permit masters comunications each others"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1" 
  source_security_group_id = aws_security_group.k8s_master_nodes.id
}

resource "aws_security_group_rule" "traffic_from_bastion_to_masters" {
  security_group_id        = aws_security_group.k8s_master_nodes.id

  type                     = "ingress"
  description              = "Traffic from the bastion node to the master node is allowed"
  from_port                = 22
  to_port                  = 22
  protocol                 = "TCP"

  source_security_group_id = aws_security_group.bastion_node.id
}

resource "aws_security_group_rule" "masters_egress" {
  security_group_id = aws_security_group.k8s_master_nodes.id

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}
