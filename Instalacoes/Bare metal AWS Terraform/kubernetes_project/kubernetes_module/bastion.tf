resource "aws_instance" "bastion" {
  ami                    = var.ami_id
  instance_type          = "t3.small"
  vpc_security_group_ids = [aws_security_group.bastion_node.id]
  key_name               = aws_key_pair.sshkey.key_name
  #Just at public subnet 0
  subnet_id              = var.public_subnet_ids[0]
  root_block_device {
    volume_size = 20
  }
  tags = {
    Name = "bastion.${var.cluster_name}"
  }
}
