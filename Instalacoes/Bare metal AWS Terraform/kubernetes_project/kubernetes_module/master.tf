resource "aws_launch_configuration" "masters_k8s_local" {
  name_prefix          = "masters.${var.cluster_name}"
  image_id             = var.ami_id
  instance_type        = var.master_instance_type
  key_name             = aws_key_pair.sshkey.key_name
  iam_instance_profile = aws_iam_instance_profile.k8s_master_role_instance_profile.id
  security_groups      = [aws_security_group.k8s_master_nodes.id]
  user_data            = <<EOT
#!/bin/bash

# SETANDO O HOSTNAME
hostnamectl set-hostname --static "$(curl -s http://169.254.169.254/latest/meta-data/local-hostname)"
cat <<SSHUBUNTU >/home/ubuntu/.ssh/config
Host *
    StrictHostKeyChecking no
SSHUBUNTU

cat <<SSHROOT >/root/.ssh/config
Host *
    StrictHostKeyChecking no
SSHROOT

# CHAVE PRIVADA PARA ACESSO AOS MASTERS
sshkey=/home/ubuntu/.ssh/key
cat <<SSHKEY >$sshkey
${local.ssh_private_key}
SSHKEY

chown ubuntu:ubuntu $sshkey /home/ubuntu/.ssh/config
chmod 400 $sshkey

# ESPERANDO CONECTIVIDADE DE REDE COM A INSTANCIA
until ping -c 1 google.com; do
  sleep 5
done

# CONFERINDO SE EXISTE ALGUM CLUSTER RODANDO
checkcluster=$(curl -sk https://${aws_lb.api_k8s_local.dns_name}:6443 | jq -r '.kind')

curl -sk https://api-company-cluster-498d8b5e5165badd.elb.us-east-1.amazonaws.com:6443 | jq -r '.kind'
if [[ $checkcluster != 'Status' ]]; then
# NESSE CASO NAO TEM CLUSTER RODANDO ENTAO CRIA CRIA O INIT EXECUTA E FINALIZA PARA ESSE MASTER

  cat <<-INIT >/tmp/init.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
apiServer:
  extraArgs:
    cloud-provider: aws
clusterName: ${var.cluster_name}
controlPlaneEndpoint: "${aws_lb.api_k8s_local.dns_name}"
controllerManager:
  extraArgs:
    cloud-provider: aws
    configure-cloud-routes: "false"
kubernetesVersion: stable
networking:
  dnsDomain: cluster.local
  podSubnet: 192.168.0.0/16 # MUDAR CASO NECESSARIO
  serviceSubnet: 10.96.0.0/12
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
nodeRegistration:
  kubeletExtraArgs:
    cloud-provider: aws
INIT

  # INICIANDO O CLUSTER
  sudo kubeadm init --config /tmp/init.yaml --upload-certs --ignore-preflight-errors=All
  # INSTALANDO O CNI
  sudo kubectl --kubeconfig /etc/kubernetes/admin.conf apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

else
  # PASSOS PARA O JOIN QUANDO O CLUSTER JA ESTA CRIADO
  MASTERS=$(aws ec2 describe-instances --filters Name=tag-key,Values="kubernetes.io/cluster/${var.cluster_name}" --region ${var.region} | jq -r '.Reservations[].Instances[] | select (.State.Name == "running") | select(.Tags[].Key == "k8s.io/role/master") | .PrivateIpAddress')

# ESPERANDO A RESPOSTA DE UM DOS NOS MASTERS
  while true; do
    for m in $MASTERS; do
      if JOIN_COMMAND=$(ssh -i $sshkey ubuntu@$m sudo kubeadm token create --print-join-command); then
        # SE CONSEGUIRMOS NOS CONECTAR E OBTER O COMANDO JOIN, OBTENHA TAMBEM O CERTIFICADO
        CERTIFICATE_KEY=$(ssh -i $sshkey ubuntu@$m sudo kubeadm init phase upload-certs --upload-certs | tail -1)
        # TODO: TECHNICAL DEBT: we are dependent on the first master node to be available since it contains  the ETCD configuration. We need to find a way to make all the master nodes able to be used for joining other nodes
        
        if [[ -n $CERTIFICATE_KEY ]]; then
          #  SE OBTIVERMOS UM CERTIFICADO NAO EH NECESSARIO FAZER UM LOOP EM OUTROS NODES MASTERS
          break 2
        fi
      fi
      # ESPERANDO ALGUNS SEGUNDOS ANTES DE TENTAR NOVAMENTE A LISTA DE NODE MASTERS
      sleep 10
    done
  done

  # PEGANDO AS VARIAVEIS DO COMANDO JOIN PARA MONTAR O JOIN ATRAVES DO YAML
  LB=$(awk '{print $3}' <<< "$JOIN_COMMAND")
  TOKEN=$(awk '{print $5}' <<< "$JOIN_COMMAND")
  CERTIFICATE=$(awk '{print $7}' <<< "$JOIN_COMMAND")

# PREPARANDO O JOIN
# We need to ensure that worker nodes that are dedicated to Prometheus and Kafka are labelled correctly
  cat <<-JOIN >/tmp/join.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: $TOKEN
    apiServerEndpoint: "$LB"
    caCertHashes: ["$CERTIFICATE"]
nodeRegistration:
  name: $(hostname)
  kubeletExtraArgs:
    cloud-provider: aws
controlPlane:
  localAPIEndpoint:
    advertiseAddress: $(hostname -i)
  certificateKey: $CERTIFICATE_KEY
JOIN

  # EXECUTAR O JOIN
  kubeadm join --config /tmp/join.yaml --ignore-preflight-errors=All
fi

# COPIA O KUBE CONFIG
sudo --user=ubuntu mkdir -p /home/ubuntu/.kube
sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
sudo chown -R $(id -u ubuntu):$(id -g ubuntu) /home/ubuntu/.kube/
EOT
  lifecycle {
    create_before_destroy = true
  }
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = true
  }
  depends_on = [
    aws_lb.api_k8s_local
  ]
}

resource "aws_autoscaling_group" "master_k8s_local" {
  name                 = "${var.cluster_name}-masters"
  launch_configuration = aws_launch_configuration.masters_k8s_local.id
  max_size             = var.num_master
  min_size             = var.num_master
  vpc_zone_identifier  = var.private_subnet_ids
  target_group_arns    = [aws_lb_target_group.control_plane.arn]
  # default_cooldown     = 150

  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "master.${var.cluster_name}"
        "propagate_at_launch" = true
      },
      {
        "key"                 = "KubernetesCluster"
        "value"               = var.cluster_name
        "propagate_at_launch" = true
      },
      {
        "key"                 = "k8s.io/role/master"
        "value"               = "1"
        "propagate_at_launch" = true
      },
      {
        "key"                 = "kubernetes.io/cluster/${var.cluster_name}"
        "value"               = "1"
        "propagate_at_launch" = true
      },
    ],
  )
  depends_on = [
    aws_lb.api_k8s_local
  ]
}




# curl -sk https://api-company-cluster-10fbcbc105819c5f.elb.us-east-1.amazonaws.com:6443 | jq -r '.kind'
# MASTERS=$(aws ec2 describe-instances --filters Name=tag-key,Values="kubernetes.io/cluster/company-cluster" --region us-east-1 | jq -r '.Reservations[].Instances[] | select (.State.Name == "running") | select(.Tags[].Key == "k8s.io/role/master") | .PrivateIpAddress')
# mkdir -p /home/$USER/.kube
# sudo cp -i /etc/kubernetes/admin.conf /home/$USER/.kube/config
# sudo chown -R $(id -u $USER):$(id -g $USER) /home/$USER/.kube/

















# # #------------------------------------------------------------------------------#
# # # Elastic IP for master node
# # #------------------------------------------------------------------------------#

# # # EIP for master node because it must know its public IP during initialisation
# # resource "aws_eip" "master" {
# #   vpc  = true
# #   tags = local.tags
# # }

# # resource "aws_eip_association" "master" {
# #   allocation_id = aws_eip.master.id
# #   instance_id   = aws_instance.master.id
# # }

# # #------------------------------------------------------------------------------#
# # # Bootstrap token for kubeadm
# # #------------------------------------------------------------------------------#

# # # Generate bootstrap token
# # # See https://kubernetes.io/docs/reference/access-authn-authz/bootstrap-tokens/
# # resource "random_string" "token_id" {
# #   length  = 3
# #   special = false
# #   upper   = false
# # }

# # resource "random_string" "token_secret" {
# #   length  = 16
# #   special = false
# #   upper   = false
# # }

# # resource "aws_instance" "master" {
# #   ami           = data.aws_ami.ubuntu.image_id
# #   instance_type = var.master_instance_type
# #   subnet_id     = var.subnet_id
# #   key_name      = aws_key_pair.main.key_name
# #   vpc_security_group_ids = [
# #     aws_security_group.egress.id,
# #     aws_security_group.ingress_internal.id,
# #     aws_security_group.ingress_k8s.id,
# #     aws_security_group.ingress_ssh.id
# #   ]
# #   tags      = merge(local.tags, { "terraform-kubeadm:node" = "master" })
# #   user_data = <<-EOF
# #   #!/bin/bash

# #   echo "Desativando o swap"
# #   sed -i '/swap/d' /etc/fstab
# #   swapoff -a

# #   echo "Desativando o firewall"
# #   systemctl disable --now ufw

# #   echo "Ativando modulos do kernel necessarios para o containerd"
# #   cat >>/etc/modules-load.d/containerd.conf<<MODULES
# #   overlay
# #   br_netfilter
# #   MODULES
# #   modprobe overlay
# #   modprobe br_netfilter

# #   echo "Adicionando configuracoes do kernel para o kubernetes"
# #   cat >>/etc/sysctl.d/kubernetes.conf<<SYSCTL
# #   net.bridge.bridge-nf-call-ip6tables = 1
# #   net.bridge.bridge-nf-call-iptables  = 1
# #   net.ipv4.ip_forward                 = 1
# #   SYSCTL
# #   sysctl --system

# #   echo "Instalando containerd com o systemd de cgroups"
# #   apt update -qq >/dev/null 2>&1
# #   apt install -qq -y containerd apt-transport-https >/dev/null 2>&1
# #   mkdir /etc/containerd
# #   containerd config default > /etc/containerd/config.toml
# #   sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
# #   systemctl restart containerd
# #   systemctl enable containerd >/dev/null 2>&1

# #   echo "Adicionando configuracoes do kernel para o kubernetes"
# #   cat >>/etc/sysctl.d/kubernetes.conf<<SYSCTL
# #   net.bridge.bridge-nf-call-ip6tables = 1
# #   net.bridge.bridge-nf-call-iptables  = 1
# #   net.ipv4.ip_forward                 = 1
# #   SYSCTL
# #   sysctl --system >/dev/null 2>&1

# #   echo "Instalando containerd com o systemd de cgroups"
# #   apt update -qq >/dev/null 2>&1
# #   apt install -qq -y containerd apt-transport-https >/dev/null 2>&1
# #   mkdir /etc/containerd
# #   containerd config default > /etc/containerd/config.toml
# #   sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
# #   systemctl restart containerd
# #   systemctl enable containerd >/dev/null 2>&1

# #   echo "Adicionando o repo do kubernetes"
# #   curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - >/dev/null 2>&1
# #   apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main" >/dev/null 2>&1

# #   echo "Instalando binarios do Kubernetes (kubeadm, kubelet)"
# #   apt install -qq -y kubeadm=${var.k8s_version} kubelet=${var.k8s_version} >/dev/null 2>&1

# #   kubeadm init \
# #     --token "${local.token}" \
# #     --token-ttl 15m \
# #     --apiserver-cert-extra-sans "${aws_eip.master.public_ip}" \
# #   %{if var.pod_network_cidr_block != null~}
# #     --pod-network-cidr "${var.pod_network_cidr_block}" \
# #   %{endif~}
# #     --node-name master
# #   # Prepare kubeconfig file for download to local machine
# #   cp /etc/kubernetes/admin.conf /home/ubuntu
# #   pwd >/home/ubuntu/out
# #   whoami >>/home/ubuntu/out
# #   ls -l /home/ubuntu/admin.conf >>/home/ubuntu/out
# #   sudo chown ubuntu:ubuntu /home/ubuntu/admin.conf 
# #   ls -l /home/ubuntu/admin.conf >>/home/ubuntu/out
# #   kubectl --kubeconfig /home/ubuntu/admin.conf config set-cluster kubernetes --server https://${aws_eip.master.public_ip}:6443
# #   # Indicate completion of bootstrapping on this node
# #   touch /home/ubuntu/done
# #   EOF
# # }

# # #------------------------------------------------------------------------------#
# # # Wait for bootstrap to finish on all nodes
# # #------------------------------------------------------------------------------#

# # resource "null_resource" "wait_for_bootstrap_to_finish" {
# #   provisioner "local-exec" {
# #     command = <<-EOF
# #     alias ssh='ssh -q -i ${var.private_key_file} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
# #     while true; do
# #       sleep 2
# #       ! ssh ubuntu@${aws_eip.master.public_ip} [[ -f /home/ubuntu/done ]] >/dev/null && continue
# #       %{for worker_public_ip in aws_instance.workers[*].public_ip~}
# #       ! ssh ubuntu@${worker_public_ip} [[ -f /home/ubuntu/done ]] >/dev/null && continue
# #       %{endfor~}
# #       break
# #     done
# #     EOF
# #   }
# #   triggers = {
# #     instance_ids = join(",", concat([aws_instance.master.id], aws_instance.workers[*].id))
# #   }
# # }

# # resource "null_resource" "download_kubeconfig_file" {
# #   provisioner "local-exec" {
# #     command = <<-EOF
# #     alias scp='scp -q -i ${var.private_key_file} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
# #     scp ubuntu@${aws_eip.master.public_ip}:/home/ubuntu/admin.conf ${local.kubeconfig_file} >/dev/null
# #     EOF
# #   }
# #   triggers = {
# #     wait_for_bootstrap_to_finish = null_resource.wait_for_bootstrap_to_finish.id
# #   }
# # }