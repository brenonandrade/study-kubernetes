locals {
  #token = "${random_string.token_id.result}.${random_string.token_secret.result}"
  # kubeconfig_file = var.kubeconfig_file != null ? abspath(pathexpand(var.kubeconfig_file)) : "${abspath(pathexpand(var.kubeconfig_dir))}/${local.cluster_name}.conf"
  tags         = merge(var.tags, { "terraform-kubeadm:cluster" = var.cluster_name })
  lb_dns_name  = aws_lb.api_k8s_local.dns_name
  ssh_private_key = file(var.private_key_file)
  ssh_public_key = file(var.public_key_file)
}