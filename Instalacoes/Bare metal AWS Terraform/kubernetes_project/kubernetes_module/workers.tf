
resource "aws_launch_configuration" "worker_k8s_local" {
  name_prefix          = "nodes.${var.cluster_name}."
  image_id             = var.ami_id
  instance_type        = var.worker_instance_type
  key_name             = aws_key_pair.sshkey.key_name
  iam_instance_profile = aws_iam_instance_profile.k8s_worker_role_instance_profile.id
  security_groups      = [aws_security_group.k8s_worker_nodes.id]
  user_data            = <<EOT
#!/bin/bash

# SETANDO O HOSTNAME
hostnamectl set-hostname --static "$(curl -s http://169.254.169.254/latest/meta-data/local-hostname)"
cat <<SSHUBUNTU >/home/ubuntu/.ssh/config
Host *
    StrictHostKeyChecking no
SSHUBUNTU

cat <<SSHROOT >/root/.ssh/config
Host *
    StrictHostKeyChecking no
SSHROOT

# CHAVE PRIVADA PARA ACESSO AOS MASTERS
sshkey=/home/ubuntu/.ssh/key
cat <<SSHKEY >$sshkey
${local.ssh_private_key}
SSHKEY

chown ubuntu:ubuntu $sshkey /home/ubuntu/.ssh/config
chmod 400 $sshkey

# ESPERANDO CONECTIVIDADE DE REDE COM A INSTANCIA
until ping -c 1 google.com; do
  sleep 5
done

MASTERS=$(aws ec2 describe-instances --filters Name=tag-key,Values="kubernetes.io/cluster/${var.cluster_name}" --region ${var.region} | jq -r '.Reservations[].Instances[] | select (.State.Name == "running") | select(.Tags[].Key == "k8s.io/role/master") | .PrivateIpAddress')

# ESPERANDO A RESPOSTA DE UM DOS NOS MASTERS
while true; do
  for m in $MASTERS; do
    if JOIN_COMMAND=$(ssh -i $sshkey ubuntu@$m sudo kubeadm token create --print-join-command); then
      # SE CONSEGUIRMOS NOS CONECTAR E OBTER O COMANDO JOIN, OBTENHA TAMBEM O CERTIFICADO
      CERTIFICATE_KEY=$(ssh -i $sshkey ubuntu@$m sudo kubeadm init phase upload-certs --upload-certs | tail -1)
      # TODO: TECHNICAL DEBT: we are dependent on the first master node to be available since it contains  the ETCD configuration. We need to find a way to make all the master nodes able to be used for joining other nodes
      
      if [[ -n $CERTIFICATE_KEY ]]; then
        #  SE OBTIVERMOS UM CERTIFICADO NAO EH NECESSARIO FAZER UM LOOP EM OUTROS NODES MASTERS
        break 2
      fi
    fi
    # ESPERANDO ALGUNS SEGUNDOS ANTES DE TENTAR NOVAMENTE A LISTA DE NODE MASTERS
    sleep 10
  done
done

LB=$(awk '{print $3}' <<< "$JOIN_COMMAND")
TOKEN=$(awk '{print $5}' <<< "$JOIN_COMMAND")
CERTIFICATE=$(awk '{print $7}' <<< "$JOIN_COMMAND")

# PREPARANDO O JOIN
# We need to ensure that worker nodes that are dedicated to Prometheus and Kafka are labelled correctly
cat <<-JOIN >/tmp/join.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: $TOKEN
    apiServerEndpoint: "$LB"
    caCertHashes: ["$CERTIFICATE"]
nodeRegistration:
  name: $(hostname)
  kubeletExtraArgs:
    cloud-provider: aws
JOIN

# EXECUTAR O JOIN PARA MASTERS OU WORKERS
kubeadm join --config /tmp/join.yaml --ignore-preflight-errors=All
EOT
  lifecycle {
    create_before_destroy = true
  }
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = true
  }
  depends_on = [
    aws_lb.api_k8s_local
  ]
}
resource "aws_autoscaling_group" "nodes-k8s" {
  name                 = "${var.cluster_name}-workers"
  launch_configuration = aws_launch_configuration.worker_k8s_local.id
  min_size             = var.nodes_min_size
  max_size             = var.nodes_max_size
  vpc_zone_identifier  = var.private_subnet_ids
  tags = concat(
    [
      {
        key                 = "Name"
        value               = "nodes.${var.cluster_name}"
        propagate_at_launch = true
      },
      {
        key                 = "KubernetesCluster"
        value               = var.cluster_name
        propagate_at_launch = true
      },
      {
        key                 = "k8s.io/role/node"
        value               = "1"
        propagate_at_launch = true
      },
      {
        key                 = "kubernetes.io/cluster/${var.cluster_name}"
        value               = "1"
        propagate_at_launch = true
      }
    ],
  )
  depends_on = [
    aws_lb.api_k8s_local,
    aws_autoscaling_group.master_k8s_local,
    aws_launch_configuration.masters_k8s_local
  ]
}