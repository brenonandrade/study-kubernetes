module "kubernetes" {
  source = "./kubernetes_module"

  region = var.region

  vpc_id         = local.vpc_id
  vpc_cidr_block = local.vpc_cidr_block

  ami_id       = var.ami_id
  cluster_name = local.cluster_name

  master_instance_type = var.master_instance_type
  worker_instance_type = var.worker_instance_type

  nodes_min_size = var.workers_min_size
  nodes_max_size = var.workers_max_size

  private_subnet_ids = local.private_subnet_ids
  public_subnet_ids  = local.public_subnet_ids

  private_key_file = var.private_key_file
  public_key_file  = var.public_key_file

  num_master = var.num_master

}

