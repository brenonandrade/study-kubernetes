# Baremetal Local Multi Master E ETCD Cluster Externo

Esse projeto é uma atualização do projeto multi master para aumentar a complexidade e alta disponibilidade do nosso kubernetes local colocando o ETCD como um serviço externo ao cluster.

Os requisitos necessários:

- [vagrant](https://www.vagrantup.com/docs/installation)

 ```bash
 sudo apt-get install vagrant
 ```

- [virtualbox](https://www.virtualbox.org/wiki/Linux_Downloads)

 ```bash
 sudo apt-get install virtualbox
 ```

 Em versões mais recentes do virtual box, precisei mexer na configuração para liberar o range de ips.

```bash
sudo mkdir /etc/vbox
sudo echo "* 10.0.0.0/8 192.168.0.0/16" >> /etc/vbox/networks.conf
sudo echo "* 2001::/64" >> /etc/vbox/networks.conf
```

A topologia proposta aqui segue a seguinte projeto do Kubernetes.
<https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/ha-topology/>
![MultiMaster](../../resources/kubeadm-ha-topology-external-etcd.svg)

Configura sobre o etcd em <https://etcd.io/>

Observe que o cluster ETCD não possuí ligação com o load balancer e a comunicação com os masters é direta. Somente os masters se comunicão com o etcd através do apiserver.

<https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/ha-topology/>
![ClusterETCD](../../resources/etcdcluster.png)

> Para a comunicação entre os nodes do cluster, a porta utilizada  é a 2380.
> Para a comunicação dos nodes com os masters a porta é a 2379.

Para essa instalação vamos configurar 3 nodes masters, 2 nodes workers e 2 load balancer e `3 vms para formar o nosso cluster ETCD`.

O foco da explicacão aqui será sob o cluster ETCD. Para entendimento da parte multi master configra o [projeto MultiMaster](../Baremetal%20Local%20MultiMaster/).

## Gerar certificado TLS

É necessário gerar um certificado tls para que os masters se comuniquem com o cluster etcd de forma criptografada. É possível rodar sem o certificado? sim, mas vamos fazer a coisa da forma certa para aprender.

Primeiro passo é criar os certificados localmente para que esses arquivos estejam disponíveis durante a criação do nosso ambiente completo.

```bash
wget -q --show-progress \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssl \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssljson
  
chmod +x cfssl cfssljson
sudo mv cfssl cfssljson /usr/local/bin/
```

Agora que ja temos os binários vamos criar a nossa Autoridade Certificadora (CA).

A pasta certs contém o certificado que eu já criei antes, mas para gerar um novo apague a pasta certs e crie uma nova no lugar.

```bash
rm -rf certs
mkdir certs
cd certs
```

O comando abaixo cria a CA baseado no json específicado

```bash

cat > ca-config.json <<EOF
{
    "signing": {
        "default": {
            "expiry": "8760h"
        },
        "profiles": {
            "etcd": {
                "expiry": "8760h",
                "usages": ["signing","key encipherment","server auth","client auth"]
            }
        }
    }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "etcd cluster",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Vila Velha",
      "O": "Kubernetes",
      "OU": "ETCD-CA",
      "ST": "Espirito Santo"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca
```

Agora vamos crirar um certificado tls. Esse certificado esta sendo criado para os ips das máquinas etcds que estão declaradas no vagrantfile, se vc alterar alguma coisa lá precisará alterar aqui também.

```bash
ETCD1_IP="10.10.10.231"
ETCD2_IP="10.10.10.232"
ETCD3_IP="10.10.10.233"

cat > etcd-csr.json <<EOF
{
  "CN": "etcd",
  "hosts": [
    "localhost",
    "127.0.0.1",
    "${ETCD1_IP}",
    "${ETCD2_IP}",
    "${ETCD3_IP}"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "BR",
      "L": "Vila Velha",
      "O": "Kubernetes",
      "OU": "ETCD-CA",
      "ST": "Brazil"
    }
  ]
}
EOF

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=etcd etcd-csr.json | cfssljson -bare etcd
```

Agora já temos todos os certificados. Esses certificado serão copiados através de scrips para dentro das instancias dos masters e etcds

O arquivo [Vagrantfile](./Vagrantfile) contem a configuração das máquinas e as chamadas dos scripts necessários para a instalação.

## Explicando o Vagrantfile

Primeiro passo é criar o cluster etcd para quando fizer o init do cluster já apontar para ele.
Segundo passo é levantar o loadbalancer pois ele é a liga entre o master e os worker será necessário durante o init.
Terceiro passo é criar os masters sendo que o master1 fará o init do cluster e os demais farão um join.
Quarto passo é criar os workers e fazer o join para dentro do cluster

Voce pode executar esses passos devagar para ir acompanhando cada um deles se quiser, mas caso queira levantar tudo só executar o `vagrant up` sem passagem de argumentos.

```bash
vagrant up etcd1 etcd2 etcd3
vagrant up loadbalancer1 loadbalncer2
vagrant up master1 mnaster2 master3
vagrant up worker1 worker2
```

## Acessando o etcd cluster da sua máquina local para testes

Se vc quiser subir somente o cluster etcd pode executar

```bash
vagrant up etcd1 etcd2 etcd3
```

instale na sua máquina o etcdctl escolhendo a versão que deseja em <https://github.com/etcd-io/etcd/releases>

```bash
# Altere a versão aqui caso necessario
ETCD_VER=v3.5.4 

mkdir -p /tmp/etcd
echo "Fazendo o download e extraindo para a pasta /tmp/etcd e removendo o zip"
curl -L "https://github.com/etcd-io/etcd/releases/download/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz" -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

echo "Movendo os binários para /usr/local/bin e dando permissão para os usuários"
sudo mv /tmp/etcd/etcdctl /usr/local/bin
sudo chmod 755 /usr/local/bin/etcdctl

echo "Limpando"
rm -rf /tmp/etcd
```

`Entre na pasta certs` e copie os certificados para a sua máquina local em /etcd/etcd/pki com as permissões necessárias.

```bash
sudo mkdir -p /etc/etcd/pki
sudo cp *.pem /etc/etcd/pki/
sudo chmod 755 /etc/etcd/pki/*
```

Agora é só carregar as variaveis de ambiente para o etcdctl saber para onde apontar e quais certificados usa.

```bash
export ETCDCTL_API=3 
export ETCDCTL_ENDPOINTS=https://10.10.10.231:2379,https://10.10.10.232:2379,https://10.10.10.233:2379
export ETCDCTL_CACERT=/etc/etcd/pki/ca.pem
export ETCDCTL_CERT=/etc/etcd/pki/etcd.pem
export ETCDCTL_KEY=/etc/etcd/pki/etcd-key.pem
```

Execute alguns comandos para teste:

```bash
etcdctl member list
etcdctl endpoint status
etcdctl endpoint health

etcdctl put mykey david
etcdctl get mykey
etcdctl del mykey
etcdctl get mykey
```

## Recomendações

Acredito que o ideal é entender passo a passo do que foi feito lendo os scripts de bootsrap de cada um dos nodes.
