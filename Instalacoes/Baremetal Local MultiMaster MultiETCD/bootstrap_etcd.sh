#!/bin/bash

echo "Adicionando os certificados no node etcd"
KEY_PATH='/vagrant/certs'
mkdir -p /etc/etcd/pki
cp $KEY_PATH/ca.pem /etc/etcd/pki/
cp $KEY_PATH/etcd.pem /etc/etcd/pki/
cp $KEY_PATH/etcd-key.pem /etc/etcd/pki/

ETCD_VER=v3.5.4

echo "Removendo instalações anteriores"
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf /tmp/etcd && mkdir -p /tmp/etcd
rm -rf /usr/local/bin/etcd*

echo "Fazendo o download e extraindo para a pasta /tmp/etcd e removendo o zip"
curl -L "https://github.com/etcd-io/etcd/releases/download/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz" -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

echo "Movendo os binários para /usr/local/bin para entrar no path"
mv /tmp/etcd/etcd /usr/local/bin
mv /tmp/etcd/etcdctl /usr/local/bin
mv /tmp/etcd/etcdutl /usr/local/bin

echo "Removendo arquivos temporarios"
rm -rf /tmp/etcd

echo "Conferindo..."
etcd --version
etcdctl version
etcdutl version

rm -rf /tmp/etcd

echo "Criando o serviço do etcd no systemd com as configurações necessárias"

NODE_IP=`ip -4 address show enp0s8 | grep inet | awk '{print $2}' | cut -d/ -f1`

ETCD_NAME=$(hostname -s)

ETCD1_IP="10.10.10.231"
ETCD2_IP="10.10.10.232"
ETCD3_IP="10.10.10.233"

cat <<EOF >/etc/systemd/system/etcd.service
[Unit]
Description=etcd

[Service]
Type=notify
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/pki/etcd.pem \\
  --key-file=/etc/etcd/pki/etcd-key.pem \\
  --peer-cert-file=/etc/etcd/pki/etcd.pem \\
  --peer-key-file=/etc/etcd/pki/etcd-key.pem \\
  --trusted-ca-file=/etc/etcd/pki/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/pki/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${NODE_IP}:2380 \\
  --listen-peer-urls https://${NODE_IP}:2380 \\
  --advertise-client-urls https://${NODE_IP}:2379 \\
  --listen-client-urls https://${NODE_IP}:2379,https://127.0.0.1:2379 \\
  --initial-cluster-token etcd-cluster-1 \\
  --initial-cluster etcd1=https://${ETCD1_IP}:2380,etcd2=https://${ETCD2_IP}:2380,etcd3=https://${ETCD3_IP}:2380 \\
  --initial-cluster-state new
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

echo "Ativando e iniciando o etcd service"

systemctl daemon-reload
# O true aqui utilizado é que o comando no primeiro cluster irá falhar, pois não conseguirá encontrar os outros nodes etcd
# Logo ele mesmo que falhe, continua a instlação para os demais nodes ao invés de parar.
systemctl enable --now etcd || true
