#!/bin/bash

# echo "Adicionando os certificados para comunicacao com o etcd"
# KEY_PATH='/vagrant/certs'
# mkdir -p /etc/kubernetes/pki/etcd
# cp $KEY_PATH/ca.pem /etc/kubernetes/pki/etcd
# cp $KEY_PATH/etcd.pem /etc/kubernetes/pki/etcd
# cp $KEY_PATH/etcd-key.pem /etc/kubernetes/pki/etcd

echo "Fazendo o pull das imagens necessarios para os containers no master"
kubeadm config images pull >/dev/null 2>&1

echo "Puxando o comando basico de join para o master"
apt install -qq -y sshpass >/dev/null 2>&1
sshpass -p "kubeadmin" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no 10.10.10.241:/join_masters.sh /join_masters.sh 2>/dev/null

echo "Adicionando --apiserver-advertise-address"
JOIN=$(cat /join_masters.sh)
ADVERTISE=$(echo "--apiserver-advertise-address=$(ip -4 address show enp0s8 | grep inet | awk '{print $2}' | cut -d/ -f1)")
IGNORE="--ignore-preflight-errors=All"

echo $JOIN $ADVERTISE $IGNORE| sudo tee /join_masters.sh

echo "Fazendo o join no cluster"
bash /join_masters.sh >/dev/null

echo  "Criando a pasta .kube para o user vagrant"
sudo --user=vagrant mkdir -p /home/vagrant/.kube

echo  "##### Criando o .kube/config a partir do admin.conf #####"
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/
