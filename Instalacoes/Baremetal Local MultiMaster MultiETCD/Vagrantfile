# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_NO_PARALLEL'] = 'yes'

VAGRANT_BOX         = "ubuntu/jammy64"
CPUS_MASTER_NODE    = 2
CPUS_WORKER_NODE    = 1
MEMORY_MASTER_NODE  = 2048
MEMORY_WORKER_NODE  = 1024
MASTER_NODES_COUNT = 3 
WORKER_NODES_COUNT  = 2 # Quantidade de workers (maximo 229)

HA_COUNT   = 2 #Para aumentar a quantidade de loadbalancer (maximo 4)
CPUS_HA    = 1
MEMORY_HA  = 512

ETCD_COUNT   = 3
CPUS_ETCD    = 1
MEMORY_ETCD  = 512

Vagrant.configure(2) do |config|
  config.vm.provision "shell", path: "bootstrap_ssh.sh"

# ETCD Cluster
  (1..ETCD_COUNT).each do |i|

    config.vm.define "etcd#{i}" do |etcdnode|

      etcdnode.vm.box       = VAGRANT_BOX
      etcdnode.vm.hostname  = "etcd#{i}.k8s.com"
      etcdnode.vm.network "private_network", ip: "10.10.10.23#{i}"
      
      etcdnode.vm.provider :virtualbox do |vbox|
        vbox.name    = "etcd#{i}"
        vbox.memory  = MEMORY_ETCD
        vbox.cpus    = CPUS_ETCD
        vbox.customize ["modifyvm", :id, "--groups", "/K8S-ETCDs"]
      end
      
      etcdnode.vm.provider :libvirt do |vbox|
        vbox.nested  = true
        vbox.memory  = MEMORY_ETCD
        vbox.cpus    = CPUS_ETCD
      end

      etcdnode.vm.provision "shell", path: "bootstrap_ssh.sh"
      etcdnode.vm.provision "shell", path: "bootstrap_etcd.sh"
      
    end
  end

# Load Balancer Cluster
  (1..HA_COUNT).each do |i|

    config.vm.define "loadbalancer#{i}" do |lb|

      lb.vm.box       = VAGRANT_BOX
      lb.vm.hostname  = "lb#{i}.k8s.com"
      lb.vm.network "private_network", ip: "10.10.10.25#{i}"

      lb.vm.provider :virtualbox do |vbox|
        vbox.name   = "loadbalancer#{i}"
        vbox.memory = MEMORY_HA
        vbox.cpus   = CPUS_HA
        vbox.customize ["modifyvm", :id, "--groups", "/K8S-LBs"]
      end

      lb.vm.provider :libvirt do |vbox|
        vbox.nested  = true
        vbox.memory  = MEMORY_HA
        vbox.cpus    = CPUS_HA
      end

      lb.vm.provision "shell", path: "bootstrap_ssh.sh"
      lb.vm.provision "shell", path: "bootstrap_lb.sh"
    end
    
  end


# Kubernetes Cluster

  # Masters
  (1..MASTER_NODES_COUNT).each do |i|

    config.vm.define "master#{i}" do |node|

      node.vm.box       = VAGRANT_BOX
      node.vm.hostname  = "master#{i}.k8s.com"
      node.vm.network "private_network", ip: "10.10.10.24#{i}"

      node.vm.provider :virtualbox do |vbox|
        vbox.name    = "master#{i}"
        vbox.memory  = MEMORY_WORKER_NODE
        vbox.cpus    = CPUS_WORKER_NODE
        vbox.customize ["modifyvm", :id, "--groups", "/K8S-Masters"]
      end

      node.vm.provider :libvirt do |vbox|
        vbox.nested  = true
        vbox.memory  = MEMORY_WORKER_NODE
        vbox.cpus    = CPUS_WORKER_NODE
      end
      node.vm.provision "shell", path: "bootstrap_nodes.sh"
      if i == 1
        # Se for o primeiro node vai fazer o init
        node.vm.provision "shell", path: "bootstrap_init_master.sh"
      else
        node.vm.provision "shell", path: "bootstrap_master.sh"
      end
    end
  end
  
  # Workers
  (1..WORKER_NODES_COUNT).each do |i|

    config.vm.define "worker#{i}" do |node|

      node.vm.box        = VAGRANT_BOX
      node.vm.hostname   = "worker#{i}.k8s.com"
      node.vm.network "private_network", ip: "10.10.10.#{i}"

      node.vm.provider :virtualbox do |vbox|
        vbox.name    = "worker#{i}"
        vbox.memory  = MEMORY_WORKER_NODE
        vbox.cpus    = CPUS_WORKER_NODE
        vbox.customize ["modifyvm", :id, "--groups", "/K8S-Workers"]
      end

      node.vm.provider :libvirt do |vbox|
        vbox.nested  = true
        vbox.memory  = MEMORY_WORKER_NODE
        vbox.cpus    = CPUS_WORKER_NODE
      end
      node.vm.provision "shell", path: "bootstrap_nodes.sh"
      node.vm.provision "shell", path: "bootstrap_worker.sh"
    end
  end

end