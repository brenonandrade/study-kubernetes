#!/bin/bash

echo "Adicionando os certificados para comunicacao com o etcd"
KEY_PATH='/vagrant/certs'
mkdir -p /etc/kubernetes/pki/etcd
cp $KEY_PATH/ca.pem /etc/kubernetes/pki/etcd
cp $KEY_PATH/etcd.pem /etc/kubernetes/pki/etcd
cp $KEY_PATH/etcd-key.pem /etc/kubernetes/pki/etcd

IP_ADDR=`ip -4 address show enp0s8 | grep inet | awk '{print $2}' | cut -d/ -f1`

LB_IP="10.10.10.250"
POD_SUBNET="192.168.0.0/16"
ETCD1_IP="10.10.10.231"
ETCD2_IP="10.10.10.232"
ETCD3_IP="10.10.10.233"

echo "Criando arquivo de configuracao do cluster com etcd externo"
cat <<EOF > kubeadm-config.yaml
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: "${IP_ADDR}"
  bindPort: 6443
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.24.3
controlPlaneEndpoint: "${LB_IP}:6443"
etcd:
    external:
        endpoints:
        - "https://${ETCD1_IP}:2379"
        - "https://${ETCD2_IP}:2379
        - "https://${ETCD3_IP}:2379"
        caFile: "/etc/kubernetes/pki/etcd/ca.pem"
        certFile: "/etc/kubernetes/pki/etcd/etcd.pem"
        keyFile: "/etc/kubernetes/pki/etcd/etcd-key.pem"
networking:
  podSubnet: ${POD_SUBNET}
clusterName: homelab
EOF

# controllerManager:
#   extraArgs:
#     allocate-node-cidrs: "true"
# networking:
#   serviceSubnet: 172.96.64.0/18
#   podSubnet: 172.96.128.0/18
# clusterName: homelab

echo "Fazendo o pull das imagens necessarios para os containers no master"
kubeadm config images pull >/dev/null 2>&1

echo "Inicializando o cluster pelo arquivo"
#kubeadm init phase upload-certs --upload-certs --config /tmp/kubeadm-config.yaml #>> /root/kubeinit.log 2>/dev/null
# kubeadm init --config /tmp/kubeadm-config.yaml --ignore-preflight-errors=All #>> /root/kubeinit.log 2>/dev/null
# kubeadm init phase upload-certs --upload-certs

kubeadm init --upload-certs --config kubeadm-config.yaml --ignore-preflight-errors=All
kubeadm init phase upload-certs --upload-certs --config kubeadm-config.yaml

echo  "Criando a pasta .kube para o user vagrant"
sudo --user=vagrant mkdir -p /home/vagrant/.kube

echo  "##### Criando o .kube/config a partir do admin.conf #####"
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/

echo "Deploy do cni weavenet"
kubectl create -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" >/dev/null 2>&1

echo "Criando o comando join para os workers /join_workers.sh"
kubeadm token create --print-join-command > /join_workers.sh

echo "Criando o comando join para os masters /join_masters.sh"
kubeadm token create --certificate-key $(kubeadm init phase upload-certs --upload-certs | tail -1 ) --print-join-command > /join_masters.sh
