#!/bin/bash

echo "Fazendo o pull das imagens necessarios para os containers no master"
kubeadm config images pull >/dev/null 2>&1

echo "Inicializando o cluster"
kubeadm init --apiserver-advertise-address=10.10.10.100 --pod-network-cidr=192.168.0.0/16 >> /root/kubeinit.log 2>/dev/null

echo  "Criando a pasta .kube para o user vagrant"
sudo --user=vagrant mkdir -p /home/vagrant/.kube

echo  "##### Criando o .kube/config a partir do admin.conf #####"
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/

echo "Deploy do cni weavenet"
kubectl --kubeconfig=/etc/kubernetes/admin.conf create -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" >/dev/null 2>&1

echo "Criando o comando join para os workers /joincluster.sh"
kubeadm token create --print-join-command > /joincluster.sh 2>/dev/null