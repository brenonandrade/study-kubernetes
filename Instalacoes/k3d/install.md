# k3d

Se vc irá deployar container, precisa ter um container runtime instalado.

Faça a instalação completa do docker com o script de conveniencia.

```bash
sudo apt-get install curl -y
curl -fsSL https://get.docker.com | bash
```

Instale o kubectl para poder acessar o cluster depois, pois o próprio k3d adiciona os parâmetros no kube/config.

```bash
sudo apt-get install kubectl -y
```

Uma vez instalado somente será necessário executar

```bash
k3d cluster create --config config.yaml
```

>Se conferir o arquivo de configuração observará que irá deployar 1 master e 3 workers.

Se quiser alterar do deploy conferir <https://k3d.io/v5.0.0/usage/configfile/>.

Execute kubectl get nodes e veja a saída.
