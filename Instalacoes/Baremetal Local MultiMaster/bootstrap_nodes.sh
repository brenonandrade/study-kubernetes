#!/bin/bash

echo "Desativando o swap"
sed -i '/swap/d' /etc/fstab
swapoff -a

echo "Desativando o firewall"
systemctl disable --now ufw >/dev/null 2>&1

echo "Ativando modulos do kernel necessarios para o containerd"
cat >>/etc/modules-load.d/containerd.conf<<EOF
overlay
br_netfilter
EOF
modprobe overlay
modprobe br_netfilter

echo "Adicionando configuracoes do kernel para o kubernetes"
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
EOF
sysctl --system >/dev/null 2>&1

echo "Instalando containerd com o systemd de cgroups"
apt update -qq >/dev/null 2>&1
apt install -qq -y containerd apt-transport-https >/dev/null 2>&1
mkdir /etc/containerd
containerd config default > /etc/containerd/config.toml
sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
systemctl restart containerd
systemctl enable containerd >/dev/null 2>&1

echo "Adicionando o repo do kubernetes"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - >/dev/null 2>&1
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main" >/dev/null 2>&1

echo "Instalando binarios do Kubernetes (kubeadm, kubelet and kubectl)"
apt install -qq -y kubeadm=1.24.0-00 kubelet=1.24.0-00 kubectl=1.24.0-00 >/dev/null 2>&1

echo "Ativando a autenticacao por ssh"
sed -i 's/^PasswordAuthentication .*/PasswordAuthentication yes/' /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
systemctl reload sshd

echo "Setando o password do root"
echo -e "kubeadmin\nkubeadmin" | passwd root >/dev/null 2>&1
echo "export TERM=xterm" >> /etc/bash.bashrc

#Essa parte poderia ir para o vagrantfile
echo "Atualizando os hosts no arquivo /etc/hosts"
cat >>/etc/hosts<<EOF
10.10.10.100  master.k8s.com     master
10.10.10.1    worker1.k8s.com    worker1
10.10.10.2    worker2.k8s.com    worker2
EOF