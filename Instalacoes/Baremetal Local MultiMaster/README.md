# Baremetal Local Multi Master com Vagrant

Os requisitos necessários:

- [vagrant](https://www.vagrantup.com/docs/installation)

 ```bash
 sudo apt-get install vagrant
 ```

- [virtualbox](https://www.virtualbox.org/wiki/Linux_Downloads)

 ```bash
 sudo apt-get install virtualbox
 ```

 Em versões mais recentes do virtual box, precisei mexer na configuração para liberar o range de ips.

```bash
sudo mkdir /etc/vbox
sudo echo "* 10.0.0.0/8 192.168.0.0/16" >> /etc/vbox/networks.conf
sudo echo "* 2001::/64" >> /etc/vbox/networks.conf
```

A topologia proposta aqui segue a seguinte projeto do Kubernetes.
<https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/ha-topology/>
![MultiMaster](../../resources/kubeadm-ha-topology-stacked-etcd.svg)

Para essa instalação vamos configurar 3 nodes masters, 2 nodes workers e 2 load balancer utilizando o HA proxy.

O motivo para 2 load balancer é que se somente um existir e ele falhar, toda a comunicação com o cluster também se perde.

>O que adianta ter alta disponibilidade para os nodes masters se não existir para o loadbalancer?

Para criar a alta disponibilidade também com o loadbalancer será usado o [Keepalived](https://www.keepalived.org/index.html).

Para uso do load balancer vamos utilizar o [HA Proxy](https://www.haproxy.org/).

Analisando o diagrama abaixo é possível entender melhor o que faremos.
![hakeealive](../../resources/multimaster-ha-keepaliver.png)

O arquivo [Vagrantfile](./Vagrantfile) contem a configuração das máquinas e as chamadas dos scripts necessários para a instalação.

## Explicando o Vagrantfile

Todas as máquinas executarão o script [bootstrap_ssh.sh](./bootstrap_ssh.sh)
Esse script nada mais faz do que copiar as chaves dentro de [files](./files/) para os users dentro do vm e autorizar a chave para acesso com o mando `vagrant ssh <nome da maquina>`

As maquinas criadas são loadbalancer1, loadbalancer2, master1, master2, master3, worker1, worker2.

É possível ter até:

- 239 workers(10.10.10.1 até 10.10.10.239)
- 9 masters (10.10.10.241 até .10.10.249)
- 4 load balancers 10.10.10.251 e o seguinte 10.10.10.254

e o endereço 10.10.10.250 ficará reservado para o keepalived

A primeira parte do vagrantfile é levantar os nossos load balancers(lb) pois ele será a liga
entre os masters e workers.

![lbconfig](../../resources/lbs_configs.png)

O arquivo responsável por essa configuração é o [bootstrap_lb.sh](./bootstrap_lb.sh) que será executado somente nas máquinas loadbalancers.

>Obs: `Os load balancers não fazem parte do cluster kubernetes`.

Todos os outros nodes que farão parte do cluster kubernetes executam o [bootstrap_nodes](./bootstrap_nodes.sh), sendo masters ou workers. Esses script contém a instalação do containerd e os binários do kubernetes, bem como alguns requisitos necessários que está na documentação do kubernetes.

A execução do primeiro master (10.10.10.241) estará separado dos demais, pois somente ele fará o init do cluster os demais somente farão o join para dentro do cluster.

O arquivo [bootstrap.sh](./bootstrap.sh) será o script que todos os nós precisam executar ao final do deploy de cada um dos nodes.

Por ultimo se analisar o vagrantfile ele caso seja master executará o script [bootstrap_master.sh](./bootstrap_master.sh) e caso seja worker executará o script [bootstrap_worker.sh](./bootstrap_worker.sh).

No script para o master ele cria o cluster e salva o comando join em um executável para que o worker faça a execução do mesmo atras de ssh para o master.

## Comandos básicos do vagrant

As máquinas definidas no vagrant file são: `loadbalancer1, loadbalancer2, master1, master2, master3, worker1, e worker2`

Subir todas as máquinas. Paciência...

```bash
vagrant up
```

![virtualbox](../../resources/virtualbox-multimaster.png)

Destruir todas as máquinas.

```bash
vagrant destroy
```

Subir uma máquina específica.

```bash
vagrant up master 
```

Destruir uma máquina específica.

```bash
vagrant destroy worker1 
```

Desligar todas as máquinas.

```bash
vagrant halt 
```

Desligar uma máquina específica.

```bash
vagrant halt worker1
```

Entrar em uma máquina utilizando ssh

```bash
vagrant ssh master
```

## Testes propostos

Faça alguns testes para verificar a alta disponibilidade.

Confira o keepalived no loadbalancer1 e no loadbalancer2

```bash
vagrant ssh loadbalancer1 # para entrar na máquina
ip -c -br a # já dentro da máquina para conferir as interfaces de rede
```

![keepalived](../../resources/keepalived.png)
Observe que foi criado um virtual network no loadbalancer1. Essa virtual network não esta presente no loadbalancer2, pois já foi declarado no loadbalancer1.

Pare o loadbalancer1 (`vagrant halt loadbalancer1`) e veja se tudo continua funcionando. Deveria estar pois foi para isso que criamos os dois load balancers. Também confira que o virtual network agora esta presente no loadbalancer2 (`vagrant ssh loadbalancer2` e depois `ip -c -br a`).

Pare o loadbalancer2 (`vagrant halt loadbalancer2`) e veja o cluster caindo fazendo um `kubectl get nodes` dentro de algum dos masters. Depois suba novamente os load balancers (`vagrant up loadbalancer1`  e `vagrant up loadbalancer2`) e confira se tudo voltando a funcionar fazendo o mesmo comando de get nodes.

Pare um master (`vagrant halt master1`) e confira se tudo está funcionando ainda. Nesse caso ficará com 2 masters. Entre em algum deles (`vagrant ssh master3`) e execute o `kubectl get nodes` para ver se tem resposta. Tem que funcionar...

Agora pare mais um master (`vagrant halt master2`) e veja o que acontecer se de 3 masters voce ficar só com 1. Execute o `kubectl get nodes` no master3.

>Deveria funcionar? Não, por causa do protocolo de consensus. Ele não sabe se ele caiu ou se o outro caiu. Estude novamente o consensus para entender sobre o quorum.

Levante um dos masters (`vagrant up master2`) que baixou e veja se volta a funcionar... Tem que voltar...
