#!/bin/bash

IP_ADDR=`ip -4 address show enp0s8 | grep inet | awk '{print $2}' | cut -d/ -f1`



echo "Fazendo o pull das imagens necessarios para os containers no master"
kubeadm config images pull >/dev/null 2>&1

echo "Inicializando o cluster"
kubeadm init --control-plane-endpoint="10.10.10.250:6443" --upload-certs --apiserver-advertise-address=$IP_ADDR --pod-network-cidr=192.168.0.0/16 >> /root/kubeinit.log 2>/dev/null

echo  "Criando a pasta .kube para o user vagrant"
sudo --user=vagrant mkdir -p /home/vagrant/.kube

echo  "##### Criando o .kube/config a partir do admin.conf #####"
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/

echo "Deploy do cni weavenet"
kubectl --kubeconfig=/etc/kubernetes/admin.conf create -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" >/dev/null 2>&1

echo "Criando o comando join para os workers /join_workers.sh"
kubeadm token create --print-join-command > /join_workers.sh

echo "Criando o comando join para os masters /join_masters.sh"
kubeadm token create --certificate-key $(kubeadm init phase upload-certs --upload-certs | tail -1 ) --print-join-command > /join_masters.sh