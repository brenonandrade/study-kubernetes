#!/bin/bash

echo "Executando o join do cluster"
apt install -qq -y sshpass >/dev/null 2>&1
sshpass -p "kubeadmin" scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no 10.10.10.241:/join_workers.sh /join_workers.sh 2>/dev/null
bash /join_workers.sh >/dev/null 2>&1