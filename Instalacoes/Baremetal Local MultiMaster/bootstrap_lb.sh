#!/bin/bash

echo "Atualizando o sistema"
apt-get update >/dev/null 2>&1
apt-get upgrade >/dev/null 2>&1

echo "Instalando o HA Proxy e Keepalived"
apt install -y keepalived haproxy 2>&1

echo "Criando o healthcheck"
cat >> /etc/keepalived/check_apiserver.sh <<EOF
#!/bin/sh

errorExit() {
  echo "*** $@" 1>&2
  exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q 10.10.10.250; then
  curl --silent --max-time 2 --insecure https://10.10.10.250:6443/ -o /dev/null || errorExit "Error GET https://10.10.10.250:6443/"
fi
EOF

chmod +x /etc/keepalived/check_apiserver.sh

echo "Criando o KeepAlive config em /etc/keepalived/keepalived.conf"
cat >> /etc/keepalived/keepalived.conf <<EOF
vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
  timeout 10
  fall 5
  rise 2
  weight -2
}

vrrp_instance VI_1 {
    state BACKUP
    interface enp0s8
    virtual_router_id 1
    priority 100
    advert_int 5
    authentication {
        auth_type PASS
        auth_pass mysecret
    }
    virtual_ipaddress {
        10.10.10.250
    }
    track_script {
        check_apiserver
    }
}
EOF

echo "Ativando o service do keepalived"
systemctl enable --now keepalived


echo "Configurando o HA Proxy"
cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend kubernetes-frontend
  bind *:6443
  mode tcp
  option tcplog
  default_backend kubernetes-backend

backend kubernetes-backend
  option httpchk GET /healthz
  http-check expect status 200
  mode tcp
  option ssl-hello-chk
  balance roundrobin
    server master1 10.10.10.241:6443 check fall 3 rise 2
    server master2 10.10.10.242:6443 check fall 3 rise 2
    server master3 10.10.10.243:6443 check fall 3 rise 2
EOF

echo "Ativando e reiniciando o HA Proxy"
systemctl enable haproxy
systemctl restart haproxy