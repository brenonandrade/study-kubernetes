# Study Kubernetes

<img src="./resources/kubernetes.png" alt="drawing" width="250"/>

O Kubernetes é um sistema de orquestração de contêineres amplamente utilizado para automatizar a implantação, o dimensionamento e o gerenciamento de aplicativos em contêineres. Ele foi desenvolvido pelo Google e é um projeto open source mantido pela Cloud Native Computing Foundation (CNCF).

As principais vantagens do uso do kubernetes são:

- Escalabilidade: Projetado para lidar com aplicativos em contêineres em grande escala. Ele oferece recursos de escalonamento automático para garantir que seus aplicativos possam lidar com grandes volumes de tráfego e que os recursos possam ser alocados dinamicamente à medida que a demanda aumenta.

- Portabilidade: Pode ser executado em qualquer ambiente de nuvem, local ou híbrido. Isso permite que as equipes de desenvolvimento e operações implantem aplicativos em contêineres em diferentes plataformas, sem precisar modificar o código do aplicativo.

- Flexibilidade: Oferece suporte a uma ampla variedade de ferramentas e serviços, incluindo bancos de dados, servidores web, caches, filas e muito mais. Isso permite que as equipes de desenvolvimento criem e implantem aplicativos em contêineres de maneira rápida e fácil.

- Automatização: Possui recursos avançados para automatizar e simplificar o processo de implantação e gerenciamento de aplicativos em contêineres. Ele lida com tarefas como balanceamento de carga, monitoramento, recuperação de falhas e escalonamento automático de forma automática, permitindo que as equipes de operações se concentrem em outras tarefas críticas.

- Segurança: O Kubernetes oferece recursos avançados de segurança, incluindo isolamento de rede, gerenciamento de identidade e acesso, e políticas de segurança. Isso garante que seus aplicativos em contêineres estejam protegidos contra ameaças externas e internas.

- Agrupamento lógico: Vários contêineres em um conjunto lógico e gerenciá-los como uma única entidade.

- Comunidade: Possui uma comunidade extensa e ativa.

Links:

- [Github Kubernetes](https://kubernetes.io/docs/home/)
- [Documentação Oficial](ttps://kubernetes.io/docs/home/)

## Como estudar?

Na pasta [manuscritos](./manuscritos/) temos toda a documentação do estudo.

Na pasta [Estudo](./Estudo/) teremos todos os laboratórios explorando os objetos do Kubernetes.

A pasta [Instalações](./Instalacoes/) possui os labotórios de como instalar o kubenetes de diferentes maneiras.

## Recomendações

Acredito que é importante saber instalar o kubernetes de várias maneiras. Estudar a arquitetura do kubernetes fará diferença em como vocẽ farã a instalação no seu ambiente futuro.

Somente faça uso de facilitadores que estão documentados aqui depois que entender bem o cli.

Entenda bem como funciona o arquivo .kube/config e sua estrutura.

Somente parta para o Helm (Gerenciador de pacote do kubernetes) depois que souber fazer tudo manualmente.

> A certificação do kuberentes CKA hoje é a mais valiosa do mercado DevOps.
