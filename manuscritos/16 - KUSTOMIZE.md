# Kustomize

Kustomize fornece uma solução para personalizar a configuração de recursos do Kubernetes livre de modelos e DSLs.
O Kustomize permite personalizar arquivos YAML brutos e sem modelo para várias finalidades, deixando o YAML original intocado e utilizável como está.
É como um make e um sed ao mesmo tempo.

>Já é nativo no kubernetes não precisando instalar nada

- É um nemo
- Mesmos manifestos para diferentes ambientes
- Fazer pequenos ajustes em diferentes casos
  - customizar algum arquivo de configuracao
  - aumentar ou diminuir replicas em um ambiente especifico
  - etc
- É simples, declarativo, e é um yaml
- trabalha com overlay que é a sobreposicão

Atua como um workspace do terraform, porém no kubernetes.

## Kustomize vs Helm

Vantagens do Kustomize:

- Não utilizar um gerenciador de pacote
- elimina o fardo de acompanhar vários manifestos para alterações específicas.
- O status quo seria manter todas as alterações em um manifesto no controle de origem, e pequenas diferenças sutis seriam difíceis de dizer o que aplicar a qual ambiente no Kubernetes com base apenas no nome do arquivo.
- pode ser executado nativamente pela interface de linha de comando (CLI) do Kubernetes, kubectl da versão 1.14.

Contras de Kustomize:

- curva de aprendizado e desafios na implementação e adoção.
- Não é fornecido com muitas convenções prontas para uso, permanecendo mais restrito a um design de mecanismo de modelo.
- O usuário final pode alterar qualquer coisa em um manifesto básico sem código de modelo explícito. Ao combinar por um arquivo, rótulo ou anotação, o usuário final precisa ter um conhecimento mais detalhado sobre o que e onde a alteração ocorrerá. YAMLs de base mal elaborados podem não ter anotações/seletores facilmente acessíveis para corresponder aos arquivos Kustomize.
- Quando comparado ao Helm, o Kustomize não é um ecossistema completo (por exemplo, um formato de repositório em si) e depende do SCM, como um repositório Git.

Pros do Helm:

-Ao permitir um ecossistema e uma convenção para que os usuários criem, compartilhem e distribuam instruções para instalar/desinstalar software, o consumo do software certamente aumenta.

- Abrangendo uma convenção e um repositório, os criadores de Helm Charts podem aplicar modelos e padrões aos consumidores. As equipes de DevOps ou Platform Engineering podem fornecer gráficos mais completos para os clientes internos.

>Dica é usar o kustomize para seus deployments pessoais das aplicacões e não na instalacao de alguma ferramenta que já tem o seu helm bem feito pela comunidade.

## Instalacao

<https://kubectl.docs.kubernetes.io/>

```bash
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
# Auto complete nesse caso para o zsh
kustomize completion zsh > ~/.oh-my-zsh/completions/_kustomize
```

## Como funciona?

Vamos pensar que temos 2 namespaces por exemplo que receberão os mesmos objetos:

- development
- production

```bash
❯ kubectl create ns production
namespace/production created

❯ kubectl create ns development
namespace/development created
```

> Vá para a pasta [Estudo/kustomize](../Estudo/kustomize/) no seu shell para executar os comandos

Primeiro precisamos criar uma estrutura de pasta para funcionar de acordo com essa idéia.

```bash
~/meuapp
├── base (Nesta Pasta Conterá os manifestos)
│   ├── deployment.yaml
│   └── service.yaml
└── overlays
    ├── development (será onde colocaremos somente as diferencas que irão para desenvolvimento)
    └── production (será onde colocaremos somente as diferencas que irão para producão)
```

```bash
❯ mkdir -p meuapp/base
❯ mkdir -p meuapp/overlays/production
❯ mkdir -p meuapp/overlays/development
```

Primeiro precisamos dos pacotes que são de base, ou seja, os deployments oficiais e colocar numa pasta chamada base

Vamos copiar os dois arquivos que seriam de base para dentro do nosso base.

```bash
❯ cp deployment.yaml meuapp/base
❯ cp deployment.yaml meuapp/base
```

Dentro da pasta base vamos criar um kustomization.yaml e apontar os recursos que serão kustomizados.

```bash
❯ cd meuapp/base
# o flag --auto detect já irá fazer todo o servico de colocar tudo para voce dentro
❯ kustomize create --autodetect 

❯ cat kustomization.yaml 
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- deployment.yaml
- service.yaml
```

Agora vamos criar um um arquivo kustomize.yaml dentro de cada um dos nosso ambientes

```bash
❯ cd ../overlays/production/
# Aqui já vamos criar um recuso passando onde estão as bases que iremos utilizar
❯ kustomize create --resources ../../base --namespace production

❯ cd ../development 
kustomize create --resources ../../base --namespace development

# Ficamos com essa arvore de arquivos
❯ tree ../../..
.
├── base
│   ├── deployment.yaml
│   ├── kustomization.yaml
│   └── service.yaml
└── overlays
    ├── development
    │   └── kustomization.yaml
    └── production
        └── kustomization.yaml
```

Nesse caso eu só personalizei o namespace, mas veja só [todos os recursos possíveis](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#kustomize-feature-list)

| namespace | string | add namespace to all resources |
|---|---|---|
| namePrefix | string | value of this field is prepended to the names of all resources |
| nameSuffix | string | value of this field is appended to the names of all resources |
| commonLabels | map[string]string | labels to add to all resources and selectors |
| commonAnnotations | map[string]string | annotations to add to all resources |
| resources | []string | each entry in this list must resolve to an existing resource configuration file |
| configMapGenerator | []ConfigMapArgs | Each entry in this list generates a ConfigMap |
| secretGenerator | []SecretArgs | Each entry in this list generates a Secret |
| generatorOptions | GeneratorOptions | Modify behaviors of all ConfigMap and Secret generator |
| bases | []string | Each entry in this list should resolve to a directory containing a kustomization.yaml file |
| patchesStrategicMerge | []string | Each entry in this list should resolve a strategic merge patch of a Kubernetes object |
| patchesJson6902 | []Patch | Each entry in this list should resolve to a Kubernetes object and a Json Patch |
| vars | []Var | Each entry is to capture text from one resource's field |
| images | []Image | Each entry is to modify the name, tags and/or digest for one image without creating patches |
| configurations | []string | Each entry in this list should resolve to a file containing Kustomize transformer configurations |
| crds | []string | Each entry in this list should resolve to an OpenAPI definition file for Kubernetes types |

Por exemplo se tivessemos deployamando as duas aplicacoes no mesmo namespace poderíamos incluir um prefixo com `--nameprefix prod-` e `--nameprefix dev-`

Isso já colocaria no arquivo o assim

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../../base
namespace: meuapp #Seria o mesmo do namespace escolhido
namePrefix: prod-
```

Dentro de cada uma das pastas vc ainda poderia especificar mais objetos e estes entrariam em resources.
Se vc observar a lista, vai ver que não é possível por exemplo fazer nada no service. Não poderiamos ter dois service identticos, logo ele deveria ter uma cópia em cada um dos repectivos ambientes com a porta definida corretamente. Vamos refazer...

```bash
❯ pwd
/home/david/gitlab/personal/study-kubernetes/Estudo/kustomize

# Copiando o services para os lugares corretos
❯ cp service.yaml meuapp/overlays/development 
❯ cp service.yaml meuapp/overlays/production

# Removendo o service que não pode ser base
❯ cd meuapp/base 
❯ rm service.yaml    
# Removendo o antigo kustomization.yaml e gerando um novo, para não ter o service.yaml que estava definido antes. Claro, vc poderia remover editando o arquivo
❯ rm kustomization.yaml 
# Gerando um novo
❯ kustomize create --autodetect
```

Agora temos o service dentro de cada um dos ambientes, pois não conseguimos resolver as portas pelo kustomize logo vamos editar esse objeto em cada um dos ambientes e adicionar esse recursos nos arquivos kustomization.yaml.

Production:

```yaml
## kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../../base
- service.yaml
namespace: production

## service.yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: default
spec:
  ports:
  - nodePort: 30000 # Porta do nodeport
    port: 80 # Porta do clusterIP
    protocol: TCP
    targetPort: 80 # Porta do Pod
  selector:
    app: nginx
  type: NodePort #Isso
```

Development:

```yaml
## kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../../base
- service.yaml
namespace: development

## service.yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: default
spec:
  ports:
  - nodePort: 30001 # Porta do nodeport
    port: 80 # Porta do clusterIP
    protocol: TCP
    targetPort: 80 # Porta do Pod
  selector:
    app: nginx
  type: NodePort #Isso
```

Vamos agora aplicat cada um dos ambientes.

Se quiser ver o que ele irá aplicar execute.

```bash
❯ kustomize build overlays/development 
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: development
spec:
  ports:
  - nodePort: 30001
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: development
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  strategy: {}
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
```

aplicando podemos usar o kubectl com o paramentro -k para falar que irá buscar o kustomimization.yaml

```bash
❯ kubectl apply -k overlays/development 
service/nginx created
deployment.apps/nginx created

❯ kubectl apply -k overlays/production 
deployment.apps/nginx created

❯ kubectl get deployments.apps -A
NAMESPACE      NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
development    nginx                    1/1     1            1           95s
production     nginx                    1/1     1            1           87s

❯ kubectl get service -A         
NAMESPACE      NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP                        PORT(S)                          10d
istio-system   istiod                 ClusterIP      10.43.38.11    <none>                             15010/TCP,15012/TCP,443/TCP,15014/TCP                       16h
development    nginx                  NodePort       10.43.58.103   <none>                             80:30001/TCP                 
```

vamos deletar e melhorar ainda mais isso.

```bash
❯ kubectl delete -k overlays/development 
service "nginx" deleted
deployment.apps "nginx" deleted

❯ kubectl delete -k overlays/production 
deployment.apps "nginx" deleted
```

Podemos fazer um reaproveitamento de código utilizando uma tag dentro de kustomization.yaml que dirá nossa estratégia de merge. Isso quer dizer que poderíamos ter qualquer objeto do kubernetes dentro de base, mas poderíamos declarar somente a diferenca em cada um dos ambiente. Confira e veja como fica em [meuapp2]

>Se o manifesto que vc quer fazer o merge estiver declarado dentro de resources ele não irá fazer o merge somente com a diferenca pois um recurso precisa ter seus valores necessário e não somente as linhas dos merges

```bash
❯ kubectl apply -k meuapp2/overlays/development 
service/nginx created
deployment.apps/nginx created

❯ kubectl apply -k meuapp2/overlays/production 
deployment.apps/nginx created

# observe os prefixos funcionando

❯ kubectl get deployments.apps -A | grep nginx
production     prod-nginx               2/2     2            2           5m8s
development    dev-nginx                1/1     1            1           4m29s

# em service.yaml na base a porta era 30000 e observe que não foi declarado o recurso como um todo.
❯ kubectl get svc -A | grep nginx
production     prod-nginx             NodePort       10.43.87.137    <none>                             80:31000/TCP                                                5m34s
development    dev-nginx              NodePort       10.43.130.177   <none>                             80:32000/TCP
```

> Uma coisa importante é que se mudado algo na base refletirar em todos os recursos que fazem heranca. Cuidado, com grandes poderes vem grandes responsabilidades.
