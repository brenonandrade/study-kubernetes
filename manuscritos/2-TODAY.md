# Kubernetes Hoje

O k8s como serviço nas clouds ajudaram bastante que pessoas com menos conhecimento fossem capazes de administrar um cluster kubernetes. Claro que ao custo de limites para não fazer *merda* mas o que fez a ferramenta ser adotada massivamente.

Optar por um cluster kubernetes raiz (baremetal) onde você constrói tudo do zero em cima de ec2 depende da maturidade da sua equipe e dos integrantes. Essa é a melhor forma de estudar, mas talvez não a de se trabalhar.

>Muitas vezes é melhor ter limites, mas ter paz vida.

Gerênciar um cluster k8s não é tão fácil assim, requer bastante conhecimento sobre seus componentes.

## Kuberentes como infra

Tenho uma teoria que o ideal é não contratar mais nada na cloud, só o serviço do kuberentes ou a força de processamento de máquinas para criar o seu cluster k8s.

Em cima do seu cluster kubernetes, vc irá deployar tudo o que precisa. Tudo que vc precisa roda em cima daquela estrutura, não ficando você refem de nenhum serviço da cloud.

Vamos validar essa teoria ao longo do caminho, pois sempre tem aparecido ferramentas que substituem um serviço na cloud.
