# Kubeconfig

O kube config nada mais é do que o arquivo yaml que contem todos as informações para conexão com o cluster.

Por padrão o arquivo chamado `config` fica dentro da pasta `.kube` do usuário em `$HOME/.kube/config`.

Durante o comando kubectl é possível se quiser passar o path onde vc criou esse arquivo com o parametro --kubeconfig.

```bash
kubectl --kubeconfig path1 get pods
```

Para evitar ficar passando toda hora esse --kubeconfig também é possível exportar uma variável de ambiente com o path ou mais de um path

```bash
export KUBECONFIG=path1:path2:path3
```

Se vc estiver trabalhando com vários clusters esse arquivo yaml poderá ter configuração de vários clusters também declarados em um único yaml, ou pode ser separado em diferentes arquivos cada uma das configurações. Porém irá gerar um certo incômodo ficar toda hora mudando o export. Além disso cada cluster pode ter vários namespaces e se vc esquecer de passar --namespaces para definir em qual namespace vai trabalhar poderá acarretar erros.

Para isso teria que fazer um comando começando sempre assim...

```bash
kubectl --namespace <NAMESPACE_NAME> --kubeconfig <PATH_TO_KUBECONFIG> <RESTO DO COMANDO>
```

É aqui que o `context` no kubernetes entra em cena. Vc pode setar um contexto, ou seja, um cluster e um namespace, e trabahar em cima dele. Isso de fato elimina ter que ficar passando --kubeconfig e --namespaces ou -n.

No Kubernetes , um contexto é uma entidade definida dentro do kubeconfig para alias de parâmetros de cluster com um nome legível por humanos. É importante entender que um contexto do Kubernetes se aplica apenas ao lado do cliente. O próprio servidor da API do Kubernetes não reconhece o contexto da mesma forma que faz outros objetos , como pods , implantações ou namespaces.

Vamos dar uma olhada em uma estrutura kubeconfig para ver como cluster, usuário e contexto se unem.

```yaml
apiVersion: v1
kind: Config
# esta seção lista todos os clusters aos quais você tem acesso. Cada cluster contém detalhes sobre a URL do servidor da API Kubernetes (onde o cluster aceita comandos do kubectl), autoridade de certificação e um nome para identificar o cluster
clusters:
- cluster:
    certificate-authority-data: ...
    server: ...
  name: k3d-k3s
contexts:
# Esta é a seção mais relevante para este tutorial, pois lista os contextos disponíveis. Cada contexto consiste no nome do cluster, usuário e namespace que você pode acessar ao invocá-lo. Observe que no exemplo, as informações do namespace são omitidas porque estão usando o namespace padrão.
- context:
    cluster: k3d-k3s
    user: admin@k3d-k3s
  name: k3d-k3s-default
# Qual o contexto está sendo usado no momento.
current-context: k3d-k3
preferences: {}
# A seção de usuários identifica cada usuário por um nome exclusivo e inclui as informações de autenticação relevantes, que podem ser certificados de cliente (como é o caso aqui), tokens de portador ou um proxy de autenticação
users:
- name: admin@k3d-k3s
  user:
    client-certificate-data: ...
    client-key-data: ...
```

Claro que é possível ir no arquivo config e editar manualmente tudo que quiser, mas se quiser utilizar a cli para evitar erros os comandos abaixo são os principais.

Se quiser ver o config que o kubectl esta enxergando no momento:

```bash
kubectl config view
```

Se quiser mudar o contexto:

```bash
kubectl config use-context <nome do contexto>
```

Se quiser ver em qual contexto está setado no momento:

```bash
kubectl config current-context 
```

Para adicionar um cluster por exemplo:

```bash
kubectl config set-cluster \
prod \
--server=https://1.2.3.4 \
--certificate-authority=xTsofu101...
```

Para adicionar um context por exemplo para o namespace production do cluster

```bash
kubectl config set-context \
prod \
--namespace=production \
--cluster=prod \
--user=admin
```

E para adicionar um user por exemplo:

```bash
kubectl config set-credentials \
admin \
--client-certificate=<CERTIFICATE> \
--client-key=<KEY>
```

Agora, sempre que você quiser acessar o cluster prod no namespace de production como o usuário admin, tudo o que você precisa fazer é usar o comando:

```bash
kubectl config use-context prod
```

>É importante lembrar que seu arquivo kubeconfig é um tipo de chave privada. Qualquer pessoa que tenha acesso a este arquivo poderá se autenticar em sua implantação do Kubernetes. É crucial que você tome todas as precauções para manter seu kubeconfig seguro e privado.

## Facilitando a vida

Existem dois binários que facilitam demais a vida e recomendo demais o uso destes pois evita ter que criar contextos com namespaces. Dessa forma vc utiliza o contexto somente para mudar de cluster e depois seta o namespace que vc quer utilizar.

Para instalar siga a documentação do github de acordo com o seu sistema operational ou os comandos abaixo para linux:

```bash
sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx
sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens
```

Siga a documentação para ter o auto complete que também ajuda a facilitar.

### kubectx

<https://github.com/ahmetb/kubectx>

Este binário permite mudar de contexto de forma rapida e visualizar quais contextos vc tem.

O comando puro `kubectx` lista todos os contextos definidos no config

Para mudar de contexto:

```bash
kubectx <CONTEXT_NAME>
```

### kubens

Este binário permite manter um namespace, praticamente ele coloca --namespace pra vc o tempo todo no comando kubectl.

o comando puro `kubens` lista todos os namepsaces do cluster.

Para definir um namespace de trabalho:

```bash
kubens <NAMESPACE_NAME>
```

## Facilitando o oh-my-zsh

Esse plugin do zsh <https://github.com/superbrothers/zsh-kubectl-prompt#with-a-plugin-manager> mostra no zsh o cluster e o namespace que vc está usando no momento. Isso ajuda a evitar erros durante o trabalho.

Somente clone o repositório dentro de ~/.oh-my-zsh/plugins e garanta que os plugins estão sendo carregados junto com o zsh.
