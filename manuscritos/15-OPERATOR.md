# Operator

Não há uma literatura oficial do kubernetes sobre o conceito de operator. Mas esse conceito é forte na comunidade e deve ser explorado: é um tipo específico de controller que objetiva facilitar a manutenção feita pelos sysadmins. Com um operator é possível, por exemplo, subir um cluster de Cassandra com um yaml muito pequeno, como o exemplo a seguir:

```yaml
apiVersion: cassandraoperator.instaclustr.com/v1alpha1
kind: CassandraCluster
"metadata":
  name: test-dc-cassandra
spec:
  size: 3
```

Algumas aplicações para operator:

- Monitoramento e alertas automáticos
- Atualizações automáticas de versão ao longo do tempo
- Instalação de recursos personalizados
- Fornecer escalonamento automático
- Gerenciamento do ciclo de vida
- Gerenciamento de armazenamento e backups

[OperatorHub](https://operatorhub.io/) é um site que agrega repositórios com códigos de controllers formatados para serem de simples instalação. O site define 5 níveis de maturidade da instalação de aplicações no ambiente kubernetes:

- 1 – Instalação básica
- 2 – atualização perfeita
- 3 – ciclo de vida completo
- 4 – com profundidade
- 5 – piloto automático

 O diagrama a seguir exibe os 5 níveis: até onde Helm Packages podem atender; até onde o ansible pode atender; e até onde esses operators (controllers) do OperatorHub podem atender.

 ![maturidade](../resources/operatorhubmaturidade.png)

## Boas praticas operator

- Operator são automatizadores de tarefa de sysadmin
- Operators podem ser utilizados no lugar de um helm package complexo
- Eles são softwares e possuem dinâmica particular para garantir sua qualidade
- Sempre se preocupe com a idempotência ao criar controllers
- Considerações específicas para o desenvolvimento
  - Faça a correta definição dos objetos na primeira volta
  - Na segunda volta valide a idempotência
  - Garanta que a estrutura seja absolutamente stateless
  - Crie finalizers (delete em cascata)
  - Muito cuidado com as consultas para descobrir o estado do cluster (isso pode sobrecarregar o cluster)
- Considerações específicas de plataforma de desenvolvimento
  - Utilize o Operator SDK (Ele já implementa boas práticas e facilita o trabalho)
  - Utilizar o Operator Utils
  - Prefira programar em Go Lang
- Só crie controllers se você tiver certeza da necessidade
  - Ele é a melhor solução técnica para o cenário?
  - Não é over-engineering ir por esse caminho?
  - Não há algo mais fácil para a necessidade?
  - Há na equipe pessoal para manter?
- Saiba que Controllers não são bala de prata – No Silver Bullet
  - Ele podem ajudar muito se bem produzidos
  - Mas sua produção é muito cuidadosa e pode gerar problemas que devem ser colocados na balança
