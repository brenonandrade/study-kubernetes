# Instalação

Vamos criar um cluster em nossa própria máquina. Dessa forma poderemos estudar como o kubernetes funciona de forma rápida e sem custo com cloud. Eu particulamente recomendo para desenvolvimento o `k3d` e depois o `kind`.
Confira a instalação do k3d em [Instalacoes/k3d](../Instalacoes/k3d/)

Outros métodos de instalação estão no [5.1-LOCAL.md](./5.1-LOCAL.md).

## Local Usando Vagrant

Nessa instalação vamos criar máquinas no virtualbox utilizando o vagrant e essas vms formarão o nosso cluster local do k8s. Vale muito a pena para aprender os requisitos necessários para uma instalação baremetal.

Para conferir essa instalação vá em [Instalacoes/Baremetal Local](../Instalacoes/Baremetal%20Local/)

## Local Multi Master Usando Vagrant

Nesse método vamos instalar um cluster com alta disponibilidade localmente usando o vagrant para criar nossas vms. Também é necessário duas vms para os loadbalancer.

Para conferir essa instalação vá em [Instalacoes/Baremetal Local MultiMaster](../Instalacoes/Baremetal%20Local%20MultiMaster/)

## Local Multi Master e Multi ETCD Usando Vagrant

Esse projeto é uma atualização do Multi Master, mas agora com o com mais 3 nodes para formar um cluster etd externo. Recomendo que entenda o projeto com o Multi Master antes de conferir esse.

Para conferir essa instalação vá em [Instalacoes/Baremetal Local MultiMaster MultiETCD](../Instalacoes/Baremetal%20Local%20MultiMaster%20MultiETCD/)

## Cluster BareMetal Multi Master

Os serviços gerenciados do Kubernetes facilitam muito a criação de um cluster, mas geralmente aplicam muitas configurações padrões que você não pode mexer. Os serviços gerenciados do Kubernetes fornecem o mais alto grau de automação (a criação e a operação são totalmente feitas para vc), mas a menor flexibilidade (você pode definir apenas as configurações que o provedor de serviços expõe por meio de sua API).

Por outro lado, instalar o Kubernetes manualmente sem scripts e ferramentas permite definir todas as configurações desejadas, mas requer muitas etapas manuais e propensas a erros. Talvez o ideal é encontrar o meio termo.

Para uma instalação por conta própria existem algumas ferramentas para ajudar a automação afim de diminuiro processos manuais.

---

## kops

<https://kops.sigs.k8s.io/>

O Kubernetes Operations (kops) é uma ferramenta CLI para “Instalação, atualizações e gerenciamento de nível de produção K8s”.

O que há de especial no kops é que ele não apenas instala o Kubernetes, mas também provisiona a infraestrutura na cloud que o cluster será executado. Inclusive é um projeto da [CF](https://landscape.cncf.io/card-mode?selected=kops).
Isso significa que você pode ir de zero a um cluster em execução com efetivamente um único comando.

### kubespray

<https://kubespray.io/#/>

kubespray é uma coleção de playbooks Ansible que automatizam a instalação do Kubernetes na infraestrutura `já existente`. Ele não provisiona a infra estrutura, somente configura.

Oferece um grande número de opcões de configuração, mas na verdade ele utiliza de fato o kubeadmin para prover seus automações. Não perderei muito tempo com esse metodo, somente gostaria de mostrar que existe.

### kubeadmin

O kubeadmin é de fato quem trabalha em mais baixo nível no cluster kubernetes, oferece menor grau de automação, mas entrega total flexibilidade, se vc souber utiliza-lo poderá configurar qualquer coisa.

O resultado é um cluster “mínimo viável” que você pode personalizar livremente de acordo com suas necessidades. Por exemplo, nem o cni ele instala.

As etapas do kubeadm são:

- Criação e distribuição de certificados de cliente e servidor para os componentes individuais do Kubernetes
- Criação e distribuição de arquivos kubeconfig para os componentes individuais do Kubernetes
- Iniciar o kubelet como um processo systemd
- Inicir os componentes restantes do Kubernetes como Pods na rede do host por meio do kubelet

voce pode declarar toda a configuração em um [arquivo de configuração](https://pkg.go.dev/k8s.io/kubernetes/cmd/kubeadm/app/apis/kubeadm/v1beta3) e executa-lo

O kubadmin é a solucão ideal pra ter o controle completo do cluster, mas não é uma operação de comando único (como é o caso de kops e kubespray).

Se vc analisar as instalações locais feitas localmente com o vagrant vai observar que existem etapas manuais.

- Provisionar a infra na qual você deseja criar o cluster
- Faça login em cada node e instalar o container runtime e os binarios do kubernetes
- Execute o kubeadm initcomando em um dos masters
- Execute o kubeadm joincomando em todos os outros nós

Criar um cluster Kubernetes com kubeadm é um processo tedioso e demorado quando feito manualmente.

---

## EKS

O EKS é o Kubernetes as a Service da AWS. Para ambientes de produção é altamente recomendavel o seu uso, principalmente por fatores de updates e complience que acontecem de forma fácil. Com esse serviço não se tem acesso aos nodes masters, somente é dado ao admin a governância da escalabilidade dos workers.

Exitem dois bons métodos, o eksctl e o terraform para fazer isso. Os dois casos são abordados em [Instalacoes/EKS](../Instalacoes/EKS/)

## GKE

Vamos montar um cluster k8s utilizando PaaS no GCP. Ainda não criado.

## AKS

Vamos montar um cluster k8s utilizando PaaS no Azure. Ainda não criado.

## HardWay

Vamos criar projeto kubernetes com IAC do jeito hard, entendendo todos os mistérios do kubernetes. Esse projeto é dedicado somente para estudo e entendimento a fundo de como todas as coisas do kuberentes funciona. Não recomendo de forma alguma utilizar esse sistema em produção, pois é mais dificil de manter em e exite uma equipe muito especializada.

e cada idéia que eu for tendo eu vou colocando aqui, pois eu quero destrincar essa ferramentar.
