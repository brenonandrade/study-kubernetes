# Start

Vamos entender algumas coisas antes de continuar.

A menor unidade é o pod. Não quer dizer que o pod tem só 1 container, apesar de geralmente ter, poderia ter um sidecar por exemplo. Você pode criar um pod sozinho, mas geralmente na vida normal vc cria um deployament.

Vc também pode criar um replicaset para controlar os pods, que vc criou sozinhos acima sem mesmo criar um deployment, mas não é normal fazer isso no dia a dia.

No dia a dia cria-se um deployment que dentro desse deployment vc declara o replicaset para garantir os pods e declara os pods, tudo em um único arquivo. Claro que vc pode criar um recurso separado do outro, mas ficará mais dificil de gerênciar depois.

Normalmente por questão de organização os services são declarados separadamente.

>É necessário entender que cada recurso pode ser criado sim separado, mas não é o que vc fará geralmente no dia a dia.

Entender a ligação entre eles é o grande segredo para entender como as coisas funcionam no kubernetes

Vale a pena uma revisão em [conceitos](./3-ARCH-CONCEPTS.md) antes de continuar.

>Os comando k aqui expostos são um alias para kubectl

---

## O que são services?

O service é o componente que irá apontar para o seu deployment. É através do service que acontecerá o redirecionamento das requisições externas para os containers do deployment. Um deployment sem um service não conseguirá ser acessado de fora do cluster.

## O que são controllers?

Controllers são recursos que gerenciam outros recursos.

Um pod é controlado pelo replicaset e não controla ninguem mais. Um pod possui um ip unico e todos os containers dentro dele respondem em portas diferentes mas com o mesmo ip.

É o replicaset que garante o número de pods em execução. Se um desses pods morrer, é o replicaset que irá levantar outro. O replicaset não é quem faz o balanceamento das requisições dos pods, quem faz isso é o kube-proxy. `O replicaset não é o responsável pelo escalonamento de containers, quem faz esse trabalho é o kube-scheduler`.

O deployment é o controller do replicaset/daemonset. Se o replicaset morrer quem irá gerenciar isso é o deployment. Outra situação é que se vc precisa subir uma atualização da sua aplicação, o deployment cria outro replicaset responsável pelos novos containers para poder ter a chance de fazer rollback se necessário, desativando o anterior mas não excluindo.

Todos esses recursos podem estar separados em namespaces diferentes. Os namespaces são o que separam os nossos recursos.

> Qualquer recurso criado senão for passado o namespace irá subir no default. Não é uma boa prática utilizar o default.

Esses são os conceitos base principais, que servirá para direcionar o aprendizado.

## Conferindo o cluster

```bash
### Para conferir informação do cluster
❯ k cluster-info  
Kubernetes control plane is running at https://510D33A8A6341FFE333F8294B58DEF38.gr7.us-east-1.eks.amazonaws.com
CoreDNS is running at https://510D33A8A6341FFE333F8294B58DEF38.gr7.us-east-1.eks.amazonaws.com/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

```bash
# Para diagnosticar o cluster podemos fazer um dump e conferir o relatório
# Esse comando gera uma saída json por padrão, mas pode ser mudado caso queira.
> ❯ kubectl cluster-info dump > ./Estudo/dump
❯ kubectl cluster-info dump --output yaml > ./Estudo/dump.yaml
```

## Conferindo os nodes

Para conferir os nodes de um cluster podemos simplesmente fazer.

```bash
❯ kubectl get nodes                              
NAME                           STATUS   ROLES    AGE   VERSION
ip-10-10-80-199.ec2.internal   Ready    <none>   43h   v1.23.9-eks-ba74326

# ou para mais informações sobre um node
❯ kubectl describe nodes ip-10-10-80-199.ec2.internal 
Name:               ip-10-10-80-199.ec2.internal
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/instance-type=t3.medium
                    beta.kubernetes.io/os=linux
                    eks.amazonaws.com/capacityType=ON_DEMAND
                    eks.amazonaws.com/nodegroup=infrastructure-ng-20220829122341729200000006
                    eks.amazonaws.com/nodegroup-image=ami-087b755b20f3ec965
                    eks.amazonaws.com/sourceLaunchTemplateId=lt-019177646aadc9d29
                    eks.amazonaws.com/sourceLaunchTemplateVersion=1
                    failure-domain.beta.kubernetes.io/region=us-east-1
                    failure-domain.beta.kubernetes.io/zone=us-east-1c
                    k8s.io/cloud-provider-aws=37bdd71e1c0a4ae3754939749add4b47
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=ip-10-10-80-199.ec2.internal
                    kubernetes.io/os=linux
                    name=infrastructure
                    node.kubernetes.io/instance-type=t3.medium
                    topology.kubernetes.io/region=us-east-1
                    topology.kubernetes.io/zone=us-east-1c
                    type=managed
Annotations:        alpha.kubernetes.io/provided-node-ip: 10.10.80.199
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Mon, 29 Aug 2022 09:25:06 -0300
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  ip-10-10-80-199.ec2.internal
  AcquireTime:     <unset>
  RenewTime:       Wed, 31 Aug 2022 04:45:05 -0300
Conditions:
  Type             Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----             ------  -----------------                 ------------------                ------                       -------
  MemoryPressure   False   Wed, 31 Aug 2022 04:41:18 -0300   Mon, 29 Aug 2022 09:25:06 -0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure     False   Wed, 31 Aug 2022 04:41:18 -0300   Mon, 29 Aug 2022 09:25:06 -0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure      False   Wed, 31 Aug 2022 04:41:18 -0300   Mon, 29 Aug 2022 09:25:06 -0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready            True    Wed, 31 Aug 2022 04:41:18 -0300   Mon, 29 Aug 2022 09:25:26 -0300   KubeletReady                 kubelet is posting ready status
Addresses:
  InternalIP:   10.10.80.199
  Hostname:     ip-10-10-80-199.ec2.internal
  InternalDNS:  ip-10-10-80-199.ec2.internal
Capacity:
  attachable-volumes-aws-ebs:  25
  cpu:                         2
  ephemeral-storage:           31444972Ki
  hugepages-1Gi:               0
  hugepages-2Mi:               0
  memory:                      3965444Ki
  pods:                        110
Allocatable:
  attachable-volumes-aws-ebs:  25
  cpu:                         1930m
  ephemeral-storage:           27905944324
  hugepages-1Gi:               0
  hugepages-2Mi:               0
  memory:                      3410436Ki
  pods:                        110
System Info:
  Machine ID:                 ec2089763f50e2ccc79ee1bda0d8e419
  System UUID:                ec208976-3f50-e2cc-c79e-e1bda0d8e419
  Boot ID:                    91c8ad25-df46-4b74-ae8f-388621b8b17f
  Kernel Version:             5.4.209-116.363.amzn2.x86_64
  OS Image:                   Amazon Linux 2
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.6.6
  Kubelet Version:            v1.23.9-eks-ba74326
  Kube-Proxy Version:         v1.23.9-eks-ba74326
ProviderID:                   aws:///us-east-1c/i-084c5c81422bdcbc5
Non-terminated Pods:          (4 in total)
  Namespace                   Name                      CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                      ------------  ----------  ---------------  -------------  ---
  kube-system                 aws-node-9mjnn            25m (1%)      0 (0%)      0 (0%)           0 (0%)         43h
  kube-system                 coredns-d5b9bfc4-tlrj5    100m (5%)     0 (0%)      70Mi (2%)        170Mi (5%)     2d22h
  kube-system                 coredns-d5b9bfc4-v2lgr    100m (5%)     0 (0%)      70Mi (2%)        170Mi (5%)     2d22h
  kube-system                 kube-proxy-p7dd4          100m (5%)     0 (0%)      0 (0%)           0 (0%)         43h
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource                    Requests    Limits
  --------                    --------    ------
  cpu                         325m (16%)  0 (0%)
  memory                      140Mi (4%)  340Mi (10%)
  ephemeral-storage           0 (0%)      0 (0%)
  hugepages-1Gi               0 (0%)      0 (0%)
  hugepages-2Mi               0 (0%)      0 (0%)
  attachable-volumes-aws-ebs  0           0
Events:                       <none>
```

>Uma curiosidade é que quando pedimos para descrever um node no eks ele não mostra os masters somente os worker nodes.

Se eu mudar o contexto para o meu k3d ele mostrará os masters também.

```bash
❯ k config use-context k3d-k3d-cluster 
Switched to context "k3d-k3d-cluster".

❯ k get nodes      
NAME                       STATUS   ROLES                  AGE   VERSION
k3d-k3d-cluster-server-0   Ready    control-plane,master   40h   v1.23.6+k3s1
k3d-k3d-cluster-agent-1    Ready    <none>                 40h   v1.23.6+k3s1
k3d-k3d-cluster-agent-0    Ready    <none>                 40h   v1.23.6+k3s1
k3d-k3d-cluster-agent-2    Ready    <none>                 40h   v1.23.6+k3s1
```

## Namespace

Namespaces serve para separar em grades os seus deployments. Por exemplo podemos separar produção de desenvolvimento se estivem rodando no mesmo cluster. Também é possível definir cotas de recursos para um determinado namespace. Vamos imaginar que um namespace develop pode usar do cluster somente uma quantidade de cpu, memória e disco e o resto ficaria para outro namespace de production.

A maior parte dos recursos do k8s pertecem a algum namespace, mas nem todos. Por exemplo persistente volumes, nodes não pertecem a nenhum namespace. Confira com os comandos abaixo.

```bash
❯ k api-resources --namespaced=true 

❯ k api-resources --namespaced=false 
```

```bash
❯ k create namespace production
namespace/production created

❯ k create namespace develop   
namespace/develop created

❯ k get ns
NAME              STATUS   AGE
default           Active   41h
kube-system       Active   41h
kube-public       Active   41h
kube-node-lease   Active   41h
production        Active   18s
develop           Active   11s

❯ k describe ns production 
Name:         production
Labels:       kubernetes.io/metadata.name=production
Annotations:  <none>
Status:       Active

No resource quota.

No LimitRange resource.
```

>`ns`é um alias para namespaces

Observe que não foi definido cotas para o namespace de produção, mas vamos definir para o namespace de desenvolvimento.

```bash
❯ k delete ns production 
namespace "production" deleted

❯ k delete ns develop 
namespace "develop" deleted

# namespace default não pode ser deletado
❯ k delete namespace default 
Error from server (Forbidden): namespaces "default" is forbidden: this namespace may not be deleted
```

`Uma boa prática é criar seus recursos utilizando um arquivo yaml e aplicar este arquivo mesmo que seja um recurso simples.`

```bash
❯ k apply -f ns.yaml 
namespace/develop created
namespace/production created
```

## ApiVersion e Kind

<https://kubernetes.io/docs/reference/using-api/>

Todo arquivo yaml precisa da api version definido. A api do k8s está em constante desenvolvimento logo o versionamento dessa api também se faz necessário. Fique sempre atento a estas versões pois podem ser incompatíveis com a versão do k8s que vc esta utilizando.

Falando um pouco de versões:

- `alpha` são as primeiras candidatas as novas funcionalidades e podem conter bugs e sem garantia de funcionalidade no futuro.

- `beta` são versões que os testes progrediram além do nível alpha e que o recurso de fato será incluído no k8s, mas a maneira de como funciona e as declarações do yaml ainda podem mudar.

- `stable` não contem alpha e beta no nome e são seguros de usar.

<https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/>
Abaixa uma tabela de amostra para a versão 1.25 retirada do link acima.

| Group                        | Version          |
|------------------------------|------------------|
| admissionregistration.k8s.io | v1               |
| apiextensions.k8s.io         | v1               |
| apiregistration.k8s.io       | v1               |
| apps                         | v1               |
| authentication.k8s.io        | v1               |
| authorization.k8s.io         | v1               |
| autoscaling                  | v1, v2, v2beta2  |
| batch                        | v1               |
| certificates.k8s.io          | v1               |
| coordination.k8s.io          | v1               |
| core                         | v1               |
| discovery.k8s.io             | v1               |
| events.k8s.io                | v1               |
| flowcontrol.apiserver.k8s.io | v1beta2, v1beta1 |
| internal.apiserver.k8s.io    | v1alpha1         |
| networking.k8s.io            | v1, v1alpha1     |
| node.k8s.io                  | v1               |
| policy                       | v1               |
| rbac.authorization.k8s.io    | v1               |
| scheduling.k8s.io            | v1               |
| storage.k8s.io               | v1, v1beta1      |

> Os grupos podem conter mais de um tipo de objeto que são chamado de `kind` que veremos daqui pra frente.

## Metadata

Todo recurso no kubernetes possue um bloco de metadata e é nesse bloco que temos alguns dados para identificar o recurso. Podemos definir quantos metadados("tags"), quisermos para poder fazer um filtro quando for procurar ou apontar para um recurso. Um desses metadados é muito importante e chamado de `labels`. Observe de agora em diante que um recurso como namespace para se identificado precisou de um metadado assim como o pod precisará e o replicaset para apontar para o pod irá fazer um match(filtro) para pods. Os metadados definem os identificadores do objeto declarado.

## Criando um Pod

Uma vez que o pod possui seu próprio ip e pode rodar mais de um container, apesar de não ser usual, podemos imaginar que ele poderia ser considerado uma "máquina virtual". É a menor objeto do k8s.

>Lembrando que sempre é possível declarar qualquer recurso usando parâmetros para o kubectl, mas com o arquivo yaml, que é declarativo, é muito mais fácil e recomendado. Um comando é dificil de ser lembrado e um arquivo é simplesmente fácil de ser aplicado.

O arquivo [pod.yaml](../Estudo/pods/pod.yaml) contém nosso yaml para definição de pod e comentado para entendimento.

Confira no arquivo [POD.md](../Estudo/pods/POD.md) como trabalhar com o pods.

## Criando um replicaset

Replicaset é um dos tipos de controller de replicação que existem no kubernetes. O replicaset é o mais utilizado, mas é necessário conhecer também o daemonset. Uma leitura em [REPLICATION.md](../Estudo/replication/REPLICATION.md) é necessária para entender melhor sobre eles e suas diferenças.

Não é prática da vida real criar replicaset sem deployment e no fim da documentação irá entender o porquê.

Os arquivos de criação de todos os modelos estão na pasta [replicaset](../Estudo/replication/).

## Criando um deployment

Agora vamos iniciar de fato o que se faz no mundo real. Um deployment costuma ter que é necessário para o ciclo de vida da aplicação. Porém o deployment para receber requests externas do cluster, precisa ter um service mais pra frente.

Toda documentação sobre deployemnts esta na pasta [deployment](../Estudo/deployment/).

Este é um dos principais temas que é necessário saber no kubernetes.

## Crindo um service par nosso deploy

Antes de criar um service é bom entender a diferença e alguns conceitos e palavras chaves dentro do service. Existem mais de um tipo de service e quais são as diferenças entre eles?

Um deploymente somente consegue ser acesso através de um service. É o service quem expõe o nosso deployment para o mundo.

Toda documentação sobre service esta na pasta [service](../Estudo/service/).

## Limitando Recursos

Existem alguns objetos que aplicam regras sobre outros. Temos o limitRange e resourceQuotas para isso

Alguns exemplos estão em [limits](../Estudo/limits;).

## Principais Comandos

### Get e Describe

Todo objeto é possível ser analisado usando o subcomando `get e describe`

````bash
# vai mostrat todos os deployments do namespace default.
# Lembrando que vc pode trocar o namespace corrent facilmente usando o kubens
❯ k get deploy
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   3/3     3            3           22h

❯ k get deployments --all-namespaces
NAMESPACE     NAME      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   coredns   2/2     2            2           19d
production    nginx     3/3     3            3           22h

# o parametro -o wide aumenta mais a saída mostrando mais informações
❯ k get deployments --all-namespaces -o wide
NAMESPACE     NAME      READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                                                                       SELECTOR
kube-system   coredns   2/2     2            2           19d   coredns      602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/coredns:v1.8.7-eksbuild.2   eks.amazonaws.com/component=coredns,k8s-app=kube-dns
production    nginx     3/3     3            3           22h   nginx        nginx:stable                                                                 app=nginx-pod

# veja que com o -o wide pegando os nodes vc consegue ver o internalip os-image kenel... 
❯ k get nodes
NAME                          STATUS   ROLES    AGE     VERSION
ip-10-10-64-15.ec2.internal   Ready    <none>   7d16h   v1.23.9-eks-ba74326

❯ k get nodes -o wide
NAME                          STATUS   ROLES    AGE     VERSION               INTERNAL-IP   EXTERNAL-IP   OS-IMAGE         KERNEL-VERSION                 CONTAINER-RUNTIME
ip-10-10-64-15.ec2.internal   Ready    <none>   7d16h   v1.23.9-eks-ba74326   10.10.64.15   <none>        Amazon Linux 2   5.4.209-116.363.amzn2.x86_64   containerd://1.6.6

# Da mesma maneira podemos ver a saída em modo yaml com muito mais informações descrevendo o objeto. Observe que com o comando get ele pega o objeto por isso mostra todo o manifesto yaml apiversion, kind, etc.
# Se não for passado nenhum parâmetro depois de nodes ele vai pegar todos, nesse caso só tem um
# caso queira ver a saída em json é só passar -o json
k get nodes -o yaml 
apiVersion: v1
items:
- apiVersion: v1
  kind: Node
  metadata:
    annotations:
      alpha.kubernetes.io/provided-node-ip: 10.10.64.15
      node.alpha.kubernetes.io/ttl: "0"
      volumes.kubernetes.io/controller-managed-attach-detach: "true"
    creationTimestamp: "2022-09-06T20:53:49Z"
    labels:
      beta.kubernetes.io/arch: amd64
      beta.kubernetes.io/instance-type: t3.medium
      beta.kubernetes.io/os: linux
      eks.amazonaws.com/capacityType: ON_DEMAND
      eks.amazonaws.com/nodegroup: infrastructure-ng-20220906172410375900000006
      eks.amazonaws.com/nodegroup-image: ami-087b755b20f3ec965
      eks.amazonaws.com/sourceLaunchTemplateId: lt-0bcd8bd6ec605fb71
      eks.amazonaws.com/sourceLaunchTemplateVersion: "4"
      failure-domain.beta.kubernetes.io/region: us-east-1
      failure-domain.beta.kubernetes.io/zone: us-east-1c
      k8s.io/cloud-provider-aws: 37bdd71e1c0a4ae3754939749add4b47
      kubernetes.io/arch: amd64
      kubernetes.io/hostname: ip-10-10-64-15.ec2.internal
      kubernetes.io/os: linux
      name: infrastructure
      node.kubernetes.io/instance-type: t3.medium
      topology.kubernetes.io/region: us-east-1
      topology.kubernetes.io/zone: us-east-1c
      type: managed
    name: ip-10-10-64-15.ec2.internal
    resourceVersion: "3731181"
    uid: 1a692a73-3d74-4bd1-bb16-f79ea4ad842f
  spec:
    providerID: aws:///us-east-1c/i-0a2863a43ff748b25
  status:
    addresses:
    - address: 10.10.64.15
      type: InternalIP
    - address: ip-10-10-64-15.ec2.internal
      type: Hostname
    - address: ip-10-10-64-15.ec2.internal
      type: InternalDNS
    allocatable:
      attachable-volumes-aws-ebs: "25"
      cpu: 1930m
      ephemeral-storage: "27905944324"
      hugepages-1Gi: "0"
      hugepages-2Mi: "0"
      memory: 3410436Ki
      pods: "110"
    capacity:
      attachable-volumes-aws-ebs: "25"
      cpu: "2"
      ephemeral-storage: 31444972Ki
      hugepages-1Gi: "0"
      hugepages-2Mi: "0"
      memory: 3965444Ki
      pods: "110"
    conditions:
    - lastHeartbeatTime: "2022-09-14T13:42:08Z"
      lastTransitionTime: "2022-09-06T20:53:48Z"
      message: kubelet has sufficient memory available
      reason: KubeletHasSufficientMemory
      status: "False"
      type: MemoryPressure
    - lastHeartbeatTime: "2022-09-14T13:42:08Z"
      lastTransitionTime: "2022-09-06T20:53:48Z"
      message: kubelet has no disk pressure
      reason: KubeletHasNoDiskPressure
      status: "False"
      type: DiskPressure
    - lastHeartbeatTime: "2022-09-14T13:42:08Z"
      lastTransitionTime: "2022-09-06T20:53:48Z"
      message: kubelet has sufficient PID available
      reason: KubeletHasSufficientPID
      status: "False"
      type: PIDPressure
    - lastHeartbeatTime: "2022-09-14T13:42:08Z"
      lastTransitionTime: "2022-09-06T20:54:09Z"
      message: kubelet is posting ready status
      reason: KubeletReady
      status: "True"
      type: Ready
    daemonEndpoints:
      kubeletEndpoint:
        Port: 10250
    images:
    - names:
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/amazon-k8s-cni@sha256:19dacc4b46485c85ddfbee3545d34948bad08ce318c0fa997541546786aa7bc4
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/amazon-k8s-cni:v1.10.4-eksbuild.1
      sizeBytes: 111420357
    - names:
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/amazon-k8s-cni-init@sha256:27aebee799d28adf2557ce8999863ce4171d451dceb3bc5c9c2b3e041316272c
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/amazon-k8s-cni-init:v1.10.4-eksbuild.1
      sizeBytes: 111263887
    - names:
      - docker.io/library/nginx@sha256:4535aaa94ae5316180fac74c56035921280275d0ec54282253e1a95536d62a05
      - docker.io/library/nginx:stable
      sizeBytes: 56765692
    - names:
      - docker.io/library/nginx@sha256:f2dfca5620b64b8e5986c1f3e145735ce6e291a7dc3cf133e0a460dca31aaf1f
      sizeBytes: 56742351
    - names:
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/kube-proxy@sha256:699b73079601ce8231aefb1a60588b8900dcbb5bb5a1160df835a58c4130e482
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/kube-proxy:v1.23.7-minimal-eksbuild.1
      sizeBytes: 25577927
    - names:
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/coredns@sha256:e6a38c89a5e40f7259269d3f4816ee0b4f71748f61f2fdadbc1d94e21a24a77b
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/coredns:v1.8.7-eksbuild.2
      sizeBytes: 13918387
    - names:
      - 602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/pause:3.5
      sizeBytes: 298689
    nodeInfo:
      architecture: amd64
      bootID: 297c13f7-6a6f-487d-bbbb-3ab6969bf102
      containerRuntimeVersion: containerd://1.6.6
      kernelVersion: 5.4.209-116.363.amzn2.x86_64
      kubeProxyVersion: v1.23.9-eks-ba74326
      kubeletVersion: v1.23.9-eks-ba74326
      machineID: ec269ac97c1d68de4151ef3fbfe66dbb
      operatingSystem: linux
      osImage: Amazon Linux 2
      systemUUID: ec269ac9-7c1d-68de-4151-ef3fbfe66dbb
kind: List
metadata:
  resourceVersion: ""

# Da mesma maneira podemos ver os chave valores guardados no etcd pelo objeto yaml sem pegar de fato o objeto usando o describe.
❯ k describe nodes ip-10-10-64-15.ec2.internal 
Name:               ip-10-10-64-15.ec2.internal
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/instance-type=t3.medium
                    beta.kubernetes.io/os=linux
                    eks.amazonaws.com/capacityType=ON_DEMAND
                    eks.amazonaws.com/nodegroup=infrastructure-ng-20220906172410375900000006
                    eks.amazonaws.com/nodegroup-image=ami-087b755b20f3ec965
                    eks.amazonaws.com/sourceLaunchTemplateId=lt-0bcd8bd6ec605fb71
                    eks.amazonaws.com/sourceLaunchTemplateVersion=4
                    failure-domain.beta.kubernetes.io/region=us-east-1
                    failure-domain.beta.kubernetes.io/zone=us-east-1c
                    k8s.io/cloud-provider-aws=37bdd71e1c0a4ae3754939749add4b47
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=ip-10-10-64-15.ec2.internal
                    kubernetes.io/os=linux
                    name=infrastructure
                    node.kubernetes.io/instance-type=t3.medium
                    topology.kubernetes.io/region=us-east-1
                    topology.kubernetes.io/zone=us-east-1c
                    type=managed
Annotations:        alpha.kubernetes.io/provided-node-ip: 10.10.64.15
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Tue, 06 Sep 2022 17:53:49 -0300
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  ip-10-10-64-15.ec2.internal
  AcquireTime:     <unset>
  RenewTime:       Wed, 14 Sep 2022 10:44:23 -0300
Conditions:
  Type             Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----             ------  -----------------                 ------------------                ------                       -------
  MemoryPressure   False   Wed, 14 Sep 2022 10:42:08 -0300   Tue, 06 Sep 2022 17:53:48 -0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure     False   Wed, 14 Sep 2022 10:42:08 -0300   Tue, 06 Sep 2022 17:53:48 -0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure      False   Wed, 14 Sep 2022 10:42:08 -0300   Tue, 06 Sep 2022 17:53:48 -0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready            True    Wed, 14 Sep 2022 10:42:08 -0300   Tue, 06 Sep 2022 17:54:09 -0300   KubeletReady                 kubelet is posting ready status
Addresses:
  InternalIP:   10.10.64.15
  Hostname:     ip-10-10-64-15.ec2.internal
  InternalDNS:  ip-10-10-64-15.ec2.internal
Capacity:
  attachable-volumes-aws-ebs:  25
  cpu:                         2
  ephemeral-storage:           31444972Ki
  hugepages-1Gi:               0
  hugepages-2Mi:               0
  memory:                      3965444Ki
  pods:                        110
Allocatable:
  attachable-volumes-aws-ebs:  25
  cpu:                         1930m
  ephemeral-storage:           27905944324
  hugepages-1Gi:               0
  hugepages-2Mi:               0
  memory:                      3410436Ki
  pods:                        110
System Info:
  Machine ID:                 ec269ac97c1d68de4151ef3fbfe66dbb
  System UUID:                ec269ac9-7c1d-68de-4151-ef3fbfe66dbb
  Boot ID:                    297c13f7-6a6f-487d-bbbb-3ab6969bf102
  Kernel Version:             5.4.209-116.363.amzn2.x86_64
  OS Image:                   Amazon Linux 2
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.6.6
  Kubelet Version:            v1.23.9-eks-ba74326
  Kube-Proxy Version:         v1.23.9-eks-ba74326
ProviderID:                   aws:///us-east-1c/i-0a2863a43ff748b25
Non-terminated Pods:          (7 in total)
  Namespace                   Name                      CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                      ------------  ----------  ---------------  -------------  ---
  kube-system                 aws-node-bhm6w            25m (1%)      0 (0%)      0 (0%)           0 (0%)         7d16h
  kube-system                 coredns-d5b9bfc4-8mmxj    100m (5%)     0 (0%)      70Mi (2%)        170Mi (5%)     7d16h
  kube-system                 coredns-d5b9bfc4-r58vq    100m (5%)     0 (0%)      70Mi (2%)        170Mi (5%)     7d16h
  kube-system                 kube-proxy-s8ldm          100m (5%)     0 (0%)      0 (0%)           0 (0%)         7d16h
  production                  nginx-6f9ccf5886-2mlft    250m (12%)    500m (25%)  256Mi (7%)       512Mi (15%)    22h
  production                  nginx-6f9ccf5886-gfhhb    250m (12%)    500m (25%)  256Mi (7%)       512Mi (15%)    22h
  production                  nginx-6f9ccf5886-ss7vd    250m (12%)    500m (25%)  256Mi (7%)       512Mi (15%)    22h
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource                    Requests     Limits
  --------                    --------     ------
  cpu                         1075m (55%)  1500m (77%)
  memory                      908Mi (27%)  1876Mi (56%)
  ephemeral-storage           0 (0%)       0 (0%)
  hugepages-1Gi               0 (0%)       0 (0%)
  hugepages-2Mi               0 (0%)       0 (0%)
  attachable-volumes-aws-ebs  0            0
Events:                       <none>
````

### Edit

Qualquer recurso pode ser editado a quente com o subcomando `edit`. Nesse caso irá abrir um editor estilo vim e pode ser mudado e salvado que o recurso já irá ser mudado.

> Só por curiosidade, o comando edit irá mostrar para ser editado a mesma saída do comando **get -o yaml** e não do describe. Os nomes dos objetos não podem ser mudados, nesse caso é necessário criar outro.

````bash
❯ k edit deploy nginx

❯ edit ns production

❯ k edit nodes ip-10-10-64-15.ec2.internal
````

### Set

Também é possível passar através de um cli o que vc quer editar sem necessáriamente abrir o editor usando o comando `set`. Essse comando é interessante para pipeliens por exemplo, pois vc não irá ter interação com o editor.

````bash
## deployment/nginx é para saber em qual deploy ele irá mudar e nginx é o nome do container do pod declarado no template
❯ kubectl set image deploy/nginx nginx=nginx:1.17.0
deployment.apps/nginx image updated

❯ k describe pod | grep Image:
    Image:          nginx:1.17.0
    Image:          nginx:1.17.0
    Image:          nginx:1.17.0

````

O comando `set` pode atualizar varias coisas de modo cli, vale a pena dar uma conferida para conhecimento. Lembrando que o completion do kubectl ajuda bastante a encontrar os comandos subsequentes como mostrado abaixo.

````bash
❯ k set                                                                                                                     env             -- Update environment variables on a pod template
image           -- Update the image of a pod template
resources       -- Update resource requests/limits on objects with pod templates
selector        -- Set the selector on a resource
serviceaccount  -- Update the service account of a resource
subject         -- Update the user, group, or service account in a role binding or cluster role binding
````

### Replace

Eu particulamente não gosto muito de mudar objeto via edit pois prefiro ter um arquivo que sei que está fiel ao que está provisionado. Mude o arquivo que tem o recurso declarado em yaml e utilise o `replace`. Se vc usar o apply em um recurso que já existe ele dará erro avisando que o recurso já existe mesmo que o arquivo tenha sofrido modificações.

````bash
❯ k replace -f fulldeployment.yaml
deployment.apps/nginx replaced
````

### Scale

Um recurso util também é o `scale` para aumentar o numero de replica de um pod.

````bash
❯ k scale deployment nginx --replicas=10
deployment.apps/nginx scaled

~/pessoais/study-kubernetes/Estudo/deployment develop !2 ?3                                                                                  ⎈ us-east-1-prod-cluster/production  1.2.9 11:52:03
❯ k get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-77989d974d-4sq8s   0/1     Pending   0          61s
nginx-77989d974d-7q2n7   1/1     Running   0          61s
nginx-77989d974d-cfd68   0/1     Pending   0          61s
nginx-77989d974d-k2n92   1/1     Running   0          61s
nginx-77989d974d-qxlzg   1/1     Running   0          61s
nginx-77989d974d-r9ssx   0/1     Pending   0          61s
nginx-77989d974d-s87qz   1/1     Running   0          7m43s
nginx-77989d974d-tkfxl   0/1     Pending   0          61s
nginx-77989d974d-v4fpk   1/1     Running   0          7m38s
nginx-77989d974d-vxqk5   1/1     Running   0          7m40s
````

> O que aconteceu aqui? Não tinha recurso para levantar os pods, por isso alguns ficaram pendentes.

Uma curiosidade é que o scale pode auterar as replicas de um replicaset. Se vc mudar o valor de um replicaset vinculado a um deployment o que acontecerá?

````bash
❯ k get deployments.apps nginx
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   5/5     5            5           25h

# Podemos confirmar que o rs é o descrito abaixo e tem 5 replicas... 
❯ k describe deploy nginx | grep NewReplicaSet:
NewReplicaSet:   nginx-77989d974d (5/5 replicas created)

❯ k get rs
NAME               DESIRED   CURRENT   READY   AGE
nginx-64bdbf6bbb   0         0         0       3h1m
nginx-6f9ccf5886   0         0         0       25h
nginx-77989d974d   5         5         5       135m
nginx-99c569c99    0         0         0       137m

# Agora vamos scalar para 1 direto no replicaset

❯ k get rs                                        
NAME               DESIRED   CURRENT   READY   AGE
nginx-64bdbf6bbb   0         0         0       3h2m
nginx-6f9ccf5886   0         0         0       25h
nginx-77989d974d   5         5         1       136m
nginx-99c569c99    0         0         0       138m

# Ele foi pra 1 mas o desired está em 5... logo ele vai subir de novo como mostra abaixo 1 minuto depois
❯ k get rs
NAME               DESIRED   CURRENT   READY   AGE
nginx-64bdbf6bbb   0         0         0       3h3m
nginx-6f9ccf5886   0         0         0       25h
nginx-77989d974d   5         5         5       137m
nginx-99c569c99    0         0         0       139m
````

> O controller do replicaset é o deployment, logo se vc quer escalar fixadamente um replicaset que tem um deployment faça direto no deployment. Isso confirma o que eu falei de que não se cria replicaset ou pods na vida real, mas sim deployments.

### Exec

Esse é método que vc pode usar para executar um comando dentro de um pod. O comando vem seguido de `-- comando`. Se vc quiser entrar no pod caixa em algum terminal como por exemplo -- bash

````bash
❯ k get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-77989d974d-266gz   1/1     Running   0          4m39s
nginx-77989d974d-dkr2k   1/1     Running   0          4m39s
nginx-77989d974d-k2n92   1/1     Running   0          134m
nginx-77989d974d-k6bn5   1/1     Running   0          4m39s
nginx-77989d974d-nhhwm   1/1     Running   0          4m39s

~/pessoais/study-kubernetes/Estudo/deployment develop !2 ?3                                                                                  ⎈ us-east-1-prod-cluster/production  1.2.9 14:06:38
❯ k exec -ti nginx-77989d974d-266gz -- bash
root@nginx-77989d974d-266gz:/# ls
````

O objeto que carrega os containers são pods, logo vc só pode se atachar em 1 pod. Se vc precisa cair dentro de um pod de um deployemnt mas não interessa qual for ele vai te levar no pod mais antigo sempre.

````bash
❯ k exec -ti deployments/nginx -- bash
root@nginx-77989d974d-k2n92:/# 

# Da mesma forma se vc pedir para cair em um replicaset ele vai te levar para o pod mais antigo daquele replicaset, daemonset, etc
❯ k exec -ti replicasets/nginx-77989d974d -- bash 
root@nginx-77989d974d-266gz:/# 
````

O exec só executa o comando em um pod por vez, mas podemos melhorar isso e executar o comando em todos os pods que tenham a label app=nginx-pod que criamos no nosso deployment. Claro que já entra um pouco de conhecimento de shell nesse caso, mas nada muito complicado.

````bash
# apt-get update em todos os pods
kubectl get pods -l app=nginx-pod --field-selector=status.phase=Running -o name | xargs -I{} kubectl exec {} -- apt-get update

# instalando o stress
kubectl get pods -l app=nginx-pod --field-selector=status.phase=Running -o name | xargs -I{} kubectl exec {} -- apt-get install stress -y

kubectl get pods -l app=nginx-pod --field-selector=status.phase=Running -o name | xargs -I{} kubectl exec {} -- stress --vm 1 --vm-bytes 500 --cpu 1 -q &

❯ kubectl get pods -l app=nginx-pod --field-selector=status.phase=Running -o name | xargs -I{} kubectl exec {} -- bash -c "stress --vm 1 --vm-bytes 500MB --cpu 1 > /dev/null 2> /dev/null &"
````

### Logs

O comando logs pega a saída que o pod jogou no console e mostra pra voce. Nesse exemplo abaixo eu tinha executado um cronjob que veremos mais pra frente e somente imprimi data/hora e um echo.

````bash
❯ k logs meu-job-27728108-vkrvl
Tue Sep 20 15:08:01 UTC 2022
Estudo de Kubernetes
````

## Labels

As labels não são objetos, são marcadores de objetos, e estão presentes em todos em seus metadados para poderem ser identificados. Quando tempos um replicaset ele sabe quais os podes ele devem replicar aplicando um filtro usando as labels. Quando tempos um service, ele sabe para quais pods ele gerará endpoints aplicando as labels. Quando queremos criar os pods em um node com caracteristicas específicas também é através das labels que ele irá filtrar.

> Use e abuse de labels a vontade. Um objeto pode ter quantas labels voce quiser mesmo que não as use muito para selecionar recursos, mas podem ser usadas para filtrar recursos durantes os comandos do kubectl.

Uma situação interessante que observo durante os estudos é que usa-se as mesmas labels para identificar um objeto que é o controller do outro. Dessa forma é mais difícil entender a diferença entre o que esta sendo filtrado, porém é mais interessante para manter depois que existe um entendimento mais aprimorado. Um exemplo disso é o quando o service faz o filtro, se vc usar o mesmo nome e não souber do que se trata, ficará em dúvida se o service filtra deployment, replicaset ou pods, pois todos teriam a mesma label, apesar dos objetos serem diferentes.

As labels podem ser adicionadas juntos aos manifestos yamls, mas também podem ser adicionados pelo cli kubectl.

Vamos adicionar labels nos nossos nodes filtrar

````bash
❯ k get nodes 
NAME                          STATUS   ROLES    AGE     VERSION
ip-10-10-35-53.ec2.internal   Ready    <none>   2d20h   v1.23.9-eks-ba74326
ip-10-10-70-52.ec2.internal   Ready    <none>   25h     v1.23.9-eks-ba74326

❯ k label nodes ip-10-10-35-53.ec2.internal disk=ssd
node/ip-10-10-35-53.ec2.internal labeled

❯ k label nodes ip-10-10-70-52.ec2.internal disk=hdd
node/ip-10-10-70-52.ec2.internal labeled

❯ k get nodes -l disk=ssd
NAME                          STATUS   ROLES    AGE     VERSION
ip-10-10-35-53.ec2.internal   Ready    <none>   2d20h   v1.23.9-eks-ba74326

# Nesse caso só vai filtrar os nodes que tem a labels disk não interessa o valor
❯ k get nodes -l disk
NAME                          STATUS   ROLES    AGE     VERSION
ip-10-10-35-53.ec2.internal   Ready    <none>   2d20h   v1.23.9-eks-ba74326
ip-10-10-70-52.ec2.internal   Ready    <none>   25h     v1.23.9-eks-ba74326

# Com o -L em maiusculo vai gerar uma coluna mostrando o valor dessas labels
❯ k get nodes -L disk 
NAME                          STATUS   ROLES    AGE     VERSION               DISK
ip-10-10-35-53.ec2.internal   Ready    <none>   2d20h   v1.23.9-eks-ba74326   ssd
ip-10-10-70-52.ec2.internal   Ready    <none>   25h     v1.23.9-eks-ba74326   hdd

# Se quiser filtrar todas as labels de um recurso
❯ k label nodes ip-10-10-35-53.ec2.internal --list
eks.amazonaws.com/nodegroup-image=ami-087b755b20f3ec965
disk=ssd
beta.kubernetes.io/instance-type=t3.medium
node.kubernetes.io/instance-type=t3.medium
kubernetes.io/hostname=ip-10-10-35-53.ec2.internal
name=infrastructure
failure-domain.beta.kubernetes.io/region=us-east-1
eks.amazonaws.com/nodegroup=infrastructure-ng-20220906172410375900000006
type=managed
failure-domain.beta.kubernetes.io/zone=us-east-1b
eks.amazonaws.com/sourceLaunchTemplateId=lt-0bcd8bd6ec605fb71
k8s.io/cloud-provider-aws=37bdd71e1c0a4ae3754939749add4b47
kubernetes.io/arch=amd64
topology.kubernetes.io/region=us-east-1
kubernetes.io/os=linux
beta.kubernetes.io/os=linux
eks.amazonaws.com/sourceLaunchTemplateVersion=4
topology.kubernetes.io/zone=us-east-1b
eks.amazonaws.com/capacityType=ON_DEMAND
beta.kubernetes.io/arch=amd64

❯ k label pod nginx-77989d974d-rgcl4 --list
pod-template-hash=77989d974d
app=nginx-pod

❯ k label deploy nginx --list              
app=nginx

❯ k label rs nginx-77989d974d --list 
pod-template-hash=77989d974d
app=nginx-pod

# Para sobreescrever uma label use o --ovewrite
❯ k label nodes ip-10-10-35-53.ec2.internal disk=nvme --overwrite 
node/ip-10-10-35-53.ec2.internal labeled
````

Aproveitando essa parte se vc adicionar dentro do template do pod um nodeSelector ele somente vai criar um pod no node que tiver a labels specificada. Dentro de fulldeployment mude o spec adicionando as duas ultimas linhas mostradas no yaml abaixo. Ou edite o arquivo.

> Se não existir um node com o label especificado não serã poss~ivel criar o container.

````yaml
spec:
  progressDeadlineSeconds: 600
  replicas: 3
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: nginx-pod
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: nginx-pod
    spec:
      containers:
      - image: nginx:latest
        imagePullPolicy: Always
        name: nginx
        ports:
        - containerPort: 80
          name: web
          protocol: TCP
        resources:
          requests:
            memory: 256Mi
            cpu: 256m

          limits:
            memory: 512Mi
            cpu: 500m

        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      # Essas duas linhas abaixo devem ser adicionadas
      nodeSelector:
        disk: nvme
````

Agora scale 3 e veja onde foram criados os pods

````bash
❯ k scale deploy nginx --replicas 3
deployment.apps/nginx scaled

# Observe que todos os 3 foram para o node que tinha nvme como disk
❯ k get pods -o wide                          
NAME                     READY   STATUS    RESTARTS   AGE   IP             NODE                          NOMINATED NODE   READINESS GATES
nginx-7594bbbdc4-8jlxt   1/1     Running   0          37m   10.10.40.137   ip-10-10-35-53.ec2.internal   <none>           <none>
nginx-7594bbbdc4-ftg2w   1/1     Running   0          11s   10.10.34.36    ip-10-10-35-53.ec2.internal   <none>           <none>
nginx-7594bbbdc4-l5kx9   1/1     Running   0          10m   10.10.51.187   ip-10-10-35-53.ec2.internal   <none>           <none>
````

Para remover as labels coloquei um - depois da label e escolha o recurso ou todos que tiverem essa label do mesmo tipo

````bash
❯ k label nodes disk- --all
node/ip-10-10-35-53.ec2.internal unlabeled
node/ip-10-10-70-52.ec2.internal unlabeled
````
