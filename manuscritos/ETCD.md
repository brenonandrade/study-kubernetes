# ETCD

Distribued etc Directory

De forma geral o ETCD é o "banco de dados do kubernetes". Ele guarda toda informação do cluster, através de chave-valor, dentro do ETCD. É um projeto da CNCF inclusive e open source.

Geralmente o ETCD costuma ser o calo dos administradores do cluster, por isso vale a pena um estudo aprofundado sobre ele e entender de fato como ele funciona pois ele é um dos serviços centrais do Kubernetes.

Somente o API server no kubernetes se comunica com o ETCD

O cluster etcd utiliza o protocolo `Raft Consensus` para alta disponibilidade do cluster. Não existe limite para o tamanho do cluster, mas estudo mostram que o valor ideal é 5 nós no cluster para suportar até 2 nós falhando. Acima disso, a replicação de dados entre os nós começa a prejudicar a performance.

>Pergunta? Por que separar o etcd dos masters se eu posso aproveitar e ter 5 máquinas para ter 2 pontos de falha tanto no master quanto no etcd ao contraŕio de ter 6 máquinas (3 pra cada) para ter somente 1 ponto de falha em cada...?

`Eu particulamente` acho que mesmo financeiramente rodando em cloud não faz muito sentido. Lembrando que se vc usa 3 zonas diferentes na cloud e vai colocar uma máquina em cada um delas, se uma zona parar, vai parar tanto para uma máquina master quanto para uma máquina etcd. Ainda existe o fator que gerenciar um cluster kubernetes já é trabalho difícil e ainda precisar separar o etcd pode gerar mais trabalho ainda. Tem que colocar na balança.

É necessário que os relógios dos sistemas estejam sincronizado. Tolera alguma diferença de horário, mas `se a diferença for grande o estado do cluster ETCD vai para read-only`.

`etcdctl é o cli` para executar qualquer comando do cluster.

| Porta | protocolo | Descricao                                                         |
|-------|-----------|-------------------------------------------------------------------|
| 2380  | tcp       | Comunicação entre os membros do cluster para replicação dos dados |
| 2379  | tcp       | Comunicação com os clientes                                       |

![etcdarch](../resources/etcdarch.png)

## Requisitos Minimos para Produção

Tudo depende do volume de transações que o cluster etcd irá trabalhar.

<https://etcd.io/docs/v3.5/op-guide/hardware/>

- SSD (Obrigatório)
  - O etcd escreve muito no disco e se o disco não for ssd o tempo de iowait (espero da cpu) fica muito alto prejudicado demais um cluster kubernetes por exemplo.
- 4gb de ram
  - O etcd mantém muitos dados em cache de memória. Dependendo o da quantidade de requisições por segundo é bom aumentar o valor da memória. Para um cluster pequeno de kubernetes em produção 4gb é irá segurar bem.
- Rede Gigabit pelo menos

## Instalação

Voce pode simplesmente baixar o binário em <https://github.com/etcd-io/etcd/releases/> e colocá-lo em `/usr/local/bin`

ou seguir os comandos, e até mesmo formar um script com a sequencia abaixo

```bash
ETCD_VER=v3.5.4

echo "Removendo instalações anteriores"
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf /tmp/etcd && mkdir -p /tmp/etcd
sudo rm -rf /usr/local/bin/etcd*

echo "Fazendo o download e extraindo para a pasta /tmp/etcd e removendo o zip"
curl -L "https://github.com/etcd-io/etcd/releases/download/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz" -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

echo "Movendo os binários para /usr/local/bin para entrar no path"
sudo mv /tmp/etcd/etcd /usr/local/bin
sudo mv /tmp/etcd/etcdctl /usr/local/bin
sudo mv /tmp/etcd/etcdutl /usr/local/bin

echo "Removendo arquivos temporarios"
rm -rf /tmp/etcd

echo "Conferindo..."
etcd --version
etcdctl version
etcdutl version

rm -rf /tmp/etcd
```

## Quick Start

Abra dois terminais

No primeiro execute o comando abaixo se seguiu a instalação e deixe rodando o serviço

```bash
etcd
```

No terminal apenas execute alguns comandos com o etcdctl

## etcdclt

os comando mais básicos do etcdctl são

- `etcdctl put <chave> <valor>` para criar uma chave e associar um valor
- `etcdctl get <chave>` para recuperar uma valor de uma chave
- `etcdctl del <chave>` apargar uma chave

>O comando put se for executado em cima uma chave já existente ele sobreescreve

```bash
etcdctl put nome "david"
OK

etcdctl get nome
nome
david

etcdctl put nome "joao" 
OK

etcdctl get nome
nome
joao

etcdctl del nome
# saida vazia
etcdctl get nome
# saida vazia
```
