# Advanced

Aqui vamos aprender algumas coisas um pouco mais avançadas no kuberentes que fazem diferença o conhecimento.

## Acessando a api do kubernetes (proxy)

o kubectl é um binário go que através de certificados autenticados consegue acessar a api do kuberentes. Para ter acesso a essa api, o kubectl pode gerar um proxy para que vc acesse na sua máquina essa api.

````bash
❯ kubectl proxy --port=8080
Starting to serve on 127.0.0.1:8080
````

E no seu navegador pode ser visto essa api.

![api](../resources/proxy-api.png)

Faça uma navegação e vai ver os recursos que foram criados.

![api-namespace-production](../resources/api-namespace-production.png)

## Como fazer um canary deploy?

Agora que já entendemos como funciona um deploy vamos tentar fazer um canary deploy que é bem requisitado durante os conhecimentos de devops.

Os services fazem um selector para os pods que possuem um determinado label. Se vc criar dois deployments cujo os pods possuem o mesmo label, por mais que sejam deployments diferents, será selecionados ambos os pods. Essa é a magina que vamos fazer.

Vamos criar um deployment com uma versão, e escalar para 5 e outro deployment com uma versão e escalar para 1. Isso quer dizer que 6 pods estarão no endpoint do service entregando o serviço, porém um deles tem versão diferente.

Na pasta [canary](../Estudo/deployment/canary/) Temos os arquivo necessário para a aplicação.

Siga os as etapas [canary](../Estudo/deployment/canary/canary.md) para aprender um pouco mais como fazer.

## Cronjobs (alias cj)

Os cronjobs são uma maneira de agendar tarefa no cluster kuberentes. Esse sistema é herdado do crontab do linux, porém de uma maneira clusterizada. Por exemplo se precisamos fazer um backup de alguma coisa, não é necessário ter o serviço rodando o tempo todo, mas somente ser instanciado em determinado momento do dia, semana, mes, ano, etc.

Faça uma leittura em [chronjob.yaml](../Estudo/cronjob/cronjob.yaml) e veja os comentários ali feitos.

> Aproveite para ver o uso de variavel de ambiente no container.

````bash
# Quando eu fui criar a primeira vez usei o apiversion batch/v1beta1 e o próprio k8s me avisou que essa api mudou.. olha que legal.. 
❯ k create -f cronjob.yaml
Warning: batch/v1beta1 CronJob is deprecated in v1.21+, unavailable in v1.25+; use batch/v1 CronJob
cronjob.batch/meu-job created
# Fiz a altera;áo e dei um replace
❯ k replace -f cronjob.yaml   
cronjob.batch/meu-job replaced

❯ k get cj
NAME      SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
meu-job   */1 * * * *   False     1        14s             2m23s

❯ k describe cj meu-job 
Name:                          meu-job
Namespace:                     production
Labels:                        <none>
Annotations:                   <none>
Schedule:                      */1 * * * *
Concurrency Policy:            Allow
Suspend:                       False
Successful Job History Limit:  3
Failed Job History Limit:      1
Starting Deadline Seconds:     <unset>
Selector:                      <unset>
Parallelism:                   <unset>
Completions:                   <unset>
Pod Template:
  Labels:  <none>
  Containers:
   meu-job:
    Image:      busybox
    Port:       <none>
    Host Port:  <none>
    Args:
      /bin/sh
      -c
      date; Estudo de Kubernetes; sleep 30
    Environment:     <none>
    Mounts:          <none>
  Volumes:           <none>
Last Schedule Time:  Tue, 20 Sep 2022 11:20:00 -0300
Active Jobs:         <none>
# Veja que ele mostra os eventos de quando foi executado
Events:
  Type    Reason            Age    From                Message
  ----    ------            ----   ----                -------
  Normal  SuccessfulCreate  2m45s  cronjob-controller  Created job meu-job-27728058
  Normal  SawCompletedJob   2m11s  cronjob-controller  Saw completed job: meu-job-27728058, status: Complete
  Normal  SuccessfulCreate  105s   cronjob-controller  Created job meu-job-27728059
  Normal  SawCompletedJob   71s    cronjob-controller  Saw completed job: meu-job-27728059, status: Complete
  Normal  SuccessfulCreate  45s    cronjob-controller  Created job meu-job-27728060
  Normal  SawCompletedJob   12s    cronjob-controller  Saw completed job: meu-job-27728060, status: Complete

# O cronjobs cria jobs e podemos ver o que aconteceu
❯ k get jobs
NAME               COMPLETIONS   DURATION   AGE
meu-job-27728060   1/1           33s        3m8s
meu-job-27728061   1/1           33s        2m8s
meu-job-27728062   1/1           33s        68s
meu-job-27728063   0/1           8s         8s

❯ k logs meu-job-27728060-ad23w
Tue Sep 20 15:08:01 UTC 2022
Estudo de Kubernetes

❯ k delete cj meu-job 
cronjob.batch "meu-job" deleted
# ou k delete -f cronjob.yaml
````

## Secrets

Secrets no kubernetes na verdade é um objeto que guarda um valor encriptado em base64, que na verdade não é muito seguro, mas quebra um galho até que uma solução melhor seja definida.

Dê uma lida em [secrets.md](../Estudo/secrets/secret.md) e siga os passos para entender como funciona guardar chave valor em um objeto secret.

> Mais uma vez reforçando, não é seguro quanto deveria ser.

## Configmap (alias cm)

É uma forma de vc adicionar configurações nos seus container e pods sem ter que buildar uma imagem com tudo o que vc precisa dentro. Dessa forma facilita o build e deixa para o kuberentes resolver isso.

Este é um recurso muito utilizado e diminui bastante a declaração de coisas nos containers.

Veja um estudo básico em [configmap](../Estudo/configmap/).

## Initcontainers

<https://kubernetes.io/docs/concepts/workloads/pods/init-containers/>

O InitContainer não é um objeto, mas sim uma configuração do pod. É um container que vai executar uma ação específica antes do seu container executar de fato. Esse container vai preparar o terreno para o container propriamente dito. Um cenário seria popular dados antes para que o container processe por exemplo.

- Os contêineres de inicialização sempre são executados até a conclusão.
- Cada contêiner de inicialização deve ser concluído com êxito antes que o próximo seja iniciado.

Se o contêiner de inicialização de um pod falhar, o kubelet reiniciará repetidamente esse contêiner de inicialização até que seja bem-sucedido. No entanto, se o Pod tiver restartPolicyNever e um contêiner de inicialização falhar durante a inicialização desse Pod, o Kubernetes tratará o Pod geral como com falha.

Vamos criar um pod com nginx e trocar a o index dele.

> Sómente ao termino dos initcontainers que os containers irão subir. Essa é a diferença básica, pois se não fosse isso poderíamos criar dois containers normalmente.

````bash
❯ k create -f podinitcontainer.yaml                       
pod/test-initcontainer created

❯ k get pod
NAME                 READY   STATUS    RESTARTS   AGE
test-initcontainer   1/1     Running   0          5s

❯ k exec -ti  test-initcontainer -- sh
Defaulted container "nginx" out of: nginx, install (init)
# cd /usr/share/nginx/html
# ls
index.html
# cat index.html
<!doctype html><html lang=en class=no-js><head class=live-site><meta name=ROBOTS content="INDEX, FOLLOW"><link rel=alternate hreflang=zh-cn href=https://kubernetes.io/zh-cn/><link rel=alternate hreflang=ko href=https://kubernetes.io/ko/><link rel=alternate hreflang=ja href=https://kubernetes.io/ja/><link rel=alternate hreflang=fr href=https://
## Parte removida para diminuir a leiura aqui
<script src=/js/main.min.5c0bf7f21dc4f66485f74efbbeeff28a7e4f8cddaac1bae47043159c922ff3a3.js integrity="sha256-XAv38h3E9mSF9077vu/yin5PjN2qwbrkcEMVnJIv86M=" crossorigin=anonymous></script></body></html># exit

## Veja os detalhes do initcontainer e a ordem dos eventos
❯ k describe pod test-initcontainer 
Name:             test-initcontainer
Namespace:        production
Priority:         0
Service Account:  default
Node:             ip-10-10-35-53.ec2.internal/10.10.35.53
Start Time:       Wed, 21 Sep 2022 15:32:16 -0300
Labels:           app=nginx
Annotations:      kubernetes.io/psp: eks.privileged
Status:           Running
IP:               10.10.40.137
IPs:
  IP:  10.10.40.137
Init Containers:
  install:
    Container ID:  containerd://8caf4a4e5ee560e0ad67d2b54c9e975e21ae9247d68943f54314076f0e726302
    Image:         busybox
    Image ID:      docker.io/library/busybox@sha256:ad9bd57a3a57cc95515c537b89aaa69d83a6df54c4050fcf2b41ad367bec0cd5
    Port:          <none>
    Host Port:     <none>
    Command:
      wget
      -O
      /work-dir/index.html
      http://kubernetes.io
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Wed, 21 Sep 2022 15:32:17 -0300
      Finished:     Wed, 21 Sep 2022 15:32:17 -0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-wx5kf (ro)
      /work-dir from workdir (rw)
Containers:
  nginx:
    Container ID:   containerd://02267149ec4f8490dd9547e525f1099b05bdb0280f204d7b3df80049233cd692
    Image:          nginx
    Image ID:       docker.io/library/nginx@sha256:0b970013351304af46f322da1263516b188318682b2ab1091862497591189ff1
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Wed, 21 Sep 2022 15:32:18 -0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     200m
      memory:  500Mi
    Requests:
      cpu:        100m
      memory:     200Mi
    Environment:  <none>
    Mounts:
      /usr/share/nginx/html from workdir (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-wx5kf (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  workdir:
    Type:       EmptyDir (a temporary directory that shares a pods lifetime)
    Medium:     
    SizeLimit:  <unset>
  kube-api-access-wx5kf:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  106s  default-scheduler  Successfully assigned production/test-initcontainer to ip-10-10-35-53.ec2.internal
  Normal  Pulling    105s  kubelet            Pulling image "busybox"
  Normal  Pulled     105s  kubelet            Successfully pulled image "busybox" in 153.359889ms
  Normal  Created    105s  kubelet            Created container install
  Normal  Started    105s  kubelet            Started container install
  Normal  Pulling    105s  kubelet            Pulling image "nginx"
  Normal  Pulled     104s  kubelet            Successfully pulled image "nginx" in 157.155083ms
  Normal  Created    104s  kubelet            Created container nginx
  Normal  Started    104s  kubelet            Started container nginx

❯ k delete po test-initcontainer 
pod "test-initcontainer" deleted
````

## Readness e Liveness Probe

Esse dois blocos de parâmetros pertencem a definição de container de cada pod.

ReadnessProbe define o momento em que seu container esta pronto para receber requisição. Geralmente tenta fazer um healthcheck e em caso de resposta estará pronto. Para isso é necessário conhecer a aplicação que está sendo iniciada e o seu tempo de demora.

LivenessProbe defini como o pod irá conferir se este container está ou não em execução. Geralemente também usa o healthcheck mas colocando intervalos para checagem e um tempo de espera para iniciar os processos.

> Muitas vezes se os dois blocos forem definidos, um pode interferir no outro se não for bem configurado.

## RBAC

Quando falamos de RBAC estamos falando de gerênciamento de usuários e permissões no k8s. Este é um estudo que deve ser aprofundado, pois irá fazer parte do seu dia a dia.

Veja um estudo simples inicialmente em [rbac](../Estudo/rbac/). Todo esse conceito será aplicado no estudo do ingress.

## Como fazer um autoscale de replicaset?

<https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/#:~:text=You%20can%20use%20topology%20spread,well%20as%20efficient%20resource%20utilization>.

 balanceando o cluster
<https://itnext.io/keep-you-kubernetes-cluster-balanced-the-secret-to-high-availability-17edf60d9cb7>
