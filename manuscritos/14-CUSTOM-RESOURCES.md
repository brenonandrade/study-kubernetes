# CUSTOM RESOURCES

<https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/>\
<https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/>

Primeiramente uma das capacidades do Kubernetes é a sua extensibilidade. Toda vez que aplicamos um objeto no kubernetes através de um kind específico algum mecanismo interno dele atua para que desempenhe a função correspondente. Por exemplo: um deployment age criando um replicaset que por sua vez atua controlando replicas de pods. Portanto, acontece que há no kubernetes: controllers/operators e custom resources, que são formas diferentes de pensar essa questão

Ao trabalhar com recursos do Kubernetes, como implantações, serviços e pods , às vezes você precisa personalizá-los para atender aos seus casos de uso, o que de outra forma seria difícil de conseguir com as opções disponíveis. É aqui que o Kubernetes CustomResourceDefinitions (CRDs) pode ajudá-lo a estender a API do Kubernetes e alcançar o resultado desejado.

Para criar um recurso no kubernetes precisamos de duas parametros inciais no manifesto:

- apiVersion
  - aponta o endpoint da api que irá usar
  - representa um grupo de recursos
- kind
  - seleciona o tipo do recurso que irá ser usado dentro do grupo de recusos do apiVersion.

````yaml
apiVersion: v1
kind: Namespace
metadata:
  name:  develop
....
---
apiVersion: apps/v1
kind: DaemonSet
````

## Custom Resource Definition – CRD

É possível criar novos kinds no kubernetes com campos e características específicas: para fazer isso é necessário criar um Custom Resource Definition ou CRD. Em seguida, ao fazer isso um novo resource ficará disponível através do api-resources do kubernetes, `acessível via HTTP por aplicações`.

Vamos mostrar o que temos na nossa instalação antes de criar um.
Já podemos observar os shortnames que temos por exemplo para services temos svc, a apiVersion que é o nosso endpoint(grupo) o kind que é o tipo, de todos os recursos que temos disponíveis para usar.

Também podemos observar o namespaced que significa que um recurso é definido por namespace como por exemplo pod. No caso de recursos que são a nível de cluster como por exemplo os persistant volume, namespace, etc possuem essa tag como false.

>Vale a pena mencionar que os próprios CRDs não contêm nenhuma lógica. O objetivo principal de um CRD é fornecer uma maneira de criar, armazenar e expor a API do Kubernetes para objetos personalizados.

```bash
kubectl api-resources 
NAME                              SHORTNAMES   APIVERSION                             NAMESPACED   KIND
bindings                                       v1                                     true         Binding
componentstatuses                 cs           v1                                     false        ComponentStatus
configmaps                        cm           v1                                     true         ConfigMap
endpoints                         ep           v1                                     true         Endpoints
events                            ev           v1                                     true         Event
limitranges                       limits       v1                                     true         LimitRange
namespaces                        ns           v1                                     false        Namespace
nodes                             no           v1                                     false        Node
persistentvolumeclaims            pvc          v1                                     true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                     false        PersistentVolume
pods                              po           v1                                     true         Pod
podtemplates                                   v1                                     true         PodTemplate
replicationcontrollers            rc           v1                                     true         ReplicationController
resourcequotas                    quota        v1                                     true         ResourceQuota
secrets                                        v1                                     true         Secret
serviceaccounts                   sa           v1                                     true         ServiceAccount
services                          svc          v1                                     true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1        false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1        false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1                false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1              false        APIService
controllerrevisions                            apps/v1                                true         ControllerRevision
daemonsets                        ds           apps/v1                                true         DaemonSet
deployments                       deploy       apps/v1                                true         Deployment
replicasets                       rs           apps/v1                                true         ReplicaSet
statefulsets                      sts          apps/v1                                true         StatefulSet
tokenreviews                                   authentication.k8s.io/v1               false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1                true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1                false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1                false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1                false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v2                         true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1                               true         CronJob
jobs                                           batch/v1                               true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1                 false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1                 true         Lease
endpointslices                                 discovery.k8s.io/v1                    true         EndpointSlice
events                            ev           events.k8s.io/v1                       true         Event
flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta2   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1beta2   false        PriorityLevelConfiguration
helmchartconfigs                               helm.cattle.io/v1                      true         HelmChartConfig
helmcharts                                     helm.cattle.io/v1                      true         HelmChart
addons                                         k3s.cattle.io/v1                       true         Addon
nodes                                          metrics.k8s.io/v1beta1                 false        NodeMetrics
pods                                           metrics.k8s.io/v1beta1                 true         PodMetrics
ingressclasses                                 networking.k8s.io/v1                   false        IngressClass
ingresses                         ing          networking.k8s.io/v1                   true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1                   true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                         false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1                              true         PodDisruptionBudget
podsecuritypolicies               psp          policy/v1beta1                         false        PodSecurityPolicy
clusterrolebindings                            rbac.authorization.k8s.io/v1           false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1           false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1           true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1           true         Role
priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass
csidrivers                                     storage.k8s.io/v1                      false        CSIDriver
csinodes                                       storage.k8s.io/v1                      false        CSINode
csistoragecapacities                           storage.k8s.io/v1                      true         CSIStorageCapacity
storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass
volumeattachments                              storage.k8s.io/v1                      false        VolumeAttachment
ingressroutes                                  traefik.containo.us/v1alpha1           true         IngressRoute
ingressroutetcps                               traefik.containo.us/v1alpha1           true         IngressRouteTCP
ingressrouteudps                               traefik.containo.us/v1alpha1           true         IngressRouteUDP
middlewares                                    traefik.containo.us/v1alpha1           true         Middleware
middlewaretcps                                 traefik.containo.us/v1alpha1           true         MiddlewareTCP
serverstransports                              traefik.containo.us/v1alpha1           true         ServersTransport
tlsoptions                                     traefik.containo.us/v1alpha1           true         TLSOption
tlsstores                                      traefik.containo.us/v1alpha1           true         TLSStore
traefikservices                                traefik.containo.us/v1alpha1           true         TraefikService
```

## Por que criar um Custom Resource?

- Permite que armazene e recupere dados estruturados
- Fornecer uma API declarativa quando combinado recursos personalizados com controladores personalizados
- Podem ser usados ​​em muitos cenários diferentes e ajudam a estender a funcionalidade do Kubernetes, automação de objetos e criação de objetos usando kubectl

Em alguns casos, você não pode concluir seus projetos com recursos integrados do Kubernetes, então você precisa de uma maneira de estendê-lo e personalizá-lo.

Suponha que você esteja executando um banco de dados ou cache dentro do cluster Kubernetes; neste caso, você tem tarefas operacionais específicas, como criar um backup, restaurar a partir de um backup existente e criar um cluster com algum conjunto predefinido de nós de servidor. `Essas tarefas geralmente são definidas usando CRDs e devem ser combinadas com um controlador para que ele execute a ação adequada após a criação de recursos específicos`.

Além disso, em caso de cenários de falha (ou seja, a instância do MySQL está inoperante), você pode definir o número de réplicas que deseja usando os CRDs. Em seguida, você pode aplicar o padrão de operador do Kubernetes (ou seja, utilizando um controlador ) sobre ele para colocar o sistema online novamente.

>Api Declarativa impõe uma separação de responsabilidade. Declara do estado desejado de um recurso e o controlador do kubernetes mantém o estado atual no kubernetes em sincronia com o estado declarado. `CRDs sem controladores são apenas objetos declarativos.`

É posssível implantar e atualizar um controller personalizado em um cluster em execução.
Os controladores personalizados funcionam com qualquer tipo de recurso, mas são mais eficazes com recursos personalizados.

## Criando um Custom Resource Definitions

Vamos definir um CRDs com o yaml da documentação que está em

```yaml
apiVersion: apiextensions.k8s.io/v1
# especifica que você deseja criar um CustomResourceDefinition
kind: CustomResourceDefinition

metadata:
  name: crontabs.stable.example.com
spec:
  # menciona o nome do grupo para a API.
  group: stable.example.com
  versions:
    #  a versão a ser usada na URL da API. Pode ter valores como v2 ou v1aplha
    - name: v1
      # controla se esta versão deve ser ativada ou desativada. Você só pode marcar uma versão como armazenamento
      served: true
      storage: true
      # especifica um esquema estrutural que você deseja validar o CRD usando a validação openAPIV3Schema antes de enviá-lo para o servidor API. Então você também está especificando que os campos de objeto customizado spec.cronSpec e spec.image devem ser uma string. O campo spec.replicas deve ser um número inteiro.
      schema:
        openAPIV3Schema:
          type: object
          properties:
            spec:
              type: object
              properties:
                cronSpec:
                  type: string
                image:
                  type: string
                replicas:
                  type: integer
                environmentType:
                  type: string
                  enum:
                  - development
                  - staging
                  - production
  # especifica se o objeto personalizado tem namespace ou está disponível em todo o cluster. Você usou o escopo Namespaced , então ele só está disponível para o namespace que você usará durante a criação do CRD. O padrão é com escopo de cluster.
  scope: Namespaced
  # especifica o nome do recurso, que deve estar no formato <plural>.<grupo> .
  names:
    # especifique o nome plural e singular do CRD.
    plural: crontabs
    singular: crontab
    # especifica o tipo do objeto personalizado.
    kind: CronTab
    # especifica a string curta que você pode usar na CLI.
    shortNames:
      - ct
```

```bash
kubectl apply -f ./Estudo/crds/crd.yaml 
customresourcedefinition.apiextensions.k8s.io/crontabs.stable.example.com created

kubectl api-resources | grep crontab 
crontabs                          ct           stable.example.com/v1                  true         CronTab
```

Agora vamos criar um objeto personalizado com o yaml

```yaml
apiVersion: 'stable.example.com/v1'
kind: CronTab
metadata:
  name: my-cron-object
spec:
  cronSpec: '* * * * */5'
  image: my-cron-image
  environmentType: development
```

```bash
kubectl apply -f ./Estudo/crds/my-crontab.yaml 
crontab.stable.example.com/my-cron-object created

# Veja como já expandiu o kubectl
kubectl get crontab                     
NAME             AGE
my-cron-object   96s

# usando o shotname
kubectl get ct -o yaml                        
apiVersion: v1
items:
- apiVersion: stable.example.com/v1
  kind: CronTab
  metadata:
    annotations:
      kubectl.kubernetes.io/last-applied-configuration: |
        {"apiVersion":"stable.example.com/v1","kind":"CronTab","metadata":{"annotations":{},"name":"my-cron-object","namespace":"default"},"spec":{"cronSpec":"* * * * */5","environmentType":"development","image":"my-cron-image"}}
    creationTimestamp: "2022-12-26T15:47:41Z"
    generation: 2
    name: my-cron-object
    namespace: default
    resourceVersion: "563771"
    uid: 58eda36f-15d9-4871-943d-2efb91fddba6
  spec:
    cronSpec: '* * * * */5'
    environmentType: development
    image: my-cron-image
kind: List
metadata:
  resourceVersion: ""
kubectl delete -f ./Estudo/crds/crd.yaml 
customresourcedefinition.apiextensions.k8s.io "crontabs.stable.example.com" deleted

kubectl get ct                          
Error from server (NotFound): Unable to list "stable.example.com/v1, Resource=crontabs": the server could not find the requested resource (get crontabs.stable.example.com)
```

É uma boa prática usar alguma validação antes de criar um recurso personalizado, pois o próprio kubernetes não fará nenhuma validação. Nesse caso pode-se escrever restrições. No nexemplo foi colocado um enum para o enrivonment restringindo as entradas.

O recurso de API CustomResourceDefinition permite definir recursos personalizados. A definição de um objeto CRD cria um novo recurso personalizado com um nome e um esquema que você especifica. A API Kubernetes serve e lida com o armazenamento de seu recurso personalizado. O nome de um objeto CRD deve ser um nome de subdomínio DNS válido .

Isso libera você de escrever seu próprio servidor de API para lidar com o recurso customizado.

Quando você cria um `novo CustomResourceDefinition (CRD)`, o Kubernetes `API Server cria um novo caminho de recurso RESTful` para cada versão especificada. O recurso personalizado criado a partir de um objeto CRD pode ter namespace ou cluster, conforme especificado no campo spec.scope do CRD. Assim como ocorre com os objetos integrados existentes, a exclusão de um namespace exclui todos os objetos personalizados desse namespace. As próprias CustomResourceDefinitions não têm namespace e estão disponíveis para todos os namespaces.

## Estrutura Interna de um CRD

De modo geral, `controllers customizados` são construidos em golang, mas não é uma obrigação.

![Schema das estruturas internas de um controller para CRD](../resources/archcrds.png)

### client go : Reflector

Um refletor observa a API Kubernetes para o tipo de recurso especificado (kind). Isso pode ser um recurso interno ou pode ser um recurso personalizado. Ao receber uma notificação sobre a existência de uma nova instância de recurso por meio da API watch, ele obtém o objeto recém-criado usando a API correspondente. Em seguida, ele coloca o objeto em uma fila Delta Fifo.

### client go : Informer

Um informer remove objetos da fila Delta Fifo. Seu trabalho é salvar o objeto para recuperação posterior e invocar o código do controlador passando o objeto.

## client go : Indexer

Um indexer fornece funcionalidade de indexação sobre objetos. Um caso de uso típico de indexação é criar um índice baseado em rótulos de objeto. O Indexador pode manter índices com base em várias funções de indexação. Indexador usa um armazenamento de dados thread-safe para armazenar objetos e suas chaves. Há uma função padrão que gera a chave de um objeto como uma combinação de `<namespace>/<name>` para esse objeto.

### Curstom Resources : Informer Reference

É a referência à instância do informer que sabe como trabalhar com seus objetos de CRD. Seu código de controlador personalizado precisa criar o Informer apropriado.

### Curstom Resources : Indexer Reference

Essa é a referência à instância do Indexer que sabe como trabalhar com seus objetos de CRDs. Seu código de controlador personalizado precisa ser criado. Você usará essa referência para recuperar objetos para processamento posterior. O client-go oferece funções para criar Informer e Indexer de acordo com suas necessidades. No seu código você pode invocar diretamente essas funções ou usar métodos de factory para criar um informer.

### Curstom Resources : Resource Event Handler

O padrão típico para escrever essas funções é obter a chave do objeto despachado e pôr essa chave em uma fila de trabalho para processamento adicional.

### Curstom Resources : Work Queue

Essa é a fila que você cria no código do controlador para dissociar a entrega de um objeto de seu processamento. As funções do manipulador de eventos de recurso são gravadas para extrair a chave do objeto entregue e adicioná-la à fila de trabalho.

---

>`Os CRDs não são úteis, a menos que você os combine com um controlador para usá-los como uma API declarativa para que o estado atual e o estado desejado estejam sempre sincronizados.`
