# Extras

Algumas coisas são boas para facilitar a vida na administração

## Alias para kubectl

Para subistuir o comando kubectl é interessante criar um alias no seu shell, nesse caso eu particulamente utilizo o comando `k`.

```bash
echo 'alias k="kubectl"' >> ~/.zshrc
echo 'alias k="kubectl"' >> ~/zshrc
```

## Kubectl Auto Complete

<https://kubernetes.io/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/>

Nada como facilitar a vida com um auto complete no terminal. O proprio kubectl gera um completion que pode ser carregado durante a abetura do seu shell. Porém é necessário ter o bash completion instalado.

```bash
sudo apt-get install bash-completion -y
```

Para bash

```bash
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

Para zsh

```bash
echo 'source <(kubectl completion zsh)' >>~/.zshrc
```

## VS Code Plugins

Alguns plugins do vs code são interessantes facilitam bastante para utilizar os recursos do kubernetes.

### Kubernetes

<https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools>

Esse plugim mostra o cluster que você tem e permite navegar entre os recursos como um explorador de arquivos. É um plugin que realmente vale a pena.

### Kubernetes Support

<https://marketplace.visualstudio.com/items?itemName=ipedrazas.kubernetes-snippets>

Esse plugin ajuda demais na hora de criar o yaml para os manifestos. Com um simples comando ele gera um template para vc simplesmente completar.

### Yaml

<https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml>

Esse plugin ajuda bastante a corrigir erros nos arquivos yaml em geral além de ter templates para k8s.

### Kubernetes Dashboard

Existem várias ferramentas gui open source legais para analisar o seu cluster kubernetes.

Essas ferramentas conseguem mostrar de forma gráfica os clusters e permite ainda criar recursos de forma muito fácil.

### Lens

<https://k8slens.dev/>

Para linux basta instalar o pacote que ele automaticamente busca no seu config os clusters que vc tem. Talvez é a ferramenta mais madura

## Octant

<https://octant.dev/>

Essa ferramente é muito poderosa inclusive é um projeto da CNCF. Eu particulamente prefiro ela ao Lens, mas acredito que as duas se completam.

## kubenav

<https://kubenav.io/>

Para ver seu cluster pelo seu dispositivo mobile essa é uma ferramenta legal, mas ainda limitada. Eu particulamente não uso, pois não gosto de trabalhar em celular.

## Kubevious

<https://github.com/kubevious/kubevious>

Essa ferramenta é gratis mas talvez a parte boa dela é paga, preciso ainda analisar, pois parece promissora.

## Kubecolor

<https://github.com/hidetatz/kubecolor>

O kubecolor internamente chamada o comando kubectl e colore a saida do terminal.

Para instalação faça o download do binário para o seu sistema operacional, extraia o arquivo e coloque em algum lugar que esteja no path

````bash
❯ wget https://github.com/hidetatz/kubecolor/releases/download/v0.0.20/kubecolor_0.0.20_Linux_x86_64.tar.gz
❯ tar zxf kubecolor_0.0.20_Linux_x86_64.tar.gz
❯ mv kubecolor /usr/local/bin
> chmod 755 /usr/local/bin/kubecolor

# crie um alias para que o comando kubectl chame o kubecolor e coloque no profile do seu terminal
echo 'alias kubectl="kubecolor"' >> ~/.zshrc
echo 'alias k="kubecolor"' >> ~/.zshrc
````

Abra novamente o terminal e veja o que acontece quando executa qualquer comando com o kubectl.

![kubecolor](../resources/kubecolor.png)

## Web Kubectl

<https://github.com/KubeOperator/webkubectl>

O Web Kubectl ajuda você a gerenciar as credenciais do kubernetes e executar o comando kubectl no navegador da Web, para que você não precise instalar o kubectl em seu PC local ou em outros servidores, além disso, o Web Kubectl pode ser usado para uma equipe.

- Suporte a vários usuários e vários clusters Kubernetes：Uma implantação do Web Kubectl pode ser usada para uma equipe, todos os membros da equipe podem usar o Web Kubectl simultaneamente, embora estejam conectando diferentes clusters Kubernetes ou privilégios diferentes.
- Isolamento de sessão：Todas as sessões online são isoladas, cada sessão tem seu próprio namespace e armazenamento que é invisível para as outras.
- Suporte a arquivo kubeconfig e token de portador : Você pode fornecer arquivo kubeconfig ou token de portador para conectar o cluster Kubernetes através do Web Kubectl.
- Fácil de usar e integrar：Você pode simplesmente usar a página de índice para um início rápido ou integrar com seu aplicativo usando api.
- Gerenciar clusters Kubernetes em VPC：Através do Web Kubectl você pode gerenciar os clusters Kubernetes em VPC que não podem ser alcançados em seu laptop.

```sh
_______________________________________________________________________
|   Local Network     |          DMZ           |      VPC/Datacenter  |
|                     |                        |                      |
|                     |    _______________     |   ----------------   |
|   ---------------   |    |             |  /~~~~~>| Kubernetes A |   |
|   | Your Laptop |~~~~~~~>| Web Kubectl | /   |   ----------------   |
|   ---------------   |    |             | \   |                      |
|                     |    ---------------  \  |   ----------------   |
|                     |                      \~~~~>| Kubernetes B |   |
|                     |                        |   ----------------   |
-----------------------------------------------------------------------
```

Somente crie um serviço em container em algum servidor e disponibilize o serviço

Exemplo:

````bash
docker run --name="webkubectl" -p 8080:8080 -d --privileged kubeoperator/webkubectl
````

## Krew

Krew é o gerenciador de plug-ins para kubectl e mantenha os plugins instalados atualizados.
>Eu particulamente não uso, pois prefiro instalar os binários usando o asdf, mas é uma boa opcao para quem não utilizar o asdf, aleḿ do krew ter muito mais plugins disponíveis.

- descobre plug-ins kubectl
- instale-os em sua máquina
- ajuda a empacotar seu plugin e distribuir em várias plataformas caso queira

[Krew](https://krew.sigs.k8s.io/) is unsupported for now.
[Instalacao](https://krew.sigs.k8s.io/docs/user-guide/setup/install/)
[Lista de plugins disponiveis](https://krew.sigs.k8s.io/plugins/)

```bash
# Atualizar a lista de plugins
kubectl krew update
# Listar todos os plugins
kubectl krew search
# procurar um plugin pelo nome
kubectl krew search cert
NAME          DESCRIPTION                                         INSTALLED
ca-cert       Print the PEM CA certificate of the current clu...  no
cert-manager  Manage cert-manager resources inside your cluster   no
clusternet    Wrap multiple kubectl calls to Clusternet           no
view-cert     View certificate information stored in secrets      no
# instalar um plugin
kubectl krew install deprecations
# Desinstalar um plugin
kubectl krew uninstall deprecations
# Atualizar todos os plugins
kubectl-krew upgrade
```

## Contributions

Always welcome. Just opening an issue should be also greatful.

## LICENSE

MIT
