# COMMUNITY

Neste repo tem bastante coisa da comunidade do kubernetes e vale a pena dar uma olhada
<https://github.com/kubernetes/community>

## Icons

Muito bom para construir fluxo e diagramas.
<https://github.com/kubernetes/community/tree/master/icons>

Na pasta [resources_extra](../resources_extra/) temos os icones que podemos usar baixados.
