# Por que Kubernetes?

O mercado hoje pede `Kubernetes`. É a ferramenta principal para `orquestração` de constainers. Em uma palestra escutei que ele já domina 90% de todo o mercado, não corri atras da informação, mas acredito que seja mesmo, pois praticamente cada cloud tem sua pŕopria versão do kubernetes.

- GKE no Google
- EKS na AWS
- AKS na Azure

Para trabalhar em projetos grandes, com alta escalabilidade e com transação massivas por segundo, é necessário ter Kubernetes em seu currículo. Uma hora ou outra vc vai ter que resolver aprender.

Muitas ferramentas [CNCF](https://www.cncf.io) (Cloud Native Computing Foundation) foram criadas para serem deployadas em cima de um cluster kubernetes.

>Para manipular containers o Kubernetes é lider e tudo esta virando container, então isso torna o kubernetes a `BASE`.

## Concorrentes do K8S
