# Overview do Kubernetes

## Arquitetura

Segue um modelo master/worker, constituindo assim um cluster, onde para seu funcionamento devem existir no mínimo três nós:

Master, responsável (por padrão) pelo gerenciamento do cluster, e os demais como workers, executores das aplicações que queremos executar sobre esse cluster.

Embora exista a exigência de no mínimo três nós para a execução do k8s em um ambiente padrão, e o master deve somente gerenciar, é possível mudar essa configuração fazendo com que master atue também como um worker. Desta maneira seria possível ter um cluster de um único node master/worker recebendo cargas de trabalho.

A figura a seguir mostra a arquitetura interna de componentes do k8s.

![Arquitetura do kubernetes](../resources/kubernetes_architecture.png "Arquitetura Kubernetes")

### **API Server**

É um dos principais componentes do k8s. Este componente fornece uma API que utiliza JSON sobre HTTP para comunicação, onde para isto é utilizado principalmente o utilitário `kubectl`, por parte dos administradores, para a comunicação com os demais nós, como mostrado no gráfico. Estas comunicações entre componentes são estabelecidas através de requisições REST.
>É o cerebro do kubernetes. Toda a comunicação acontece através do api-server. Todos os nós conversam com ele. É uma comunicação super segura.

### **etcd**

O etcd é um datastore chave-valor distribuído que o k8s utiliza para armazenar as especificações, status e configurações do cluster. Todos os dados armazenados dentro do etcd são manipulados apenas através da API. Por questões de segurança, o etcd é por padrão executado apenas em nós classificados como master no cluster k8s, mas também podem ser executados em clusters externos, específicos para o etcd. Somente o api server tem acesso ao etcd.
> É o banco de dados do estado do cluster.

### **Controller Manager**

Antes de entender isso, vamos entender [pod](#pod) e [deployament](#deployment)

É o controller manager quem garante que o cluster esteja no último estado definido no etcd. Por exemplo: se no etcd um **deployment está configurado** para possuir dez réplicas de um pod, é o controller manager quem irá verificar se o estado atual do cluster corresponde a este estado e, em caso negativo, procurará conciliar ambos.

>O controller manager cuida para que os deployments sejam atingidos e dentro de deployments temos os pods e quantas replicas queremos desses pods.

**kube-controler-manager** é quem interagem com o kube-api-server

### **Scheduler**

O kube-scheduller é responsável por selecionar o nó que irá hospedar um determinado pod para ser executado. Esta seleção é feita baseando-se na quantidade de recursos disponíveis em cada nó, como também no estado de cada um dos nós do cluster, garantindo assim que os recursos sejam bem distribuídos. Além disso, a seleção dos nós, na qual um ou mais pods serão executados, também pode levar em consideração políticas definidas pelo usuário, tais como afinidade, localização dos dados a serem lidos pelas aplicações, etc.
>É quem de fato executa o horizontal scaling dos nossos pods

### **Kubelet**

O kubelet pode ser visto como o **agente do k8s que é executado em cada nó worker**. Em cada nó worker deverá existir um agente Kubelet em execução. O Kubelet é responsável por de fato gerenciar os pods, que foram direcionados pelo controller do cluster, dentro dos nós, de forma que para isto o Kubelet pode iniciar, parar e manter os contêineres e os pods em funcionamento de acordo com o instruído pelo controlador do cluster.

### **Kube-proxy**

Age como um proxy e um load balancer. Este componente é responsável por efetuar roteamento de requisições para os pods corretos, como também por cuidar da parte de rede do nó. É quem vai expor as portas em cada nó.

### **Container Runtime**

O container runtime é o ambiente de execução de contêineres necessário para o funcionamento do k8s. Em 2016 suporte ao rkt foi adicionado, porém desde o início o Docker já é funcional e utilizado por padrão.

### **supervisord**

Para garantir que o kubelet e o container runtime esteja rodando. Caso uma falha aconteça, por algumas tentativas ele irá tentar reestabelecer estes serviços para que o node seja capaz de levantar os containers e comunicar com o controller manager.

### **CNI**

<https://kubernetes.io/pt-br/docs/concepts/cluster-administration/networking/>

kube-net é o CNI padrão do kubernetes mas não é o melhor.
A função do CNI é fazer a rede do kubernetes funcionar, disponibilizando criando a comunicação entre os pods mesmo em diferentes nodes.

>Diferentemente do docker swarm que utiliza nat para os pods o kubernetes através do cni disponibiliza um ip para cada pod. Os containers de um mesmo pod possuem o mesmo ip logo conseguem se comunicar através do localhost.

|CNI Name|Tipo Deployment|Encapsulamento e Rotas|Políticas de Rede|Uso de Datashore|Encriptação|Ingress|Enterprise|Link|
|---|---|---|---|---|---|---|---|---|
|Flannel|DaemonSet|VxLAN|No|Etcd|Yes|No|No|<https://github.com/flannel-io/flannel>|
|Calico|DaemonSet|IPinIP,BGP,eBPFVxLAN,eBPF,VxLAN|Yes|Etcd|Yes|Yes|Yes|<https://www.tigera.io/>|
|Cilium|DaemonSet|IPinIP,BGP,eBPFVxLAN,eBPF,VxLAN|Yes|Etcd|Yes|Yes|No|<https://www.cilium.io/>|
|Weavenet|DaemonSet|IPinIP,BGP,eBPFVxLAN,eBPF,VxLAN|Yes|No|Yes|Yes|Yes|<https://www.weave.works/oss/net/>|
|Canal|DaemonSet|VxLAN|Yes|Etcd|No|Yes|No|<https://github.com/projectcalico/canal>|

A diferença entre eles é o modelo de network que eles trabalham. No momento desse estudo o weavenet e o calido são considerado os melhores para um cluster baremetal.

>Se vc estiver usando o eks o vpc-cni é o padrão utilizado pela AWS.

## Objetos no Kubernetes

O que um controller?

É o objeto responsável por interagir com o API Server e orquestrar algum outro objeto. Exemplos de objetos desta classe são os Deployments e ReplicaSet.

### **Pod**

É o menor objeto do k8s. O k8s não trabalha com os contêineres diretamente, mas organiza-os dentro de pods, que são abstrações que dividem os mesmos recursos, como endereços, volumes, ciclos de CPU e memória. Um pod, embora não seja comum, pode possuir vários contêineres;
Os Pods realmente tem um ip cada um e não um nat como é no docker swarm.
>É possivel que containers dentro de um pod tenha diferentes ips? Não. **Todos os containers dentro de um pod compartilham o mesmo IP.**  
Pode ter mais de um container rodando por pod? Sim  
Quem é o controller se um dos pods morrer? Replicaset  

### **ReplicaSets**

É um objeto responsável por garantir a quantidade de pods em execução no nó;
>Quem é o controller do ReplicaSet? Deployment

### **DeamonSet**

É um replicaset com a diferença que vc não escolhe quantas replicas de pod vc vai ter. Ele é utilizado quando vc precisa que o pod esteja rodando em todos os nós do seu cluster. Exemplo disso é pods de métricas como o prometheus. Ele rodará por default um pod em CADA NÓ.
>Ainda é um tipo de replicaset, logo seu controller é um deployment.

### **Deployment**

É um dos principais controllers utilizados. O Deployment, em conjunto com o ReplicaSet, garante que determinado número de réplicas de um pod esteja em execução nos nós workers do cluster. Além disso, o Deployment também é responsável por gerenciar o ciclo de vida das aplicações, onde características associadas a aplicação, tais como imagem, porta, volumes e variáveis de ambiente, podem ser especificados em arquivos do tipo yaml ou json para posteriormente serem passados como parâmetro para o kubectl executar o deployment. Esta ação pode ser executada tanto para criação quanto para atualização e remoção do deployment;

>Quando existe uma atualização do replicaset quem é o responsável? Deployemnt  
Quando existe a atualização de um deployment ele cria um novo replicaset e dá um down no antigo. Isso serve para rollback. Ele fecha o novo e sobe o antigo.

### **Jobs e CronJobs**

São objetos responsáveis pelo gerenciamento de jobs isolados ou recorrentes.

### **Namespaces**

São separações para projetos dentro do kubernetes podendo inclusive ser utilizado para quotas de recursos.
>O namespace do kubernetes é o kube-system, nunca deve ser deployado nada dentro dele.
Se não for passado um namespace ele vai enviar para o default.

## Requisitos

Iremos repetir na parte de requisitos, mas vou manter aqui enquanto não finalizo.

### **Portas Essenciais**

`Nó master`

|Protocolo|Direção|Portas|Propósito|Utilizado por|
|---|---|---|---|---|
|TCP|Entrada|6443|Api do Kubernetes|Todos|
|TCP|Entrada|2379-2380|Api servidor-client do etc|kube-apiserver, etc|
|TCP|Entrada|10250|Api do Kubelet|kubeadm, Camada de gerenciamento
|TCP|Entrada|10259|kube-scheduler|kubeadm|
|TCP|Entrada|10257|kube-controller-manager|kubeadm|

`Nó worker`

|Protocolo|Direção|Portas|Propósito|Utilizado por|
|---|---|---|---|---|
|TCP|Entrada|10250|Api do Kubelet|O próprio, Camada de gerenciamento|
|TCP|Entrada|30000-32767|Serviços NodePort|Todos|

### **Considerações para grandes clusters**

Um cluster é um conjunto de nós executando agentes Kubernetes, gerenciados pelo plano de controle. O Kubernetes **v1.23** oferece suporte a clusters com até 5000 nós. O Kubernetes foi projetado para configurações que atendem a todos os critérios a seguir:

- Não mais de 110 pods por nó
- Não mais de 5.000 nós
- Não mais de 150.000 pods totais
- Não mais de 300.000 contêineres totais

Para cluster em larga escala conferir <https://kubernetes.io/docs/setup/best-practices/cluster-large/>

## Tá, mas qual tipo de aplicação eu devo rodar sobre o k8s?

O melhor app para executar em contêiner, principalmente no k8s, são aplicações que seguem o The Twelve-Factor App.
