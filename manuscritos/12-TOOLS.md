# Tools

O Kubernetes possui diversas ferramentas que envolvem além da instalação muitas outras melhorias. Se observar bem muitas ferramentas da CNCF são deployadas no ambiente do Kubernetes.

---

## Helm

<https://helm.sh/>

É o gerenciado de pacote dos kubernetes. Praticamente te entrega todos os manifestos prontos para a instalação de algum serviço. Muito utilizado.

## Service Mesh

Geralmente será escolhido um dos 3. O mais utilizado pela comunidade é o Istio.

### Istio

<https://istio.io/latest/docs/>
Service mesh dedicado ao Kubernetes. Deployado através de um DaemonSet e instala um pod em cada um dos hosts. Sua instalação permite acrescentar várias ferrametnas com o kiali, prometheus, jaeger.

### Consul

<https://www.consul.io/>
Service mesh desenvolvido pela Hashicorp sendo um binário único e n~

### Linkerd

<https://istio.io/latest/docs/>
Service mesh dedicado ao Kubernetes. muito com ele pode ser instalado várias ferrametnas com o kiali, prometheus, jaeger.

## Traefik

<https://doc.traefik.io/traefik/>

Proxy reverso e loadbalancer para containers. É muito utilizado inclusive para substituir o ingress padrão do kuberentes.

## Repository

### Harbor

<https://goharbor.io/docs/2.0.0/install-config/>

Registry bem documentado e utilizado pela comunidade.

Logs
Os logs do Minikube podem ser acessados através do seguinte comando.

```bash
minikube logs
```

### Artifactory

<https://jfrog.com/artifactory/>

## Certificados

### Cert Manager

<https://cert-manager.io/docs/>
Gerenciador de certificados mais utilizado pela comunicade. Utiliza lets encrypt e outros certificados e já prove automaticamente a renovação.

## Storage

### Rook

## CICD

### Tekton

<https://tekton.dev/>

Ferramenta de CICD deployada deployada em Kuberentes. É um tendencia muito forte a ser usada.

## GitOps

Basicamente temos as duas opções abaixo. Elas são bem equivalente nos questitos funcionalidades. Vale a pena ver o [video](https://www.youtube.com/watch?v=NQidP_NHZig) comparando as duas ferramentas.

### ArgoCD

<https://argoproj.github.io/cd/>
É o mais  utilizado e possui mais estrelas no CNCF, inclusive mais material na Internet. Já passou por revisão de processo de segurança e irá se graduar no ano de 2022.

Um Estudo dessa ferramenta está no repositório <https://gitlab.com/davidpuziol/study-argocd>

### Flux

<https://fluxcd.io/>

O Flux não deixa a deseja em relação ao ArgoCD. Oferece as mesma funcionalidade e é um projeto graduando ainda na CNCF..

## Servless

O kuberentes consegue prover um serviço como lambda também. O container executa a sua função e morre.

### Openfass

<https://www.openfaas.com/>

### Knative

<https://knative.dev/docs/>

### Kubeless

<https://github.com/vmware-archive/kubeless>

### Karpenter

<https://github.com/vmware-archive/kubeless>

