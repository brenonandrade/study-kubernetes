# Topologias

O `mínimo que um cluster kubernetes precisa ter para trabalhar é 1 node`. Esse node será master, mas uma flag habilitada após a instalação pode permitir fazer deploy dentro do master. Não é uma boa prática, mas as vezes necessário. Claro que este cluster não teria nenhuma alta disponibilidade.

>Um cluster com um master se torna indisponível caso seu master pare.

Um cluster com redundância (alta disponibilidade) precisa no mínimo 3 masters.

>Se o seu parque de máquina é pequeno, poderia sim habilitar a flag para que os 3 masters recebam deploys, para que no futuro com a entrada de mais máquinas elas entrem como workers e os masters vão fazendo somente o que deve ser feito.

Para entender alta disponibilidade é preciso entender o protocolo de consensus.

---

## Consensus

[Raft Consensus](https://raft.github.io/) é um algorítmo que implementa o protocolo consensus que garante a confiabilidade do cluster. O k8s utiliza esse algorítmo e esse [site](http://thesecretlivesofdata.com/) explica bem como funciona . É esse algorítmo que garante a ordem no cluster. O consensus é aplicado aos nodes masters que teremos no nosso cluster para controlar os workers.

Os nodes masters pertencente de um cluster sempre estarão em algum dos estados abaixo do Raft Consensus:

- Follower (seguidor, não da ordem, apenas segue)
- Candidate (candidato a se tornar lider)
- Leader (Lider)

>No Raft Consensus cada node master tem direito a um voto para eleger um lider.

Se vc só tem um nó ele inicia como seguidor, se candidata a ser o lider e como só tem 1 para votar (ele mesmo) ele se torna o lider.

Se vc tiver somente um único nó master e ele cair, perderá o controle do cluster, por isso é necessário uma redundância de nós masters em sistemas distribuídos. Como falei todo nó master entra como seguidor e se ele não encontrar um lider ele se candidata e o nó que tiver maior número de votos será o lider. Se o node master cair, os outros masters novamente entram em estado de candidatos e elegem um novo lider. O lider o estado real do cluster, por isso ele precisa saber de tudo que se passa com os seguidores.

>Não é necessário entender como funciona a eleição, somente o número de nodes masters para se ter uma redundância caso o lider fique offline.  
A fórmula de tolerância é (N-1)/2 falhas e precisa de um quorum de (N/2)+1. Quorum é a quantidade mínima de nodes para garantir a maioria.

| Nodes |     Quorum      | Tolerância Falha |
| ----- | ----------------| ---------------- |
|   1   | (1/2)+1 = `1`.5 |  (1-1)/2 = `0`   |
|   2   | (2/2)+1 = `2`   |  (2-1)/2 = `0`.5 |
|   3   | (3/2)+1 = `2`.5 |  (3-1)/2 = `1`   |
|   4   | (4/2)+1 = `3`   |  (4-1)/2 = `1`.5 |
|   5   | (5/2)+1 = `3`.5 |  (5-1)/2 = `2`   |
|   6   | (6/2)+1 = `4`   |  (6-1)/2 = `2`.5 |
|   7   | (7/2)+1 = `4`.5 |  (7-1)/2 = `3`   |

*Podemos observar que reprecisa ser impar a partir de 3 para ter pelo menos 1 redundância. Trabalhar com pares é perde de tempo e recurso.

>Estudos mostram que replicação de dados consomem muita rede e processamento. Logo, 3 é suficiente, 5 é uma boa escolha, 7 começa a piorar e 9 piora o desempenho. Reza a lenda que quem tem 1 não tem nenhum, então no mínimo 3 por favor.

---

## Multi Master com etcd junto control plane

A topologia abaixo mostra como isso acontece, porém o etcd ainda permanece junto com o control plane (controller manager), ele serão executados no mesmo nó master.

É mais simples de configurar do que um cluster com nós etcd externos e mais simples de gerenciar para replicação.

![multimaster](../resources/Topology%20Kubernetes%20Multi%20Master.jpg "Multi Master")

Como falado no protocolo de consensus é necessário no mínimo 3 nodes masters. Nesse caso vc já estaria replicando o etcd e o control plane ao mesmo tempo.

Porém aqui vemos um integrante diferente que é o `load balancer`. Ele serve para distribuir a carga dentre os masters e caso um master fique indisponível redireciona a comunicação para outro master.

Os workers se comunicam com os masters através de um load balancer.

Para o load balancer você pode utilizar um na própria cloud ou por exemplo o [HA Proxy](https://www.haproxy.org/) ou o [Traefik](https://traefik.io/).

>O que precisamos no load balancer? Um dns ou um ip que aponte para os nós master na porta  **6443**.

---

## Multi Master com etcd separado

Seria a mesma coisa que a topologia anterior, porém os etcds rodam em nodes separados.

![Arquitetura do kubernetes](../resources/Topology%20Kubernetes%20Multi%20Master%20Separated%20etcd.jpg "Arquitetura Kubernetes")

Um cluster de alta disponibilidade com etcd externo é uma topologia em que o cluster de armazenamento de dados distribuído fornecido pelo etcd é externo ao cluster formado pelos nós que executam os componentes do plano de controle.

Fornece uma configuração de alta disponibilidade em que a perda de uma instância do plano de controle ou de um membro etcd tem menos impacto e não afeta a redundância do cluster tanto quanto a topologia de alta disponibilidade empilhada.

No entanto, essa topologia requer o dobro de hosts. Um **mínimo de três hosts para nós do plano de controle e três hosts para nós etcd**
