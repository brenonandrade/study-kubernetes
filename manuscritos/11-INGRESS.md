# Ingress

https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/

Você precisa perceber que a implantação de um cluster Kubernetes para um aplicativo específico tem muitos requisitos essenciais. Escolher um controlador de ingresso é um deles. Um controlador Ingress para Kubernetes é um recurso que ajuda a conectar o mundo externo a clusters e ajuda a definir um conjunto específico de regras, para que as solicitações do usuário para acessar um microsserviço específico sejam processadas corretamente.


Os dois métodos de fazer isso ou é usando um service nodeport ou loadbalancer.

Com o ingress a partir de um único endereço é possível redirecionar para o service correto.

Existem vários ingress

- Traefik
- Nginx 
- Ha proxy

Antes de iniciar o nosso estudo do ingress precisamos ter 2 deployments rodando para testes 

## Preparação do ambientes

e cada deployment com o seu service. Cada um desses apps respondem uma pagina estatica com o nome do autor, coisa bem simples. O que nos interessa é conseguir separar as requisições usando o ingress.

Na pasta [ingress](../Estudo/ingress/) temos todos os arquivos necessários. Aplique os app1, app2, svc1 e svc2.

````bash
❯ k create -f app1.yaml
deployment.apps/app1 created

❯ k create -f app2.yaml
deployment.apps/app2 created

❯ k create -f svc1.yaml
service/app1 created

❯ k create -f svc2.yaml
service/app2 created

❯ k get deploy
NAME   READY   UP-TO-DATE   AVAILABLE   AGE
app1   2/2     2            2           2m51s
app2   2/2     2            2           2m51sinternal   <none>           <none>

# Otimos uqe ficou um pod em cada node usando replica para 2.
# Mais um exemplo que o get consegue pegar varios ao mesmo tempo num unico comando
❯ k get deploy,svc,po,ep -o wide
NAME                   READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                      SELECTOR
deployment.apps/app1   2/2     2            2           19m   app1         dockersamples/static-site   app=app1
deployment.apps/app2   2/2     2            2           19m   app2         dockersamples/static-site   app=app2

NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE   SELECTOR
service/app1         ClusterIP   172.20.254.153   <none>        80/TCP    19m   app=app1
service/app2         ClusterIP   172.20.89.10     <none>        80/TCP    19m   app=app2
service/kubernetes   ClusterIP   172.20.0.1       <none>        443/TCP   28d   <none>

NAME                        READY   STATUS    RESTARTS   AGE    IP             NODE                          NOMINATED NODE   READINESS GATES
pod/app1-788bb997b4-j4hss   1/1     Running   0          111s   10.10.62.218   ip-10-10-35-53.ec2.internal   <none>           <none>
pod/app1-788bb997b4-qnzhn   1/1     Running   0          110s   10.10.89.113   ip-10-10-70-52.ec2.internal   <none>           <none>
pod/app2-5d884f5687-jkdc6   1/1     Running   0          104s   10.10.93.155   ip-10-10-70-52.ec2.internal   <none>           <none>
pod/app2-5d884f5687-ww798   1/1     Running   0          105s   10.10.60.90    ip-10-10-35-53.ec2.internal   <none>           <none>

NAME                   ENDPOINTS                            AGE
endpoints/app1         10.10.62.218:80,10.10.89.113:80      19m
endpoints/app2         10.10.60.90:80,10.10.93.155:80       19m
endpoints/kubernetes   10.10.181.230:443,10.10.183.85:443   28d
````

Entre em um dos pods, instale o curl e bata nos ips para conferir se esta tudo ok. O app1 consegue fazer um curl do app2 tranquilamente, pois estão na mesma rede....

````bash
❯ k exec -ti app1-767f4d7f7c-98nh9 -- sh
# apt update && apt install curl -y
# curl 10.10.34.36
# curl curl 10.10.34.36
# #Vai ver a saida aqui com o author correta de quem vc apontou.
````

## Ingress Nginx

<https://docs.nginx.com/nginx-ingress-controller/>

Todo o conteudo estará nas pasta [nginx](../Estudo/ingress/nginx/)

No ingress nginx quando ele não consegue encontrar o serviço ele precisa devolver um erro 404. Isso nada mais é do que um outro deployment que mostra a página com o erro. Claro que também precisamos criar o service para apontar para esse deployment.

Vamos deployar também o deployment do default backend que será usado pelo ingress do nginx, mas vamos separar tudo do nginx em um namespace dedicado.

````bash
❯ k create namespace ingress
namespace/ingress created

❯ k create -f default-backend.yaml -n ingress
deployment.apps/default-backend created

❯ k create -f default-backend-svc.yaml -n ingress 
service/default-backend created

❯ k create -f default-backend-svc.yaml -n ingress 
service/default-backend created

❯ k get deploy,svc,po,ep -n ingress -o wide
NAME                              READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS        IMAGES                                        SELECTOR
deployment.apps/default-backend   2/2     2            2           4m13s   default-backend   gcr.io/google-containers/defaultbackend:1.4   app=default-backend

NAME                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE   SELECTOR
service/default-backend   ClusterIP   172.20.153.215   <none>        80/TCP    33s   app=default-backend

NAME                                   READY   STATUS    RESTARTS   AGE     IP             NODE                          NOMINATED NODE   READINESS GATES
pod/default-backend-67576b5c99-ldxvd   1/1     Running   0          4m14s   10.10.83.23    ip-10-10-70-52.ec2.internal   <none>           <none>
pod/default-backend-67576b5c99-mcqsf   1/1     Running   0          4m14s   10.10.47.159   ip-10-10-35-53.ec2.internal   <none>           <none>

NAME                        ENDPOINTS                            AGE
endpoints/default-backend   10.10.47.159:8080,10.10.83.23:8080   34s
````

Vai no mesmo container que vc instalou o curl para facilitar e faca um curl o clusterip e veja se algum container esta respondendo o 404.

````bash
❯ k exec -ti app1-788bb997b4-qnzhn -- sh   
# curl 172.20.153.215
default backend - 404# 
# curl 10.10.47.159:8080
default backend - 404# 
````

> Se vc bater no clusterip qualquer um dos dois pods poderá responder. Se bater no específico na porta 8080 somente aquele vai responder.

Segunda parte agora é criar um configmap para ser usado no ingress para habilitar uma configuração vts que vamos utilizar e será vista mais pra frente

````bash
❯ k create -f nginx-ingress-controller-cm.yaml -n ingress
configmap/nginx-ingress-controller-conf created

❯ k get cm -n ingress
NAME                            DATA   AGE
kube-root-ca.crt                1      14m
nginx-ingress-controller-conf   1      11s

❯ k describe cm -n ingress nginx-ingress-controller-conf 
Name:         nginx-ingress-controller-conf
Namespace:    ingress
Labels:       app=nginx-ingress-lb
Annotations:  <none>

Data
====
enable-vts-status:
----
true

BinaryData
====

Events:  <none>
````

Vamos criar um serviceaccount para o nosso ingress e dar algumas permissões a nível de clusterrole. Lembrando do estudo de rbac, vamos precisar também vincular a esta cluster role com a service account usando um cluster role biding

````bash
# Criando a service account ja definida para o namespace ingress
❯ k create -f nginx-ingress-controller-sa.yaml 
serviceaccount/nginx created

# Criando o clusterole
❯ k create -f nginx-ingress-controller-cr.yaml 
clusterrole.rbac.authorization.k8s.io/nginx-role created

# Dando ao usuario a cluster role
❯ k create -f nginx-ingress-controller-crb.yaml 
clusterrolebinding.rbac.authorization.k8s.io/nginx-role 

❯ k get sa -n ingress
NAME      SECRETS   AGE
default   1         45m
nginx     1         5m
````

> Eu separei esses 3 arquivos manifestos, mas é interessante mante-los no mesmo arquivo.

Agora por fim vamos criar um deployment do nginx ingress controller

https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/

