# Limits

Existem alguns objetos que aplicam regras sobre outros, como uma espécie de política, afim de manter um cluster saudável ou até mesmo dividir melhor os recursos.

Existem dois tipos de limites o de range e o quotas.

Uma cota de recurso, definida por um objeto ResourceQuota, fornece restrições que limitam consumo de recursos agregados por namespace. Pode limitar a quantidade de objetos que podem ser criado em um namespace por tipo, bem como a quantidade total de recursos computacionais que podem ser consumidos por recursos nesse namespace.

> É interessante o uso de quotas para dividir o cluster para diferentes equipes, diferentes ambientes e até mesmo diferentes empresas caso várias usem o mesmo cluster.

Porém é possível que somente 1 deployment monopolize todo esse recurso e nada mais pode ser criado ali dentro pois vai atingir a quota. Dessa forma usamos o LimitRange para aplicar regras.

> Após aplicado um limitrange esse só vale para recursos que forem atualizados ou criados, os que já estão em execução não sofrem mudanças. Da mesma forma se uma quota for diminuída e os recursos somem mais do que possível, somente passará a valor a partir de da atualização ou criação de recursos.

Caso os limites de quotas disponíveis no namespace sejam menores que a soma dos limites dos Pods/Contêineres, pode haver contenção por recursos. Nesse caso, os contêineres ou Pods não serão criados. Se um deployment exiget 5 replicas a soma total dessas replicas não podem ultrapassar a cota do namespace.

## LimitRange (alias limits)

<https://kubernetes.io/pt-br/docs/concepts/policy/limit-range/>

É um objeto que é utilizado para limitar a quantidade de recursos que determinado namespace vai usar. É possível aplicar essa regra para o cluster inteiro caso deseje.

Quando se aplica um type container irá valer quando se cria um pod. Quando se cria um deployment, pos mais que ele tenha pods, a regra não vale, logo ele pode criar com o tamanho que quiser desde que não extrapole a quota caso exista.

O arquivo [limitrangens.yaml](./limitrangens.yaml) vai aplicar uma regra de limites para todos os container do namespace develop. Todos os containers criados nesse namespace que não definiram seus limites herdarão o mínimo 1/8 (128m) de cpu e 128M de memoria e no máximo 1/4 cpu (256m) e 256Mi de memória. Além disso caso especificado, um container somente pode atingir 384m de cpu e 384Mi de memoria.

Porém, um pod somando todos os seus containers, somente poderá ter 1/2 cpu (512m) e 512Mi de memória

````bash
❯ k create -f limitrangens.yaml
limitrange/lr-container-ns-develop created
````

Façamos um teste para analisar o caso. Foi tentado criar um pod com valores maiores do que o permitido. Somente era possível 384Mi de memória e foi tentado criar com 1024 e somente era permitido 384m de cpu e foi requisitado 384m.

````bash
❯ k create -f testepod.yaml 
Error from server (Forbidden): error when creating "testepod.yaml": pods "testepod" is forbidden: [maximum memory usage per Container is 384Mi, but limit is 1Gi, maximum cpu usage per Container is 384m, but limit is 1024m]
````

> A regra de pod é aplicada para a soma de limits, de um pod, que é o hard limit e não o request.

Agora vamos aplicar um deployment para testar a soma de pods. Nesse deployment existe um pod com 2 containers. Um com o nginx com o limit maximo 350m de cpu e 350Mi de memória e um debian de 180m de cpu e 180Mi de memória que totalizam 530m de cpu e 530Mi de memória. Mas o nosso limite para um pod é de 512.

````bash
❯ k create -f testedeployment.yaml
deployment.apps/nginxteste created

# Observe que nenhum pod foi criado apesar do deployment ter sido criado.
❯ k get pods -n develop
No resources found in develop namespace.

# Para conferir o que aconteceu é necessário ir no replicaset desse deployment e ver pq ele não subiu o pod.
❯ k get rs -n develop                       
NAME                    DESIRED   CURRENT   READY   AGE
nginxteste-57958c8f7b   1         0         0       5m30s

❯ k describe rs -n develop nginxteste-57958c8f7b 
Name:           nginxteste-57958c8f7b
Namespace:      develop
Selector:       app=nginxteste,pod-template-hash=57958c8f7b
Labels:         app=nginxteste
                pod-template-hash=57958c8f7b
Annotations:    deployment.kubernetes.io/desired-replicas: 1
                deployment.kubernetes.io/max-replicas: 2
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/nginxteste
Replicas:       0 current / 1 desired
Pods Status:    0 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginxteste
           pod-template-hash=57958c8f7b
  Containers:
   nginx:
    Image:      nginx:latest
    Port:       80/TCP
    Host Port:  0/TCP
    Limits:
      cpu:     350m
      memory:  350Mi
    Requests:
      cpu:        300m
      memory:     300Mi
    Environment:  <none>
    Mounts:
      /usr/share/nginx/html from html (rw)
   debian:
    Image:      debian
    Port:       <none>
    Host Port:  <none>
    Command:
      /bin/sh
      -c
    Args:
      while true; do date >> /html/index.html; sleep 1; done
    Limits:
      cpu:     180m
      memory:  180Mi
    Requests:
      cpu:        128m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /html from html (rw)
  Volumes:
   html:
    Type:       EmptyDir (a temporary directory that shares a pods lifetime)
    Medium:     
    SizeLimit:  <unset>
Conditions:
  Type             Status  Reason
  ----             ------  ------
  ReplicaFailure   True    FailedCreate
Events:
  Type     Reason        Age                  From                   Message
  ----     ------        ----                 ----                   -------
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-6dkf6" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-tkfx9" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-4fdmz" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-fxt2r" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-c56vc" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-jbdkz" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m46s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-5k22c" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m45s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-kdcdp" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  5m45s                replicaset-controller  Error creating: pods "nginxteste-57958c8f7b-xwgvp" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
  Warning  FailedCreate  18s (x8 over 5m44s)  replicaset-controller  (combined from similar events): Error creating: pods "nginxteste-57958c8f7b-gd264" is forbidden: [maximum cpu usage per Pod is 512m, but limit is 530m, maximum memory usage per Pod is 512Mi, but limit is 555745280]
````

Nesse caso, diminuo o valor de limit do container debian para cpu: 160m e memory: 160 que totaliza 510 em cada uma das somas e teste novamente.

````bash
❯ k edit deploy -n develop nginxteste 
deployment.apps/nginxteste edited

❯ k get pods -n develop
NAME                          READY   STATUS    RESTARTS   AGE
nginxteste-575955f599-bwb59   2/2     Running   0          13s
````

> Caso não seja definido o namespace no yaml pode ser feito o mesmo comando passando o namespace no comando. Mas por boa prática sempre fixe o namespace no manifesto.

É possível definir limites para numero de tags de imagem, tamanho de volume, etc.
<https://docs.openshift.com/container-platform/4.8/nodes/clusters/nodes-cluster-limit-ranges.html>

Para conferir o limitrange

````bash
❯ k describe limitranges -n develop
Name:       resource-limits
Namespace:  develop
Type        Resource  Min  Max    Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---  ---    ---------------  -------------  -----------------------
Container   cpu       -    384m   128m             256m           -
Container   memory    -    384Mi  128Mi            256Mi          -
Pod         memory    -    512Mi  -                -              -
Pod         cpu       -    512m   -                -              -
````

````bash
❯ k delete limitranges -n develop resource-limits
limitrange "resource-limits" deleted
````

## Taints

As taints são uma espécie de marcação como as labels, mas são específicas para nodes de um cluster. As taints são um parâmetro dos nodes lidas pelos scheduler para saber se um node pode ou não receber pods, executar, etc. Geralmente quando se definir os parâmetros de um pod é possível setar afinidade para que um pod seja schedulado para um node que tenha uma label disk=ssh por exemplo. O taint faz o oposto, ele repele pods.

> Nodes masters por padrão no kuberentes não recebem pods pq tem uma taint NoSchedule definida.

Porém pensar dessa maneira confunde o uso de taints. Pense nas taints como chave e fechadura. Quando vc põe uma taint em um node, esse node cria uma fechadura que é a taint e um pod ter a chave que é o tolerations. Se for declarado um toleraion com a mesma chave valot declarado no taint, ele consegue ser schedulado para aquele node.

Vamos testar

````bash
# Conferindo o node e checando se tem alguma taint
❯ k get nodes
NAME                          STATUS   ROLES    AGE   VERSION
ip-10-10-35-53.ec2.internal   Ready    <none>   43h   v1.23.9-eks-ba74326

❯ k describe nodes ip-10-10-35-53.ec2.internal | grep Taints             
Taints:             <none>

# Colocando uma taint no node com chave valor key1=value1:NoSchedule

❯ kubectl taint nodes ip-10-10-35-53.ec2.internal key1=value1:NoSchedule 
node/ip-10-10-35-53.ec2.internal tainted

❯ k describe nodes ip-10-10-35-53.ec2.internal | grep Taints            
Taints:             key1=value1:NoSchedule
````

Vamos ficar sem nenhum pod para conferir ... Delete os deployments e pods que vc tiver.

````bash
❯ k delete deployments.apps -n develop nginxteste  
deployment.apps "nginxteste" deleted

❯ k get pods -n develop
No resources found in develop namespace.
````

Agora vamos tentar criar novamente o deployment

````bash
❯ k create -f testedeploymentfixed.yaml
deployment.apps/nginxteste created

❯ k get deployments.apps -n develop
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
nginxteste   0/1     1            0           7s

# Ele fica e pending pq não encontrou um node para ser schedulado, pois eu só tenho 1.
❯ k get pods -n develop
NAME                          READY   STATUS    RESTARTS   AGE
nginxteste-575955f599-zw55s   0/2     Pending   0          15s

#### Subi mais um node para ver se ele vai schedulara quando o outro aparecer. o Node 35-53 eh o que tinha a taint.
❯ k get nodes
NAME                          STATUS   ROLES    AGE   VERSION
ip-10-10-35-53.ec2.internal   Ready    <none>   43h   v1.23.9-eks-ba74326
ip-10-10-70-52.ec2.internal   Ready    <none>   31s   v1.23.9-eks-ba74326

# E agora ele foi para o node 70-52
❯ k get pods -n develop -o wide
NAME                          READY   STATUS    RESTARTS   AGE     IP             NODE                          NOMINATED NODE   READINESS GATES
nginxteste-575955f599-zw55s   2/2     Running   0          8m47s   10.10.77.123   ip-10-10-70-52.ec2.internal   <none>           <none>

# escalarmos...
❯ k scale deployment -n develop nginxteste --replicas=3
deployment.apps/nginxteste scaled

# Todos foram para o noh que não tinha a taint.
❯ k get pods -n develop -o wide                         
NAME                          READY   STATUS        RESTARTS   AGE   IP             NODE                          NOMINATED NODE   READINESS GATES
nginxteste-575955f599-cqsfk   2/2     Terminating   0          93s   10.10.78.41    ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-575955f599-n8d9h   2/2     Running       0          92s   10.10.89.113   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-575955f599-pnsmn   2/2     Running       0          93s   10.10.65.224   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-575955f599-zw55s   2/2     Running       0          11m   10.10.77.123   ip-10-10-70-52.ec2.internal   <none>           <none>
````

> Uma curiosidade é que se removermos a taint do node1 (final 53) o kubernetes não irá balancear o cluster dividindo a carga, somente a partir do aparecimento de novos pods que ele irá ir colocando no node1. Isso seré resolvido mais pra frente no estudo avançado.

Vamos agora colocar o tolaration no pod. Descomente o bloco tolerations dentro do [testedeploymentfixed.yaml](testedeploymentfixed.yaml) e reaplique o deploy ou se deletou crie novamente

````bash
❯ k replace -f testedeploymentfixed.yaml
deployment.apps/nginxteste replaced

❯ k scale deployment -n develop nginxteste --replicas=5
deployment.apps/nginxteste scaled

# Obeserve que agora por ter o tolerations como se fosse a chave da fechadura ele pode ser schedulado para dentro do nodes que tem a taint.

❯ k get pods -n develop -o wide                        
NAME                          READY   STATUS    RESTARTS   AGE   IP             NODE                          NOMINATED NODE   READINESS GATES
nginxteste-6558754d8d-n654h   2/2     Running   0          23s   10.10.89.113   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-6558754d8d-p5kpj   2/2     Running   0          63s   10.10.39.225   ip-10-10-35-53.ec2.internal   <none>           <none>
nginxteste-6558754d8d-sdxm8   2/2     Running   0          23s   10.10.34.36    ip-10-10-35-53.ec2.internal   <none>           <none>
nginxteste-6558754d8d-wmsvf   2/2     Running   0          23s   10.10.93.155   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-6558754d8d-xsv58   2/2     Running   0          23s   10.10.75.107   ip-10-10-70-52.ec2.internal   <none>           <none>
````

O valor no NoExecute faz com que todos os pods imediatamente sejam retirados desse cluster e não somente os que forem criados posteriormente como funciona no NoSchedule.

````bash
❯ kubectl taint nodes ip-10-10-35-53.ec2.internal key2=value2:NoExecute
node/ip-10-10-35-53.ec2.internal modified

# Veja que alguns pods que estavam na maquina 35-53 sumiaram e somente tem agora na maquina 70-52. Porém também não conseguiu levantar pq não tinha recuso sobrando nela suficiente.
 k get pods -n develop -o wide
NAME                          READY   STATUS    RESTARTS   AGE    IP             NODE                          NOMINATED NODE   READINESS GATES
nginxteste-6558754d8d-m4zx8   0/2     Pending   0          2m3s   <none>         <none>                        <none>           <none>
nginxteste-6558754d8d-n654h   2/2     Running   0          30m    10.10.89.113   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-6558754d8d-skl9m   0/2     Pending   0          2m3s   <none>         <none>                        <none>           <none>
nginxteste-6558754d8d-wmsvf   2/2     Running   0          30m    10.10.93.155   ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-6558754d8d-xsv58   2/2     Running   0          30m    10.10.75.107   ip-10-10-70-52.ec2.internal   <none>           <none>
````

Agora vamos aplicar a tolerations NoExecute e ver o que acontece. Coloque também essa chave no tolerations ficando desse jeito:

````bash
      tolerations:
      - key: "key1"
        operator: "Exists"
        effect: "NoSchedule"
      - key: "key2"
        operator: "Exists"
        effect: "NoExecute"
````

Isso significa que se ele encontrar as duas regras "chaves" para as duas "fechaduras" ele vai poder subir.

````bash
❯ k replace -f testedeploymentfixed.yaml
deployment.apps/nginxteste replaced

❯ k scale deployment -n develop nginxteste --replicas=6
deployment.apps/nginxteste scaled

# Veja que agora subiu nos dois
❯ k get pods -n develop -o wide                        
NAME                          READY   STATUS    RESTARTS   AGE     IP             NODE                          NOMINATED NODE   READINESS GATES
nginxteste-7f588c7b99-7jlb6   2/2     Running   0          3m17s   10.10.51.187   ip-10-10-35-53.ec2.internal   <none>           <none>
nginxteste-7f588c7b99-87vf9   2/2     Running   0          6s      10.10.83.23    ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-7f588c7b99-g5nww   2/2     Running   0          6s      10.10.54.127   ip-10-10-35-53.ec2.internal   <none>           <none>
nginxteste-7f588c7b99-j96jt   2/2     Running   0          6s      10.10.92.90    ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-7f588c7b99-lpwgs   2/2     Running   0          6s      10.10.78.41    ip-10-10-70-52.ec2.internal   <none>           <none>
nginxteste-7f588c7b99-vf5ch   2/2     Running   0          6s      10.10.40.137   ip-10-10-35-53.ec2.internal   <none>           <none>
````

É através das taints que o kuberentes sabe se um nó esta inutilizável, com problema até mesmo se ele não terminou de inicializar ainda para receber pods. Ele aplica uma taint, e o kubelet retira avisando que está pronto.

> Se existe um node que possui uma caracteristica específica gpu, este pode ter uma taint para garantir que apps que não precisam de processamento gráfico sejam criados nesses node.

Quando chegar a hora de trabalhar com taints, é pq os conhecimentos já estão bem avançados o cluster está grande e equipes diferentes colaborando.

Remova as taints para evitar problemas nos próximos aprendizados.

````bash
❯ kubectl taint nodes ip-10-10-35-53.ec2.internal key2=value2:NoExecute-
node/ip-10-10-35-53.ec2.internal untainted

❯ kubectl taint nodes ip-10-10-35-53.ec2.internal key1=value1:NoSchedule-
node/ip-10-10-35-53.ec2.internal untainted
````

> Se um replicaset de um deployment não for capaz de subir pods por conta de algum taint dado ao node e este taint for removido, no mesmo momento o replicaset irá identificar que agora pode continuar subindo e fará o seu trabalho.

## ResourceQuotas (alias quota)

<https://kubernetes.io/pt-br/docs/concepts/policy/resource-quotas/>

Quotas é o conjunto de recurso que se dá pra um determinado namespace distribuir entre seus pods. Vamos imaginar que a somatória de todos os nodes do cluster de 12 cpu e 24gb de memoria. o Custo desse cluster é 1000 reais. Voce colocará serviços de 3 empresas nesse cluster.

Empresa 1 paga 500 reais e vc vai definir para ela 6 cpu e 12gb de ram
Empresa 2 paga 300 reais e vc vai definir para ela 3,6 cpu e 7,2gb de de ram
Empresa 3 paga 200 reais e vai vai defiir para ela 2.4 cpu e 4.8gb de ram

Na empresa 1 vc quer ainda dividir 6cpu em 2 ambienes. Produção e desenvolimento. 4 para producao e 2 para desenvolvimento.

Logo vc criaria por exemplo 4 namespaces e dentro de cada um desses namespaces aplicaria a cota.

- empresa1-prod
- empresa1-dev
- empresa2-all
- empresa3-all

Mesmo que o container tenha recurso sobrando, se um namespace tem uma cota definida é aquilo que ele pode usar.

As cotas podem ser aplicadas para:

- Cpu
- Memoria
- Armazenamento
- Contagem de objetos
- Prioridade de objetos

> A documentação é muito bem explicada e considero que cotas muito específicas podem prejudicar inicialmente o aprendizado. Para isso vamos somente limitar o uso total de recursos, mas não a contagem nem classificar recursos.

Vamos aplicar um resourcequota no em um namespace development.

````bash
❯ k describe quota --all-namespaces
Name:                   quota-name-develop
Namespace:              develop
Resource                Used    Hard
--------                ----    ----
limits.cpu              3060m   2
limits.memory           3060Mi  2Gi
persistentvolumeclaims  0       5
pods                    6       2
requests.cpu            2568m   1
requests.memory         2568Mi  1Gi
requests.storage        0       5Gi
````

Cada um dos pods aqui presentes tem um total máximo de 510cpu e 510Mb de memória se analisar no seu describe ou get. Sendo 160 para o debian e 350 para o nginx.

````bash
❯ k get deployments.apps -n develop
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
nginxteste   6/6     6            6           22h
````

Claro que nesse momento esta extendendo a cota, pois os recursos que já estão aplicados não sofrem essa aplicação, somente os recursos que serão criados.

Vamos scalar para 1 e depois para 6 novamente.

````bash
❯ k scale deploy -n develop nginxteste --replicas 1
deployment.apps/nginxteste scaled           1           22h

❯ k scale deploy -n develop nginxteste --replicas 6
deployment.apps/nginxteste scaled

❯ k get deployments.apps -n develop                               
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
nginxteste   2/6     2            2           22h

❯ k describe rs -n develop nginxteste-7f588c7b99 
Name:           nginxteste-7f588c7b99
Namespace:      develop
Selector:       app=nginxteste,pod-template-hash=7f588c7b99
Labels:         app=nginxteste
                pod-template-hash=7f588c7b99
Annotations:    deployment.kubernetes.io/desired-replicas: 6
                deployment.kubernetes.io/max-replicas: 8
                deployment.kubernetes.io/revision: 3
Controlled By:  Deployment/nginxteste
Replicas:       2 current / 6 desired
Pods Status:    2 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginxteste
           pod-template-hash=7f588c7b99
  Containers:
   nginx:
    Image:      nginx:latest
    Port:       80/TCP
    Host Port:  0/TCP
    Limits:
      cpu:     350m
      memory:  350Mi
    Requests:
      cpu:        300m
      memory:     300Mi
    Environment:  <none>
    Mounts:
      /usr/share/nginx/html from html (rw)
   debian:
    Image:      debian
    Port:       <none>
    Host Port:  <none>
    Command:
      /bin/sh
      -c
    Args:
      while true; do date >> /html/index.html; sleep 1; done
    Limits:
      cpu:     160m
      memory:  160Mi
    Requests:
      cpu:        128m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /html from html (rw)
  Volumes:
   html:
    Type:       EmptyDir (a temporary directory that shares a pods lifetime)
    Medium:     
    SizeLimit:  <unset>
Conditions:
  Type             Status  Reason
  ----             ------  ------
  ReplicaFailure   True    FailedCreate
Events:
  Type     Reason            Age                     From                   Message
  ----     ------            ----                    ----                   -------
  Normal   SuccessfulDelete  4m12s                   replicaset-controller  Deleted pod: nginxteste-7f588c7b99-87vf9
  Normal   SuccessfulDelete  4m12s                   replicaset-controller  Deleted pod: nginxteste-7f588c7b99-vf5ch
  Normal   SuccessfulDelete  4m12s                   replicaset-controller  Deleted pod: nginxteste-7f588c7b99-lpwgs
  Normal   SuccessfulDelete  4m12s                   replicaset-controller  Deleted pod: nginxteste-7f588c7b99-g5nww
  Normal   SuccessfulDelete  4m12s                   replicaset-controller  Deleted pod: nginxteste-7f588c7b99-j96jt
  Warning  FailedCreate      4m2s                    replicaset-controller  Error creating: pods "nginxteste-7f588c7b99-2srpd" is forbidden: exceeded quota: quota-name-develop, requested: limits.cpu=510m,limits.memory=510Mi,pods=1,requests.cpu=428m,requests.memory=428Mi, used: limits.cpu=3060m,limits.memory=3060Mi,pods=6,requests.cpu=2568m,requests.memory=2568Mi, limited: limits.cpu=2,limits.memory=2Gi,pods=2,requests.cpu=1,requests.memory=1Gi
  Warning  FailedCreate      4m2s                    replicaset-controller  Error creating: pods "nginxteste-7f588c7b99-kqdnn" is forbidden: exceeded quota: quota-name-develop, requested: limits.cpu=510m,limits.memory=510Mi,pods=1,requests.cpu=428m,requests.memory=428Mi, used: limits.cpu=3060m,limits.memory=3060Mi,pods=6,requests.cpu=2568m,requests.memory=2568Mi, limited: limits.cpu=2,limits.memory=2Gi,pods=2,requests.cpu=1,requests.memory=1Gi

❯ k delete -f testedeploymentfixed.yaml 
deployment.apps "nginxteste" deleted
````

  Podemos ver que somente 2 pods foram conseguidos scalar por eles juntos soman quase 1cpu e 1gb de ram.

  O uso de cotas será muito bem vindo mais pra frente para controlar ambientes e principalmente equipes.
