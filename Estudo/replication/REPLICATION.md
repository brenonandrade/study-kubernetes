# Replication

Antes de entrarmos em detalhes sobre como vc faria a replicação, vamos falar sobre o porquê.Normalmente, vc deseja replicar seus contêineres (e, portanto, seus aplicativos) por vários motivos, incluindo:

- Confiabilidade: Ao ter váriod pods (que roda um container) de um aplicativo, você evita problemas se um ou mais falharem. Isso é particularmente verdadeiro se o sistema substituir qualquer pod que falhe.

- Balanceamento de carga: ter vários pods permite enviar tráfego facilmente para diferentes instâncias para evitar a sobrecarga de uma única instância ou nó. Isso é algo que o Kubernetes faz imediatamente, tornando-o extremamente conveniente.

- Dimensionamento: quando a carga se torna muito grande para o número de pods existentes, o Kubernetes permite que vc amplie facilmente seu aplicativo, adicionando instâncias adicionais conforme necessário.

## Replicaset (alias rs)

A função do `replicaset` é gerenciar os pods. Como mensionado anteriormente não existe muita razão para se criar um pod solto sem um replicaset para cuidar desse pod. Inclusive e boa prática independente de ser sómente 1 único pod que ele seja controlado por um replicaset para garantir o seu estado.

>Na verdade não se definem pods, nem replicasets, se cria deployaments que tem replicaset e pods.

Vamos imaginar uma aplicação que finaliza um container com erro por algum motivo, por exemplo um erro de desenvolvimento. Esse pod irã morrer e quem irá levanar outro? Essa é a função do replicaset. Se vc avisar que vc quer 5 pods rodando sempre para uma aplicação o replicaset cuidará disso.

Assim como um pod solto não tem muito valor, um replicaset também não tem. O controller do replicaset é o deployment.

Dentro do manifesto yaml do replicaset temos um campo chamado template e nesse campo que deve ser definido o pod que serã replicado.

>Não existe como vc criar um pod em um manifesto, aplicar este manifesto e depois criar um replicaset fitrando pelo label do pod criado anteriormente. Um replicaset possuí em sua definição a configuração do pod ele irá criar. Já pensou que bagunça seria se vc criar um replicaset baseado em um pod e depois deletar o pod? Ou vc deleta tudo junto ou não deleta nada.

Um replicaset seleciona os pods através de um match com algum label que foi definido dentro do metada to template (que é o pod).

Mesmo que um pod tenha sido configurado dentro de um yaml para replicaset, esses pods são objetos separados e criados objetos para eles. Um replicaset é um replicaset e um pod é um pod. Por isso que mesmo que vc crie um replicaset vc consegue fazer um `kubectl get pods` para ver somente os pods.

## Diferenças entre replicaset daemonset e replication controller

Na verdade todos esses objetos são controllers de pods, porém com pequenos detalhes que os diferenciam.

Um replicaset cria quantos pods vc quer não importando onde será executado deste que atendam os requisitos de filtros e cotas. Vamos imaginar que foi pedido para rodar 10 pods de uma aplicação qualquer no namespace production e somente em mãquinas do tipo ubuntu. Se existem 3 nodes que podem receber esses 10 pods, eles serão criados de acordo com o schedules dentro nesses nodes, podendo estar os 10 pods dentro do mesmo node, pois este pode ter mais recursos sobrando que os demais.

>Um replicaset cria os pods onde tem recurso

### Daemonset (alias ds)

>Um Daemonset não é um replicaset ok? Ele é um outro tipo de objeto que se parece demais com o replicaset, mas com uma diferença muito pequena, e por isso está sendo abordado aqui.

Um daemonset cria 1 pod e somente 1 pod em todos os nodes do cluster. Por isso vc não precisa definir quantas replicas quer. Só de declarar um daemonset é exatamente isso que irá acontecer.

Agora filtrando melhor o que a frase anterior, se vc não filtrar em quais nodes receberão os pods ele criará em todos os nodes que podem receber recursos, mas se filtrar que somente quer nos nodes ubuntu por exemplo somente estes receberão um pod.

O daemoset é muito útil para ferramentas de observabilidade por exemplo que vc quer um pod em cada node monitorando o desempenho da máquina ou exportando logs por exemplo.

Um detalhe aqui falado foi sobre nodes que podem receber pods.

>Por padrão nodes masters não recebem recurso como por exemplo no eks, mas é possível em clusters manualmente criado executar clusters nos masters apesar de não ser recomendado. Dessa forma um daemonset também poderia ser criado no master, assim como replicaset.

Existe prioridade entre daemon set e replicaset?
E se não existir espaço para um daemonset?

É possível definir prioridades?

## Replication Controller (alias rc)

O Replicaset é a evolução do replication controller. Este foi criado nos primórdios do k8s e depois substituido pelo replicaset. O replicaset tem algumas funcionalidades a mais quando comparados ao replication controller.

Replicaset seleciona os pods pelos labels, por isso usa dentro do selector um matchLabels. Esse seletor é mais generalizado do que o selector do replication controller.

A segundo coisa é sobre atualização de pods. O replicaset usa o comando rollout e o replication controller usa rolling-update.

Este controller somente foi mensionado por curiosidade, mas a tendência é ser depreciado, não perca tempo com ele e vai direto pro replicaset.

## Aplicando os nossos manifestos

````bash
❯ k create -f replicaset1.yaml
replicaset.apps/nginx-rs created

❯ k create -f daemonset.yaml
daemonset.apps/prometheus-exporter-ds created

❯ k get rs
NAME       DESIRED   CURRENT   READY   AGE
nginx-rs   5         5         5       109m

❯ k get ds
NAME                     DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
prometheus-exporter-ds   1         1         1       1            1           <none>          27m

# Analisando os nomes é possivel notar que ele vem com o nome do objeto e depois um numero randomico para cada um dos pods.

❯ k get po
NAME                           READY   STATUS    RESTARTS   AGE
nginx-rs-6zhfd                 1/1     Running   0          107m
nginx-rs-8d6np                 1/1     Running   0          107m
nginx-rs-j7mmj                 1/1     Running   0          107m
nginx-rs-qjgl9                 1/1     Running   0          107m
nginx-rs-sxvpv                 1/1     Running   0          107m
prometheus-exporter-ds-8d75m   1/1     Running   0          27m
````

Só para conferir os nossos objetos

````bash
❯ k describe rs nginx-rs 
Name:         nginx-rs
Namespace:    production
Selector:     app=nginx-pod
Labels:       app=nginx-rs
Annotations:  <none>
Replicas:     5 current / 5 desired
Pods Status:  5 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginx-pod
  Containers:
   nginx:
    Image:        nginx
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:           <none>

❯ k describe ds prometheus-exporter-ds 
Name:           prometheus-exporter-ds
Selector:       app=prometheus-exporter-ds
Node-Selector:  <none>
Labels:         app=prometheus-exporter-ds
Annotations:    deprecated.daemonset.template.generation: 3
Desired Number of Nodes Scheduled: 1
Current Number of Nodes Scheduled: 1
Number of Nodes Scheduled with Up-to-date Pods: 1
Number of Nodes Scheduled with Available Pods: 1
Number of Nodes Misscheduled: 0
Pods Status:  1 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=prometheus-exporter-ds
           tier=monitoring
  Containers:
   prometheus:
    Image:      prom/node-exporter
    Port:       80/TCP
    Host Port:  0/TCP
    Limits:
      memory:  200Mi
    Requests:
      cpu:        100m
      memory:     200Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                  Message
  ----    ------            ----  ----                  -------
  Normal  SuccessfulCreate  35m   daemonset-controller  Created pod: prometheus-exporter-ds-95qsr
  Normal  SuccessfulDelete  34m   daemonset-controller  Deleted pod: prometheus-exporter-ds-95qsr
  Normal  SuccessfulCreate  34m   daemonset-controller  Created pod: prometheus-exporter-ds-gfspl
  Normal  SuccessfulDelete  32m   daemonset-controller  Deleted pod: prometheus-exporter-ds-gfspl
  Normal  SuccessfulCreate  32m   daemonset-controller  Created pod: prometheus-exporter-ds-8d75m
````

Pode escalar mais pods com um simples comando.

````bash
❯ k scale rs nginx-rs --replicas 3
replicaset.apps/nginx-rs scaled

❯ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-rs-6zhfd                 1/1     Running   0          116m
nginx-rs-qjgl9                 1/1     Running   0          116m
nginx-rs-sxvpv                 1/1     Running   0          116m
prometheus-exporter-ds-8d75m   1/1     Running   0          37m

❯ k scale rs nginx-rs --replicas 10
replicaset.apps/nginx-rs scaled

❯ k get pods
NAME                           READY   STATUS              RESTARTS   AGE
nginx-rs-4bkls                 0/1     ContainerCreating   0          4s
nginx-rs-4g8vn                 1/1     Running             0          4s
nginx-rs-6zhfd                 1/1     Running             0          117m
nginx-rs-blb8g                 1/1     Running             0          4s
nginx-rs-cwxvf                 0/1     ContainerCreating   0          4s
nginx-rs-dz4bf                 1/1     Running             0          4s
nginx-rs-nbg8x                 0/1     ContainerCreating   0          4s
nginx-rs-qjgl9                 1/1     Running             0          117m
nginx-rs-sxvpv                 1/1     Running             0          117m
nginx-rs-xs2lj                 1/1     Running             0          4s
prometheus-exporter-ds-8d75m   1/1     Running             0          37m
````

Observe que o comadno acima quadno eu pedi os pods ele ainda estava criando pods, mas no total de 10 como escalei.

> Se vc escalar com um comando e depois aplicar novamente o yaml, ele vai escalar para o que esta no manifesto.

Veja que não é possível escalar um daemonset

````bash
❯ k scale ds --replicas 2 prometheus-exporter-ds 
Error from server (NotFound): the server could not find the requested resource
````

Entendendo melhor agora o que os replications fazem. Delete um pod e veja que ele criará um novo. No momento que eu fiz isso eu estava com um replicaset de 5 pods.

````bash
❯ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-rs-blb8g                 1/1     Running   0          12m
nginx-rs-d2mlb                 1/1     Running   0          5s
nginx-rs-qjgl9                 1/1     Running   0          129m
nginx-rs-sxvpv                 1/1     Running   0          129m
nginx-rs-xs2lj                 1/1     Running   0          12m
prometheus-exporter-ds-8d75m   1/1     Running   0          50m

# deletando dois pods o primeiro e o terceiro
❯ k delete pod nginx-rs-blb8g nginx-rs-qjgl9 
pod "nginx-rs-blb8g" deleted
pod "nginx-rs-qjgl9" deleted

#Observer abaixo dois novos com 4s
❯ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-rs-d2mlb                 1/1     Running   0          45s
nginx-rs-js5g5                 1/1     Running   0          4s
nginx-rs-kz66d                 1/1     Running   0          4s
nginx-rs-sxvpv                 1/1     Running   0          130m
nginx-rs-xs2lj                 1/1     Running   0          13m
prometheus-exporter-ds-8d75m   1/1     Running   0          50m
````

>Se uma alteração de imagem for feita no arquivo, por exemplo para uma v2, e for aplicado novamente este arquivo, os replications não criam novos pods com a nova imagem até que novos pods precisem ser criados para atingir o objetivo, sejam por ter sido deletados ou escalados.

É para isso que existe o deployment. Entendeu agora pq não faz sentido vc ter um replicaset também sem deployment?

## extra

Somente execute essa tarefa depois de finalizar o estudo do deployment para entender o que é um rollout.

````bash
❯ k create -f daemonset.yaml 
daemonset.apps/prometheus-exporter-ds created

❯ k rollout history daemonset prometheus-exporter-ds 
daemonset.apps/prometheus-exporter-ds 
REVISION  CHANGE-CAUSE
1         <none>

❯ k get ds prometheus-exporter-ds -o yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  annotations:
    deprecated.daemonset.template.generation: "1"
  creationTimestamp: "2022-09-18T14:52:06Z"
  generation: 1
  labels:
    app: prometheus-exporter-ds
  name: prometheus-exporter-ds
  namespace: production
  resourceVersion: "4520349"
  uid: f8c45d52-04da-477a-86a7-13b6826e8a91
spec:
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: prometheus-exporter-ds
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: prometheus-exporter-ds
        tier: monitoring
    spec:
      containers:
      - image: prom/node-exporter
        imagePullPolicy: Always
        name: prometheus
        ports:
        - containerPort: 80
          protocol: TCP
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 200Mi
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      tolerations:
      - key: node-role.kubernetes.io/control-plane
        operator: Exists
      - key: node-role.kubernetes.io/master
        operator: Exists
# Esse campo updateStrategy é default no daemonset
  updateStrategy:
    rollingUpdate:
      maxSurge: 0
      maxUnavailable: 1
    type: RollingUpdate
status:
  currentNumberScheduled: 2
  desiredNumberScheduled: 2
  numberAvailable: 1
  numberMisscheduled: 0
  numberReady: 1
  numberUnavailable: 1
  observedGeneration: 1
  updatedNumberScheduled: 2
````

É possível fazer um rollout de um daemonset pois ele tem um parametro dentro de spec chamado updateStrategy, diferente do replicaset que não possui. Porém para evitar essa diferença acostume-se a criar um deployment também para o daemonset e deixe a estratégia de rollout para o deployment. Eu paerticulamente achei meio inconsistente essa diferença, mas vamos que vamos.
