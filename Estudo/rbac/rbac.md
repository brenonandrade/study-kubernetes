# RBAC

https://kubernetes.io/docs/reference/access-authn-authz/rbac/

Esse estudo serve para aplicar os conceitos iniciais do rbac e como são gerenciados usuarios, permissões e roles dentro do k8s.

Role-based access control (RBAC) Role-based access control (RBAC)  é um método de regular o acesso a recursos de computador ou rede com base nas funções de usuários individuais em sua organização.

A autorização RBAC usa o rbac.authorization.k8s.io Grupo de APIs para orientar as decisões de autorização, permitindo configurar políticas.

A API RBAC declara quatro tipos de objeto Kubernetes:

- Role
- ClusterRole
- RoleBinding
- ClusterRoleBinding

Você pode descrever objetos ou alterá-los usando ferramentas como kubectl, assim como qualquer outro objeto do Kubernetes.

## ClusterRoles e Roles

Uma função RBAC ou ClusterRole contém regras que representam um conjunto de permissões. As permissões são puramente aditivas (não há regras de "negação").

Uma role sempre define permissões em um determinado namespace. Ao criar uma role, você precisa especificar o namespace ao qual ela pertence.

Uma cluster role não precisa de namespace.

> Essa é a diferença entre esses dois objetos

Você pode usar um ClusterRole para:

- definir permissões em recursos com namespace e ter acesso a namespaces individuais
- definir permissões em recursos com namespace e ter acesso a todos os namespaces
- definir permissões em recursos com escopo de cluster

Se você deseja definir uma função em um namespace, use uma role e se você quiser definir uma função em todo o cluster, use um ClusterRole

As clusters roles são as funções que um determinado usuário pode exercer no kubernetes.

Vamos ver quais clusterroles temos no eks.

````bash
❯ k get clusterrole
NAME                                                                   CREATED AT
admin                                                                  2022-08-25T19:26:38Z
aws-node                                                               2022-08-25T19:26:44Z
cluster-admin                                                          2022-08-25T19:26:38Z
edit                                                                   2022-08-25T19:26:38Z
eks:addon-manager                                                      2022-08-25T19:26:44Z
eks:certificate-controller-approver                                    2022-08-25T19:26:42Z
eks:certificate-controller-signer                                      2022-08-25T19:26:42Z
eks:cloud-controller-manager                                           2022-08-25T19:26:42Z
eks:cloud-provider-extraction-migration                                2022-08-25T19:26:42Z
eks:cluster-event-watcher                                              2022-08-25T19:26:42Z
eks:fargate-manager                                                    2022-08-25T19:26:45Z
eks:fargate-scheduler                                                  2022-08-25T19:26:42Z
eks:k8s-metrics                                                        2022-08-25T19:26:42Z
eks:node-bootstrapper                                                  2022-08-25T19:26:45Z
eks:node-manager                                                       2022-08-25T19:26:44Z
eks:nodewatcher                                                        2022-08-25T19:26:42Z
eks:pod-identity-mutating-webhook                                      2022-08-25T19:26:42Z
eks:podsecuritypolicy:privileged                                       2022-08-25T19:26:45Z
eks:tagging-controller                                                 2022-08-25T19:26:42Z
system:aggregate-to-admin                                              2022-08-25T19:26:38Z
system:aggregate-to-edit                                               2022-08-25T19:26:38Z
system:aggregate-to-view                                               2022-08-25T19:26:38Z
system:auth-delegator                                                  2022-08-25T19:26:38Z
system:basic-user                                                      2022-08-25T19:26:38Z
system:certificates.k8s.io:certificatesigningrequests:nodeclient       2022-08-25T19:26:38Z
system:certificates.k8s.io:certificatesigningrequests:selfnodeclient   2022-08-25T19:26:38Z
system:certificates.k8s.io:kube-apiserver-client-approver              2022-08-25T19:26:38Z
system:certificates.k8s.io:kube-apiserver-client-kubelet-approver      2022-08-25T19:26:38Z
system:certificates.k8s.io:kubelet-serving-approver                    2022-08-25T19:26:38Z
system:certificates.k8s.io:legacy-unknown-approver                     2022-08-25T19:26:38Z
system:controller:attachdetach-controller                              2022-08-25T19:26:38Z
system:controller:certificate-controller                               2022-08-25T19:26:38Z
system:controller:clusterrole-aggregation-controller                   2022-08-25T19:26:38Z
system:controller:cronjob-controller                                   2022-08-25T19:26:38Z
system:controller:daemon-set-controller                                2022-08-25T19:26:38Z
system:controller:deployment-controller                                2022-08-25T19:26:38Z
system:controller:disruption-controller                                2022-08-25T19:26:38Z
system:controller:endpoint-controller                                  2022-08-25T19:26:38Z
system:controller:endpointslice-controller                             2022-08-25T19:26:38Z
system:controller:endpointslicemirroring-controller                    2022-08-25T19:26:38Z
system:controller:ephemeral-volume-controller                          2022-08-25T19:26:38Z
system:controller:expand-controller                                    2022-08-25T19:26:38Z
system:controller:generic-garbage-collector                            2022-08-25T19:26:38Z
system:controller:horizontal-pod-autoscaler                            2022-08-25T19:26:38Z
system:controller:job-controller                                       2022-08-25T19:26:38Z
system:controller:namespace-controller                                 2022-08-25T19:26:38Z
system:controller:node-controller                                      2022-08-25T19:26:38Z
system:controller:persistent-volume-binder                             2022-08-25T19:26:38Z
system:controller:pod-garbage-collector                                2022-08-25T19:26:38Z
system:controller:pv-protection-controller                             2022-08-25T19:26:38Z
system:controller:pvc-protection-controller                            2022-08-25T19:26:38Z
system:controller:replicaset-controller                                2022-08-25T19:26:38Z
system:controller:replication-controller                               2022-08-25T19:26:38Z
system:controller:resourcequota-controller                             2022-08-25T19:26:38Z
system:controller:root-ca-cert-publisher                               2022-08-25T19:26:38Z
system:controller:route-controller                                     2022-08-25T19:26:38Z
system:controller:service-account-controller                           2022-08-25T19:26:38Z
system:controller:service-controller                                   2022-08-25T19:26:38Z
system:controller:statefulset-controller                               2022-08-25T19:26:38Z
system:controller:ttl-after-finished-controller                        2022-08-25T19:26:38Z
system:controller:ttl-controller                                       2022-08-25T19:26:38Z
system:coredns                                                         2022-08-25T19:26:44Z
system:discovery                                                       2022-08-25T19:26:38Z
system:heapster                                                        2022-08-25T19:26:38Z
system:kube-aggregator                                                 2022-08-25T19:26:38Z
system:kube-controller-manager                                         2022-08-25T19:26:38Z
system:kube-dns                                                        2022-08-25T19:26:38Z
system:kube-scheduler                                                  2022-08-25T19:26:38Z
system:kubelet-api-admin                                               2022-08-25T19:26:38Z
system:monitoring                                                      2022-08-25T19:26:38Z
system:node                                                            2022-08-25T19:26:38Z
system:node-bootstrapper                                               2022-08-25T19:26:38Z
system:node-problem-detector                                           2022-08-25T19:26:38Z
system:node-proxier                                                    2022-08-25T19:26:38Z
system:persistent-volume-provisioner                                   2022-08-25T19:26:38Z
system:public-info-viewer                                              2022-08-25T19:26:38Z
system:service-account-issuer-discovery                                2022-08-25T19:26:38Z
system:volume-scheduler                                                2022-08-25T19:26:38Z
view                                                                   2022-08-25T19:26:38Z
vpc-resource-controller-role                                           2022-08-25T19:26:47Z

❯ k get roles --all-namespaces 
NAMESPACE     NAME                                             CREATED AT
kube-public   system:controller:bootstrap-signer               2022-08-25T19:26:39Z
kube-system   eks-vpc-resource-controller-role                 2022-08-25T19:26:47Z
kube-system   eks:addon-manager                                2022-08-25T19:26:45Z
kube-system   eks:authenticator                                2022-08-25T19:26:42Z
kube-system   eks:certificate-controller                       2022-08-25T19:26:42Z
kube-system   eks:fargate-manager                              2022-08-25T19:26:45Z
kube-system   eks:k8s-metrics                                  2022-08-25T19:26:42Z
kube-system   eks:node-manager                                 2022-08-25T19:26:44Z
kube-system   extension-apiserver-authentication-reader        2022-08-25T19:26:39Z
kube-system   system::leader-locking-kube-controller-manager   2022-08-25T19:26:39Z
kube-system   system::leader-locking-kube-scheduler            2022-08-25T19:26:39Z
kube-system   system:controller:bootstrap-signer               2022-08-25T19:26:39Z
kube-system   system:controller:cloud-provider                 2022-08-25T19:26:39Z
kube-system   system:controller:token-cleaner                  2022-08-25T19:26:39Z
kube-system   vpc-resource-controller-leader-election-role     2022-08-25T19:26:47Z
````

Vamos descrever uma role para entender como é mostrado

````bash
❯ k describe clusterrole cluster-admin 
Name:         cluster-admin
Labels:       kubernetes.io/bootstrapping=rbac-defaults
Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  *.*        []                 []              [*]
             [*]                []              [*]
````

Veja o [role.yaml](./role.yaml) e os comentários

````bash
❯ k create -f role.yaml
role.rbac.authorization.k8s.io/pod-reader created

❯ k describe role pod-reader
Name:         pod-reader
Labels:       <none>
Annotations:  kubernetes.io/description:
                Add endpoints write permissions to the edit and admin roles. This was
                removed by default in 1.22 because of CVE-2021-25740. See
                https://issue.k8s.io/103675. This can allow writers to direct LoadBalancer
                or Ingress implementations to expose backend IPs that would not otherwise
                be accessible, and can circumvent network policies or security controls
                intended to prevent/isolate access to those backends.      
              whos: quem pode usar
              why: por que usar essa role
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  pods       []                 []              [get watch list]
````

Vamos aplicar também para uma clusterrole. Veja que o arquivo é praticamente o mesmo modelo, mas sem o namespace.

````bash
❯ k create -f clusterole.yaml 
clusterrole.rbac.authorization.k8s.io/custom:aggregate-to-edit:endpoints created

❯ k describe clusterrole custom:aggregate-to-edit:endpoints 
Name:         custom:aggregate-to-edit:endpoints
Labels:       rbac.authorization.k8s.io/aggregate-to-edit=true
Annotations:  kubernetes.io/description:
                Add endpoints write permissions to the edit and admin roles. This was
                removed by default in 1.22 because of CVE-2021-25740. See
                https://issue.k8s.io/103675. This can allow writers to direct LoadBalancer
                or Ingress implementations to expose backend IPs that would not otherwise
                be accessible, and can circumvent network policies or security controls
                intended to prevent/isolate access to those backends.      
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  endpoints  []                 []              [create delete deletecollection patch update]
````

## Service Accounts

Um service account é um usuário no kubernetes que pode assumir uma determinada role ou cluster role. Um service account sempre é criado para um namespace definido por obrigação. Caso não seja passado o namespace será no default.

Criando uma service account pelo cli.

````bash
❯ k create sa david   

❯ k describe sa david
Name:                david
Namespace:           default
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   david-token-g7wq2
Tokens:              david-token-g7wq2
Events:              <none>
````

Vamos criar um segundo usuario mas usando um yaml.

````bash
❯ k create -f admin-user.yaml      
serviceaccount/tribulus created

# listei por todos os namespaces pois o usuário criado foi no namespace develop
❯ k get sa --all-namespaces  
NAMESPACE         NAME                                 SECRETS   AGE
default           david                                1         37m
default           default                              1         27d
develop           default                              1         19d
develop           tribulus                             1         5s
kube-node-lease   default                              1         27d
kube-public       default                              1         27d
kube-system       attachdetach-controller              1         27d
kube-system       aws-cloud-provider                   1         27d
kube-system       aws-node                             1         27d
kube-system       certificate-controller               1         27d
kube-system       clusterrole-aggregation-controller   1         27d
kube-system       coredns                              1         27d
kube-system       cronjob-controller                   1         27d
kube-system       daemon-set-controller                1         27d
kube-system       default                              1         27d
kube-system       deployment-controller                1         27d
kube-system       disruption-controller                1         27d
kube-system       eks-vpc-resource-controller          1         27d
kube-system       endpoint-controller                  1         27d
kube-system       endpointslice-controller             1         27d
kube-system       endpointslicemirroring-controller    1         27d
kube-system       ephemeral-volume-controller          1         27d
kube-system       expand-controller                    1         27d
kube-system       generic-garbage-collector            1         27d
kube-system       horizontal-pod-autoscaler            1         27d
kube-system       job-controller                       1         27d
kube-system       kube-proxy                           1         27d
kube-system       namespace-controller                 1         27d
kube-system       node-controller                      1         27d
kube-system       persistent-volume-binder             1         27d
kube-system       pod-garbage-collector                1         27d
kube-system       pv-protection-controller             1         27d
kube-system       pvc-protection-controller            1         27d
kube-system       replicaset-controller                1         27d
kube-system       replication-controller               1         27d
kube-system       resourcequota-controller             1         27d
kube-system       root-ca-cert-publisher               1         27d
kube-system       service-account-controller           1         27d
kube-system       service-controller                   1         27d
kube-system       statefulset-controller               1         27d
kube-system       tagging-controller                   1         27d
kube-system       ttl-after-finished-controller        1         27d
kube-system       ttl-controller                       1         27d
kube-system       vpc-resource-controller              1         27d
production        default                              1         19d
tools             default                              1         19d
````

> Um detalhe observado é que se for criado um namespace, sempre criará um serviceaccount chamado default. pq isso acontece?

````bash
❯ k create namespace teste
namespace/teste created

❯ k get sa -n teste        
NAME      SECRETS   AGE
default   1         11s

❯ k delete namespaces teste
namespace "teste" deleted
````

## ClusterRoleBinding e RoleBinding

Esses dois objetos do kubernetes fazem a associação de uma clusterrole ou uma role com um serviceaccount (usuario).

Vamos ver o que temos por default

````bash
❯ k get clusterrolebindings.rbac.authorization.k8s.io                 
NAME                                                   ROLE                                                               AGE
aws-node                                               ClusterRole/aws-node                                               26d
cluster-admin                                          ClusterRole/cluster-admin                                          26d
eks:addon-manager                                      ClusterRole/eks:addon-manager                                      26d
eks:certificate-controller                             ClusterRole/system:controller:certificate-controller               26d
eks:certificate-controller-approver                    ClusterRole/eks:certificate-controller-approver                    26d
eks:certificate-controller-signer                      ClusterRole/eks:certificate-controller-signer                      26d
eks:cloud-controller-manager                           ClusterRole/eks:cloud-controller-manager                           26d
eks:cloud-provider-extraction-migration                ClusterRole/eks:cloud-provider-extraction-migration                26d
eks:cluster-event-watcher                              ClusterRole/eks:cluster-event-watcher                              26d
eks:fargate-manager                                    ClusterRole/eks:fargate-manager                                    26d
eks:fargate-scheduler                                  ClusterRole/eks:fargate-scheduler                                  26d
eks:k8s-metrics                                        ClusterRole/eks:k8s-metrics                                        26d
eks:kube-proxy                                         ClusterRole/system:node-proxier                                    26d
eks:kube-proxy-fargate                                 ClusterRole/system:node-proxier                                    26d
eks:kube-proxy-windows                                 ClusterRole/system:node-proxier                                    26d
eks:node-bootstrapper                                  ClusterRole/eks:node-bootstrapper                                  26d
eks:node-manager                                       ClusterRole/eks:node-manager                                       26d
eks:nodewatcher                                        ClusterRole/eks:nodewatcher                                        26d
eks:pod-identity-mutating-webhook                      ClusterRole/eks:pod-identity-mutating-webhook                      26d
eks:podsecuritypolicy:authenticated                    ClusterRole/eks:podsecuritypolicy:privileged                       26d
eks:tagging-controller                                 ClusterRole/eks:tagging-controller                                 26d
system:basic-user                                      ClusterRole/system:basic-user                                      26d
system:controller:attachdetach-controller              ClusterRole/system:controller:attachdetach-controller              26d
system:controller:certificate-controller               ClusterRole/system:controller:certificate-controller               26d
system:controller:clusterrole-aggregation-controller   ClusterRole/system:controller:clusterrole-aggregation-controller   26d
system:controller:cronjob-controller                   ClusterRole/system:controller:cronjob-controller                   26d
system:controller:daemon-set-controller                ClusterRole/system:controller:daemon-set-controller                26d
system:controller:deployment-controller                ClusterRole/system:controller:deployment-controller                26d
system:controller:disruption-controller                ClusterRole/system:controller:disruption-controller                26d
system:controller:endpoint-controller                  ClusterRole/system:controller:endpoint-controller                  26d
system:controller:endpointslice-controller             ClusterRole/system:controller:endpointslice-controller             26d
system:controller:endpointslicemirroring-controller    ClusterRole/system:controller:endpointslicemirroring-controller    26d
system:controller:ephemeral-volume-controller          ClusterRole/system:controller:ephemeral-volume-controller          26d
system:controller:expand-controller                    ClusterRole/system:controller:expand-controller                    26d
system:controller:generic-garbage-collector            ClusterRole/system:controller:generic-garbage-collector            26d
system:controller:horizontal-pod-autoscaler            ClusterRole/system:controller:horizontal-pod-autoscaler            26d
system:controller:job-controller                       ClusterRole/system:controller:job-controller                       26d
system:controller:namespace-controller                 ClusterRole/system:controller:namespace-controller                 26d
system:controller:node-controller                      ClusterRole/system:controller:node-controller                      26d
system:controller:persistent-volume-binder             ClusterRole/system:controller:persistent-volume-binder             26d
system:controller:pod-garbage-collector                ClusterRole/system:controller:pod-garbage-collector                26d
system:controller:pv-protection-controller             ClusterRole/system:controller:pv-protection-controller             26d
system:controller:pvc-protection-controller            ClusterRole/system:controller:pvc-protection-controller            26d
system:controller:replicaset-controller                ClusterRole/system:controller:replicaset-controller                26d
system:controller:replication-controller               ClusterRole/system:controller:replication-controller               26d
system:controller:resourcequota-controller             ClusterRole/system:controller:resourcequota-controller             26d
system:controller:root-ca-cert-publisher               ClusterRole/system:controller:root-ca-cert-publisher               26d
system:controller:route-controller                     ClusterRole/system:controller:route-controller                     26d
system:controller:service-account-controller           ClusterRole/system:controller:service-account-controller           26d
system:controller:service-controller                   ClusterRole/system:controller:service-controller                   26d
system:controller:statefulset-controller               ClusterRole/system:controller:statefulset-controller               26d
system:controller:ttl-after-finished-controller        ClusterRole/system:controller:ttl-after-finished-controller        26d
system:controller:ttl-controller                       ClusterRole/system:controller:ttl-controller                       26d
system:coredns                                         ClusterRole/system:coredns                                         26d
system:discovery                                       ClusterRole/system:discovery                                       26d
system:kube-controller-manager                         ClusterRole/system:kube-controller-manager                         26d
system:kube-dns                                        ClusterRole/system:kube-dns                                        26d
system:kube-scheduler                                  ClusterRole/system:kube-scheduler                                  26d
system:monitoring                                      ClusterRole/system:monitoring                                      26d
system:node                                            ClusterRole/system:node                                            26d
system:node-proxier                                    ClusterRole/system:node-proxier                                    26d
system:public-info-viewer                              ClusterRole/system:public-info-viewer                              26d
system:service-account-issuer-discovery                ClusterRole/system:service-account-issuer-discovery                26d
system:volume-scheduler                                ClusterRole/system:volume-scheduler                                26d
vpc-resource-controller-rolebinding                    ClusterRole/vpc-resource-controller-role                           26d
````

Agora vamos descrever uma delas para ver.

````bash
❯ k describe clusterrolebindings.rbac.authorization.k8s.io cluster-admin
Name:         cluster-admin
Labels:       kubernetes.io/bootstrapping=rbac-defaults
Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
Role:
  Kind:  ClusterRole
  Name:  cluster-admin
Subjects:
  Kind   Name            Namespace
  ----   ----            ---------
  Group  system:masters
````

Agora vamos vincular o usuario david a uma role de cluster admin. Observe que quando passei a service account é necessaário passar nasmespace:usuario pois pode existir o mesmo usuario em um namespace diferentes.

````bash
❯ k create clusterrolebinding david-crb --serviceaccount=default:david --clusterrole=cluster-admin  
clusterrolebinding.rbac.authorization.k8s.io/david-crb created

# Observer que o cluster role binding tem um nome e mostra qual a role esse binding esta feito, mas não mostra o usuario na saída do get
❯ k get clusterrolebindings.rbac.authorization.k8s.io | grep david-crb
david-crb                                              ClusterRole/cluster-admin                                          8m29s

## Para ver a saída com o usuario use o -o wide
❯ k get clusterrolebindings.rbac.authorization.k8s.io -o wide -name david-crb
NAME        ROLE                        AGE   USERS   GROUPS   SERVICEACCOUNTS
david-crb   ClusterRole/cluster-admin   14m                    default/david

❯ k describe clusterrolebindings.rbac.authorization.k8s.io david-crb 
Name:         david-crb
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  ClusterRole
  Name:  cluster-admin
Subjects:
  Kind            Name   Namespace
  ----            ----   ---------
  ServiceAccount  david  default
````

> Com um describe em um service account ele não mostra quais as roles esse usuario tem. Somente através do cluster role binding.

Vamos usar o arquivo yaml para vincular o admin-user ao cluster-admin

````bash
❯ k create -f admin-user-rcbinding.yaml
clusterrolebinding.rbac.authorization.k8s.io/admin-user created

❯ k describe clusterrolebindings.rbac.authorization.k8s.io admin-user
Name:         admin-user
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  ClusterRole
  Name:  cluster-admin
Subjects:
  Kind            Name        Namespace
  ----            ----        ---------
  ServiceAccount  admin-user  kube-system
````