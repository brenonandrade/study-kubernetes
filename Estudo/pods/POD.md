# PODS (alias po)

https://kubernetes.io/docs/concepts/workloads/pods/

Pode é a menor objeto do kubernetes. Dentro de um pod dode ser declarado um ou mais containers, mas o usual é somente 1. Na verdade um pod é um container com containers dentro.

Um pod tem um ip único não nateado e todos os containers deste pod usam o mesmo ip mudando somente as portas que eles expõe.

Quando um pod esta iniciando ele fica com o status de `Pending` e depois muda para `Running`. Se todos os containers finalizarem de forma correta ele passa para a `Sucessed` porém se algum de seus containers tiver problemas passa para ``Failed`. Se não for possível conseguir o estado do pod ele passará para `Unknow`.

Por outro lado os containers dentro dos pods também tem estágios. `Waiting` é o estágio que o container está esperando para a inicialização, como poder exemplo estar esperando a imagem ser baixada. `Running` é o estado que o container está rodando sem problemas e `Terminated` indica que o container parou seja por que terminal sua tarefa ou por falha.

Se um node morrer e existir pods nele, estes são agendados para exclusão e novos pods são recriados em outros containers.

O pod é um conjunto de containers rodando, logo entra

Um pod só consegue existir se tiver recurso para ele ser criado.

>Várias parâmetros podem ser definidos dentro de um pod, aqui é só o principal para se familiarizar com o recurso e depois será melhor visto em outras explicações.

````bash
#usando o kubens posso mudar de namespace como mostrei em em outros arquivo
❯ kubens production  
Context "arn:aws:eks:us-east-1:513498712583:cluster/us-east-1-prod-cluster" modified.
Active namespace is "production".

# como foi criado no namespace de production nao precisei passar o namespace, mas caso vc não faça assim só parar o -n
❯ k get pods
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          111m

❯ k get pods -n production
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          114m

❯ k get pods -n production
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          114m

❯ k get pods
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          111m

~/pessoais/study-kubernetes/Estudo/pods develop !2 ?1                    ⎈ us-east-1-prod-cluster/production  1.2.8 17:11:15
❯ k get pods -n production
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          114m

# Para conferir o pod usamos o describe
❯ k describe po nginx-pod 
Name:             nginx-pod
Namespace:        production
Priority:         0
Service Account:  default
Node:             ip-10-10-80-199.ec2.internal/10.10.80.199
Start Time:       Fri, 02 Sep 2022 15:19:21 -0300
Labels:           company=company
                  name=nginx-pod
                  team=devops
Annotations:      kubernetes.io/psp: eks.privileged
Status:           Running
IP:               10.10.71.253
IPs:
  IP:  10.10.71.253
Containers:
  nginx:
    Container ID:   containerd://ab2cb26d9309bc3cdaee5a018473e5ee5f64a005b2e84e9f48228e53069f4270
    Image:          nginx
    Image ID:       docker.io/library/nginx@sha256:b95a99feebf7797479e0c5eb5ec0bdfa5d9f504bc94da550c2f58e839ea6914f
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Fri, 02 Sep 2022 15:19:22 -0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     500m
      memory:  128Mi
    Requests:
      cpu:        500m
      memory:     128Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-c8hnx (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-c8hnx:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Guaranteed
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:                      <none>

# Remover um pod pode ser feito atravez do delete
❯ k delete -f pod.yaml 
pod "nginx-pod" deleted

❯ k get po 
No resources found in production namespace.
````

Esse pod criado não consegue ser acessado de fora do cluster e nem ser escalado ainda. Quem escala um pod é o replicaset.

Todo essa parte de especificação de pod também pode ser criado dentro de um replicaset, logo vc não irá criar normalmente um pod solto sem um replicaset para controlar. Dessa forma a criação de somente pods em ambiente real não é usual.

Voce pode dar um nome do pod diferente da label dele, mas geralmente costumamos dar o mesmo nome.