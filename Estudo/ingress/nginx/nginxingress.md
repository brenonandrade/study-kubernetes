# Nginx Ingress Controller

## Preparação do ambientes

e cada deployment com o seu service. Cada um desses apps respondem uma pagina estatica com o nome do autor, coisa bem simples. O que nos interessa é conseguir separar as requisições usando o ingress.

Na pasta [ingress](../Estudo/ingress/) temos todos os arquivos necessários. Aplique os app1, app2, svc1 e svc2.

````bash
❯ k create -f app1.yaml
deployment.apps/app1 created

❯ k create -f app2.yaml
deployment.apps/app2 created

❯ k create -f svc1.yaml
service/app1 created

❯ k create -f svc2.yaml
service/app2 created

❯ k get deploy
NAME   READY   UP-TO-DATE   AVAILABLE   AGE
app1   2/2     2            2           2m51s
app2   2/2     2            2           2m51sinternal   <none>           <none>

# Otimos uqe ficou um pod em cada node usando replica para 2.
# Mais um exemplo que o get consegue pegar varios ao mesmo tempo num unico comando
❯ k get deploy,svc,po,ep -o wide
NAME                   READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                      SELECTOR
deployment.apps/app1   2/2     2            2           19m   app1         dockersamples/static-site   app=app1
deployment.apps/app2   2/2     2            2           19m   app2         dockersamples/static-site   app=app2

NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE   SELECTOR
service/app1         ClusterIP   172.20.254.153   <none>        80/TCP    19m   app=app1
service/app2         ClusterIP   172.20.89.10     <none>        80/TCP    19m   app=app2
service/kubernetes   ClusterIP   172.20.0.1       <none>        443/TCP   28d   <none>

NAME                        READY   STATUS    RESTARTS   AGE    IP             NODE                          NOMINATED NODE   READINESS GATES
pod/app1-788bb997b4-j4hss   1/1     Running   0          111s   10.10.62.218   ip-10-10-35-53.ec2.internal   <none>           <none>
pod/app1-788bb997b4-qnzhn   1/1     Running   0          110s   10.10.89.113   ip-10-10-70-52.ec2.internal   <none>           <none>
pod/app2-5d884f5687-jkdc6   1/1     Running   0          104s   10.10.93.155   ip-10-10-70-52.ec2.internal   <none>           <none>
pod/app2-5d884f5687-ww798   1/1     Running   0          105s   10.10.60.90    ip-10-10-35-53.ec2.internal   <none>           <none>

NAME                   ENDPOINTS                            AGE
endpoints/app1         10.10.62.218:80,10.10.89.113:80      19m
endpoints/app2         10.10.60.90:80,10.10.93.155:80       19m
endpoints/kubernetes   10.10.181.230:443,10.10.183.85:443   28d
````

Entre em um dos pods, instale o curl e bata nos ips para conferir se esta tudo ok. O app1 consegue fazer um curl do app2 tranquilamente, pois estão na mesma rede....

````bash
❯ k exec -ti app1-767f4d7f7c-98nh9 -- sh
# apt update && apt install curl -y
# curl 10.10.34.36
# curl curl 10.10.34.36
# #Vai ver a saida aqui com o author correta de quem vc apontou.
````

## Ingress Nginx

<https://docs.nginx.com/nginx-ingress-controller/>


<https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/>

A primeira coisa que vamos fazer é criar um namespace e um service account e configurar o rbac.

````bash
❯ k create -f ns-sa.yaml 
namespace/nginx-ingress created
serviceaccount/nginx-ingress created
````

Agora vamos criar um role e vincular ao nosso usuario.

````bash
❯ k create -f rbac.yaml 
clusterrole.rbac.authorization.k8s.io/nginx-ingress created
clusterrolebinding.rbac.authorization.k8s.io/nginx-ingress created
````

Agora vamos criar um secret para tls

````bash
❯ k create -f default-server-secret.yaml 
secret/default-server-secret created
````

Agora vamos criar um configmap que é dentro desse configma que podemos fazer as configurações futuras quando for levantar um de fato o ingress controller

````bash
❯ k create -f nginx-config.yaml
configmap/nginx-config created
````

Vamos criar o nosso recurso ingressClass
<https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.2/guide/ingress/ingress_class/>

````bash
❯ k apply -f ingress-class.yaml 
ingressclass.networking.k8s.io/nginx created
````

Se você quiser definir o Ingress Controller como padrão, remova o comentário da anotação ingressclass.kubernetes.io/is-default-class. Com esta anotação definida como verdadeira, todos os novos Ingresses sem um campo ingressClassName especificado serão atribuídos a este IngressClass.

> O Ingress Controller falhará ao iniciar sem um recurso IngressClass.Nota

Conferindo...

````bash
❯ k get sa,ingressclasses.networking.k8s.i,cm,secrets -n nginx-ingress
NAME                           SECRETS   AGE
serviceaccount/default         1         45m
serviceaccount/nginx-ingress   1         45m

NAME                                   CONTROLLER                     PARAMETERS   AGE
ingressclass.networking.k8s.io/nginx   nginx.org/ingress-controller   <none>       23m

NAME                         DATA   AGE
configmap/kube-root-ca.crt   1      45m
configmap/nginx-config       1      29m

NAME                               TYPE                                  DATA   AGE
secret/default-server-secret       kubernetes.io/tls                     2      33m
secret/default-token-7wln4         kubernetes.io/service-account-token   3      45m
secret/nginx-ingress-token-jccl5   kubernetes.io/service-account-token   3      45m
````

Algumas extensões da api para que o nginx funcione são necessárias de acordo com a documentação, sem isso os pods do nginx controller não ficarão em estado de ready.

````bash
❯ k create -f custom-resource-virtualserver.yaml 
customresourcedefinition.apiextensions.k8s.io/virtualservers.k8s.nginx.org created

❯ k create -f custom-resource-virtualserverroutes.yaml 
customresourcedefinition.apiextensions.k8s.io/virtualserverroutes.k8s.nginx.org created

❯ k create -f custom-resource-transportserver.yaml 
customresourcedefinition.apiextensions.k8s.io/transportservers.k8s.nginx.org created

❯ k create -f custom-resource-policies.yaml         
customresourcedefinition.apiextensions.k8s.io/policies.k8s.nginx.org created

❯ k get customresourcedefinitions.apiextensions.k8s.io
NAME                                         CREATED AT
eniconfigs.crd.k8s.amazonaws.com             2022-08-25T19:26:44Z
globalconfigurations.k8s.nginx.org           2022-09-23T20:40:59Z
policies.k8s.nginx.org                       2022-09-26T16:40:00Z
securitygrouppolicies.vpcresources.k8s.aws   2022-08-25T19:26:47Z
transportservers.k8s.nginx.org               2022-09-26T16:39:50Z
virtualserverroutes.k8s.nginx.org            2022-09-26T16:39:41Z
virtualservers.k8s.nginx.org                 2022-09-26T16:24:02Z
````

Na documentação existe uma extensão para esse ingress ser um load balancer para conexoes tcp e upd. Esse também é uma extensão que precisa ser feita na api.

````bash
❯ k create -f custom-resource-tcp-upd-global-config.yaml 
customresourcedefinition.apiextensions.k8s.io/globalconfigurations.k8s.nginx.org created
````

Existem 2 metodos de deploy. O primeiro é usando um deploy padrão e vc definirá o número de replicas. O segundo é via daemonset que executará um em cada node do cluster. Na documentação é possível achar os dois métodos. Vamos fazer com o deployment e replicaset mesmo.

Algumas mexidas foram feitas nesse arquivo perante ao original.

- Adicionado número de revisões
- CPU e Memory limits
- Grade Period
- Anotations para o prometheus

````bash
❯ k create -f nginx-ingress-controller.yaml

❯ k get pod
NAME                             READY   STATUS    RESTARTS   AGE
nginx-ingress-6db9ccb858-47xj4   1/1     Running   0          6s
nginx-ingress-6db9ccb858-xz9lm   1/1     Running   0          8s
````

Além disso esse controller precisa do seu service para gerar um load balancer específico para ele. É interessante nesse caso fixar a porta que será usada pelo nodeport.

````bash
❯ k create -f nginx-ingress-controller-svc.yaml 
service/nginx-ingress created

❯ k get svc                                     
NAME            TYPE           CLUSTER-IP       EXTERNAL-IP                                                              PORT(S)                      AGE
nginx-ingress   LoadBalancer   172.20.200.219   a03f3ab8a351341819dcbadaef7af2f5-946151604.us-east-1.elb.amazonaws.com   80:30080/TCP,443:30443/TCP   3m4s
````

A partir de agora teremos 1 load balancer e vamos apontar para todos os apps por dentro dele atraves do recurso de ingress que foi extendido na api.

A partir de agora precisamos fazer alguams regras do ingress para bater na nossa aplicação.

Se batermos na url do load balancer, ele não tem nenhum reapontamento, vai devolver 404.

![404](./resources/404.png)

A partir de agora vamos fazer objeto ingress para apontar para os nossos aps.

````bash
❯ k create -f ingress.yaml
ingress.networking.k8s.io/nginx-ingress created

❯ k get ingress
NAME            CLASS    HOSTS                                                                   ADDRESS   PORTS   AGE
nginx-ingress   <none>   a03f3ab8a351341819dcbadaef7af2f5-946151604.us-east-1.elb.amazonaws.co             80      11s

❯ k get ingress nginx-ingress 
NAME            CLASS    HOSTS                                                                    ADDRESS   PORTS   AGE
nginx-ingress   <none>   a03f3ab8a351341819dcbadaef7af2f5-946151604.us-east-1.elb.amazonaws.com             80      42m
````
