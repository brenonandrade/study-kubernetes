# Namespaces

O Kubernetes oferece suporte a vários clusters virtuais apoiados pelo mesmo cluster físico. Esses clusters virtuais são chamados de namespaces.

Os namespaces fornecem um escopo para nomes. Os nomes dos recursos precisam ser exclusivos em um namespace, mas não entre namespace.

Na verdade, o Kubernetes não implementa nenhum tipo de separação de privilégios. Em vez disso, ele delega esses controles a um plug-in de autorização dedicado. Por padrão, o RBAC (para controle de acesso baseado em função) é usado. Um namespaces somente se atenta aos recusos e não aos privilégios.
