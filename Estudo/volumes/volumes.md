# Volumes

Quando criamos um volume dentro do pod na verdade estamos apontando para um volume já criado. Seja pelo próprio kubernetes, nfs, s3, etc. Voce cria uma configuração para este volume e dentro do container, aponta onde esse volume irá montar sua pasta.

É possível criar um volume físico na definição de um pod, mas esse volume náo é persistente e morrerá junto com o pod. Esse volume é chamado `emptyDir`. Vamos começar por este recurso que será mais fácil.

## Emptydir

Leia atentamente o exemplo [podemptyvol.yaml](./podemptyvol.yaml) e seus comentários. Observe que náo declarei tudo que pode ser declarado em um pod para focar na opção do volume.

````bash
❯ k create -f podemptyvol.yaml
pod/busybox created

❯ k describe po -n develop busybox
Name:             busybox
Namespace:        develop
Priority:         0
Service Account:  default
Node:             ip-10-10-35-53.ec2.internal/10.10.35.53
Start Time:       Mon, 19 Sep 2022 12:49:30 -0300
Labels:           app=busybox
Annotations:      kubernetes.io/psp: eks.privileged
Status:           Running
IP:               10.10.40.137
IPs:
  IP:  10.10.40.137
Containers:
  busybox:
    Container ID:   containerd://25cddc324007e94f28c290ceafb949bf7e1e7e52e08901c2cb5d9d24dac8204b
    Image:          busybox
    Image ID:       docker.io/library/busybox@sha256:ad9bd57a3a57cc95515c537b89aaa69d83a6df54c4050fcf2b41ad367bec0cd5
    Port:           <none>
    Host Port:      <none>
    State:          Waiting
      Reason:       CrashLoopBackOff
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 19 Sep 2022 13:10:27 -0300
      Finished:     Mon, 19 Sep 2022 13:10:27 -0300
    Ready:          False
    Restart Count:  9
    Limits:
      cpu:     200m
      memory:  500Mi
    Requests:
      cpu:        100m
      memory:     200Mi
    Environment:  <none>
    # Aqui ele mostra o mount point de /app apontando para meu-dir
    Mounts:
      /app from meu-dir (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-rs7lx (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
# Veja que ele mesmo fala que é um diretório temporário e tem o mesmo ciclo de vida que o pod
  meu-dir:
    Type:       EmptyDir (a temporary directory that shares a pods lifetime)
    Medium:     
    SizeLimit:  <unset>
  kube-api-access-rs7lx:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason     Age                  From               Message
  ----     ------     ----                 ----               -------
  Normal   Scheduled  24m                  default-scheduler  Successfully assigned develop/busybox to ip-10-10-35-53.ec2.internal
  Normal   Pulled     24m                  kubelet            Successfully pulled image "busybox" in 522.345414ms
  Normal   Pulled     24m                  kubelet            Successfully pulled image "busybox" in 144.088817ms
  Normal   Pulled     24m                  kubelet            Successfully pulled image "busybox" in 130.482832ms
  Normal   Created    23m (x4 over 24m)    kubelet            Created container busybox
  Normal   Started    23m (x4 over 24m)    kubelet            Started container busybox
  Normal   Pulled     23m                  kubelet            Successfully pulled image "busybox" in 160.404518ms
  Normal   Pulling    22m (x5 over 24m)    kubelet            Pulling image "busybox"
  Normal   Pulled     22m                  kubelet            Successfully pulled image "busybox" in 115.022324ms
  Warning  BackOff    4m9s (x94 over 24m)  kubelet            Back-off restarting failed container

#Caindo para dentro do container podemos ver que existe a pasta app
  ❯ k exec -ti -n develop busybox -- sh
/ # ls
app   bin   dev   etc   home  proc  root  sys   tmp   usr   var
/ # 

k delete po -n develop busybox
````

> Pergunta.. Se passarmos isso para um replicaset e criarmos arquivos dentro... um enxerga o outro?

````bash
❯ k create -f replicaemptyvol.yaml

❯ k get pod -n develop            
NAME            READY   STATUS    RESTARTS   AGE
busybox-hmrgt   1/1     Running   0          45s
busybox-nrgft   1/1     Running   0          45s
busybox-w949f   1/1     Running   0          45s

# Criando um arquivo na pod hmrgt
❯ k exec -ti -n develop busybox-hmrgt -- sh
/ # echo "teste" >> /app/teste
/ # cd /app/
/app # ls
teste

# Procurando o arquivo em outro pod
❯ k exec -ti -n develop busybox-nrgft -- sh 
/ # cd /app/
/app # ls
/app # 
````

Podemos constatar que no `emptyDir` cada volume nesse caso é exclusivo do seu próprio pod.

No caso se apontassemos para um s3 por exemplo (já criado anteriormente) todos os pods compartilhariam esse mesmo storage.

> O emptyDir é interessante para agregador de log. Descarregar todo o log da aplicação em um diretório e um sidecar por exemplo envia os logs para um lugar específico, exemplo graylog. O EmptyDir também pode ser usado para compartilhar dados entre os containers do mesmo pod, fazendo os dois ter o mesmo ponto de montagem.

O local onde fica o nosso volume onde é?
Para vc encontrar esse volume no host execute
find /var/lib/kubelet/pods/ -iname nomedopod que mostrará os diretório.
Se vc criar um arquivo lá vai refletir no pod.
## PersistenteVolume(PV) e PersistentVolumeClaim(PVC)

Volumes e PersistentVolumes diferem das seguintes maneiras:

- Um Volume separa o armazenamento de um contêiner, mas o vincula a um Pod, enquanto os PVs separam o armazenamento de um Pod.

- O ciclo de vida de um volume depende do Pod que o usa, enquanto o ciclo de vida de um PV não.

- Um Volume permite a reinicialização segura do contêiner e permite o compartilhamento de dados entre os contêineres, enquanto um PV permite o encerramento ou a reinicialização segura do Pod.

Um arquivo YAML de manifesto separado é necessário para criar um PV, mas não é necessário para um volume.

<https://kubernetes.io/pt-br/docs/concepts/storage/persistent-volumes/>

volume persistente vem com o luxo adicional de ser independente do pod ao qual está conectado, tornando-os completamente independentes do ciclo de vida do pod.

Com o volume persistente, um caso de uso simples é que precisamos implantar o contêiner com estado com um banco de dados que precisa de um volume persistente existente para que seu contêiner ou pod possa acessar os dados sempre que for agendado ou reiniciado.

É como se vc pegasse um disco, um compartilhamento, ou alguma coisa do gênero e fizesse com que fosse enxergado como um disco para o cluster.

Claro que é necessário criar um volume na aws antes 

````bash
aws ec2 create-volume --availability-zone=us-east-1a --size=10 --volume-type=gp2
````

Se analisarmos a saída o que nos interessa aqui é o VolumeType e o VolumeId para montar  nossos persistente volume.

````json
{
    "AvailabilityZone": "us-east-1a",
    "CreateTime": "2022-09-19T18:25:13+00:00",
    "Encrypted": false,
    "Size": 10,
    "SnapshotId": "",
    "State": "creating",
    "VolumeId": "vol-0502bd3d3733ca5e6",
    "Iops": 100,
    "Tags": [],
    "VolumeType": "gp2",
    "MultiAttachEnabled": false
}
````

Aplique agora o persistvol.yaml e leia para entender cada um dos parametros.

````bash
❯ k create -f persistvol.yaml

❯ k get pv pv -o yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  creationTimestamp: "2022-09-19T18:47:48Z"
  finalizers:
  - kubernetes.io/pv-protection
  name: pv
  resourceVersion: "4751670"
  uid: 4ac811f9-f296-41b2-9dd4-08977e403a71
spec:
  accessModes:
  - ReadWriteMany
  awsElasticBlockStore:
    fsType: xfs
    volumeID: aws://us-east-1a/vol-0502bd3d3733ca5e6
  capacity:
    storage: 1Gi
  persistentVolumeReclaimPolicy: Retain
  storageClassName: gp2-retain
  volumeMode: Filesystem
status:
  phase: Available
````

> No arquivo de criação eu tinha definido um namespace, mas o recurso não ficou com um namespace definido.

PVs e PVCs diferem em provisionamento, funcionalidades e a pessoa responsável por criá-los, especificamente:

- Os PVs são criados pelo administrador do cluster ou dinamicamente pelo Kubernetes, enquanto os usuários/desenvolvedores criam os PVCs.
- Os PVs são recursos de cluster fornecidos por um administrador, enquanto os PVCs são uma solicitação de armazenamento e recursos de um usuário.
- Os PVCs consomem recursos dos PVs, mas não vice-versa.
- Um `PV é semelhante a um nó em termos de recursos de cluster`, enquanto um PVC é como um Pod no contexto de consumo de recursos de cluster. Esse é o motivo pelo qual ele não tem namespace, assim como um node também não tem.



A próxima etapa é criar a cadeia de volume persistente para anexá-la ao pod, um PersistentVolumeClaim (PVC) é uma solicitação de armazenamento por um usuário que fará uma partição do volume persistente. Abaixo está o exemplo para criar o PVC

Depois de um persistente volume estar criado, vc precisa attachar esse volume a um ou alguns pod, e usamos para isso o `Persistente volume claim`.

Se vc está na cloud da aws provavelmente vai usar um elastic block storage, logo vamos faze ro uso dele aqui tb.





# apiVersion: v1
# kind: PersistentVolume
# metadata:
#   name: volume-teste
#   labels:
#     type: amazonEBS
# spec:
#   capacity:
#     storage: 1Gb
#   accessModes:
#     # - ReadWriteMany # varios nodes podem  ler e escrever
#     - ReadWriteOnce # Somente um node pode escrever
#     # - ReadOnlyMany # geral pode ler

#   # Essa parte é o que acontece com os dados após o volumeclaim ser removido, que veremos mais pra frente 
#   # - Retain mantem os dados
#   # - Delete apara os dados quando o claim é deletado
#   # - Recycle deixa disponível o volume para caso vc fizer novamente o claim
#   persistentVolumeReclaimPolicy: Retain
#   awsElasticBlockStore:
#     volumeID: vol-0502bd3d3733ca5e6
#     fsType: ext4


# apiVersion: storage.k8s.io/v1
# kind: StorageClass
# metadata:
#   name: pv0003
#   labels:
#     type: amazonEBS
# provisioner: kubernetes.io/aws-ebs
# parameters:
#   type: gp2
# reclaimPolicy: Retain
# allowVolumeExpansion: true
# mountOptions:
#   - debug
# volumeBindingMode: Immediate

