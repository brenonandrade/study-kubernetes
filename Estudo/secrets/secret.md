# Secrets

<https://kubernetes.io/docs/concepts/configuration/secret/>

Secrets é um objeto que guarda dados confidenciais como senhas, tokens, keys, etc. A vantagem par ao uso de secrets que que estas podem ser criadas independente do pod ou deixá-las expostas no código ou no nas variaveis de ambiente do pod do seu deployment.

> Os secrets dos pods são por padrão armazenados sem criptografia no etcd. Qualquer pessoa com acesso a api pode recuperar e modificar um segredo.

Existem melhores maneiras de gerenciar isso, como o uso do vault do ansible e até melhor com o Vault da hashicorp, mas por hora vamos entender como isso funciona.

Vamos usar o arquivo [secret.txt](../Estudo/secrets/secrets.txt) para criar um secret via cli.

````bash
# Além do generic pode ser usado o docker-registry ou tls
❯ k create secret generic my-secret --from-file=secret.txt 
secret/my-secret created

❯ k get secrets 
NAME                  TYPE                                  DATA   AGE
default-token-sfhgx   kubernetes.io/service-account-token   3      17d
my-secret             

❯ k describe secrets my-secret 
Name:         my-secret
Namespace:    production
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
secret.txt:  21 bytes

# Observe que na frente de secret.txt temos um hash
❯ k get secrets my-secret -o yaml
apiVersion: v1
data:
  secret.txt: MTEzMmRmc2NhcmRhcjM0MmRhNDU2
kind: Secret
metadata:
  creationTimestamp: "2022-09-20T15:29:45Z"
  name: my-secret
  namespace: production
  resourceVersion: "4924534"
  uid: 449ee9b3-3f9f-4fce-8192-b7f3befb032e
type: Opaque
````

Essa chave codificada é uma chave base64 que pode ser decodificada de forma muito simples.

````bash
❯ echo MTEzMmRmc2NhcmRhcjM0MmRhNDU2 | base64 --decode
1132dfscardar342da456 
````

E a saída nada mais é do que o conteúdo do nosso arquivo.

Se vc criar um secret usando o yaml, terá que passar o conteúdo desse arquivo no yaml, logo mata um problema gerando outro, pois um secret não deveria revelar o conteúdo via codigo e por isso é interessante somente fazer via cli apesar de não ser seguro ainda.

> O uso do secret não é a melhor opçào justamente por conta de ser facilmente decodificado, mas em caso de necessidade até ser encontrado uma solução mais adequada para a sua equipe pode ser um quebra galho.

Usando esse secret que acabamos de criar, vamos montar um pod simples para ter acesso a este arquivo. Leia antes o [podsecret.yaml](./podsecret.yaml).

````bash
❯ k create -f podsecretvol.yaml 
pod/test-secret created

# Veja que na pasta tem o arquivo e o valor já decodificado
❯ k exec -ti test-secret -- sh  
/ # cd app/
/app # ls
secret.txt
/app # cat secret.txt 
1132dfscardar342da456/app # 
````

> Só por curiosidade se vc montar um pod em um namespace e a secret tiver em outro o pod não conseguirá inicializar e o kubelet avisará que teve um erro para montar o .

Agora vamos criar um secret com chave valores simples ao inves de arquivo

````bash
❯ k create secret generic secret-key-value --from-literal user=david --from-literal password=admin
secret/secret-key-value created

❯ k describe secrets secret-key-value
Name:         secret-key-value
Namespace:    production
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  5 bytes
user:      5 bytes

❯ k get secrets  secret-key-value -o yaml
apiVersion: v1
data:
# os dados encriptados
  password: YWRtaW4=
  user: ZGF2aWQ=
kind: Secret
metadata:
  creationTimestamp: "2022-09-20T17:11:47Z"
  name: secret-key-valeu
  namespace: production
  resourceVersion: "4938787"
  uid: afa665aa-8089-4730-9775-14d534f59670
type: Opaque
````

Mais uma vez se eu pegar os dados encriptados e decodificar

````bash
❯ echo YWRtaW4= | base64 --decode
admin                                                                                                        
❯  echo ZGF2aWQ= | base64 --decode
david 
````

Vamos agora montar um pod com variáveis de ambiente usando secrets. Veja que foi removido toda parte de volume e definimos envs pegando de uma secret especifica uma chave específica

````bash
❯ k create -f podsecretvar.yaml
pod/test-secret created

❯ k exec -ti  test-secret -- sh 
/ # echo $USERNAME
david
/ # echo $PASSWORD
admin

❯ k delete po test-secret 
pod "test-secret" deleted
````

> Se vc vai criar via cli uma secret com vários parametros chave e valor, como faríamos para adicionar uma nova secret?

Agora vamos falar um pouco da verdade. Voce vai usar secrets? Talvez não, pois é possível definir uma configuração para o container usando o configmap e nesta configuração vc pode passar tudo que precisa.
