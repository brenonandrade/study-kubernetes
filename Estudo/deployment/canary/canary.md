# Canary Deploy

Para entender o canary deploy também é necessário ter passado pelo service antes. Logo se chegou até aqui sem conhecer os fundamentos do service, volte depois!

## O que é o canary deploy?

O canary deploy é um recurso muito útil para testar uma aplicação em um ambiente de produção. Sobe-se uma porcentagem de uma versão nova junto com a versão antiga para avaliar possíveis erros. Com o tempo vai aumentando os pods da nova versão e baixando os pods da versão antiga até atingir 100% de pods da nova versão.

## Misturando as versões

O service seleciona pods através de um selector para gerar endpoints que respondem por um serviço e não deployments. Logo é possível que dois deployments tenham as mesmas labels para pods (e tb para deployments) e os pods selecionados tenham diferentes imagens em seus containers.

## Aplicando a idéia

Vamos criar um deployment simples com uma versão específica do nginx.

````bash
❯ k create -f canarydeploy1.yaml
deployment.apps/nginx created

❯ k create -f canaryservice.yaml
service/nginx created

❯ k get svc nginx
NAME    TYPE           CLUSTER-IP       EXTERNAL-IP                                                              PORT(S)        AGE
nginx   LoadBalancer   172.20.145.136   ae26a247871f6470499a2638c26cd56d-863123621.us-east-1.elb.amazonaws.com   80:31111/TCP   10s

# Observe que temos um total de 5 endpoints para esse service
❯ k get endpoints
NAME    ENDPOINTS                                                     AGE
nginx   10.10.33.130:80,10.10.56.115:80,10.10.63.149:80 + 2 more...   2m4s

# E a imagem desses pods
❯ k describe po nginx-7c4dc844bd-w7jgf | grep Image
    Image:          nginx:1.15.0
    Image ID:       docker.io/library/nginx@sha256:62a095e5da5f977b9f830adaf64d604c614024bf239d21068e4ca826d0d629a4

````

Agora vamos criar o segundo deployment com o nginx de versao latest

````bash
❯ k create -f canarydeploy2.yaml
deployment.apps/nginx2 created

#veja que o ultimo tem 31 segundos rodando e inicia o nome nginx2
❯ k get pods  
NAME                      READY   STATUS    RESTARTS   AGE
nginx-7c4dc844bd-fhqfk    1/1     Running   0          9h
nginx-7c4dc844bd-n4scd    1/1     Running   0          9h
nginx-7c4dc844bd-w7jgf    1/1     Running   0          9h
nginx-7c4dc844bd-xnmpl    1/1     Running   0          9h
nginx-7c4dc844bd-zq2bv    1/1     Running   0          9h
nginx2-695b9cc764-xndrf   1/1     Running   0          31s

# Observe que agora temos 6 endpoints
❯ k describe svc nginx
Name:                     nginx
Namespace:                production
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx
Type:                     LoadBalancer
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       172.20.145.136
IPs:                      172.20.145.136
LoadBalancer Ingress:     ae26a247871f6470499a2638c26cd56d-863123621.us-east-1.elb.amazonaws.com
Port:                     nginx  80/TCP
TargetPort:               80/TCP
NodePort:                 nginx  31111/TCP
Endpoints:                10.10.33.130:80,10.10.56.115:80,10.10.63.149:80 + 3 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

❯ k get endpoints nginx
NAME    ENDPOINTS                                                     AGE
nginx   10.10.33.130:80,10.10.56.115:80,10.10.63.149:80 + 3 more...   9h

❯ k get endpoints -o yaml
apiVersion: v1
items:
- apiVersion: v1
  kind: Endpoints
  metadata:
    annotations:
      endpoints.kubernetes.io/last-change-trigger-time: "2022-09-19T01:02:20Z"
    creationTimestamp: "2022-09-18T15:48:30Z"
    name: nginx
    namespace: production
    resourceVersion: "4604396"
    uid: 2696d02e-7c54-4461-a9bd-a40edbe74e58
  subsets:
  - addresses:
    - ip: 10.10.33.130
      nodeName: ip-10-10-35-53.ec2.internal
      targetRef:
        kind: Pod
        name: nginx-7c4dc844bd-zq2bv
        namespace: production
        resourceVersion: "4527304"
        uid: 7dff7a87-9da0-4515-8cb6-99c4538aea02
    - ip: 10.10.56.115
      nodeName: ip-10-10-35-53.ec2.internal
      targetRef:
        kind: Pod
        name: nginx-7c4dc844bd-xnmpl
        namespace: production
        resourceVersion: "4527298"
        uid: 6f27cdcf-f281-41eb-b25c-46041a6ac81e
    - ip: 10.10.63.149
      nodeName: ip-10-10-35-53.ec2.internal
      targetRef:
        kind: Pod
        name: nginx-7c4dc844bd-fhqfk
        namespace: production
        resourceVersion: "4527301"
        uid: e99d9c22-b3c4-4aae-addc-bed9e2a84a68
    - ip: 10.10.65.224
      nodeName: ip-10-10-70-52.ec2.internal
      targetRef:
        kind: Pod
        name: nginx-7c4dc844bd-w7jgf
        namespace: production
        resourceVersion: "4527281"
        uid: 7dc25e81-8d45-44bd-a298-9c00319da0f6
    - ip: 10.10.78.41
      nodeName: ip-10-10-70-52.ec2.internal
      targetRef:
        kind: Pod
        name: nginx-7c4dc844bd-n4scd
        namespace: production
        resourceVersion: "4527282"
        uid: b4531b47-6983-49c2-89aa-3dd105e510ed
    - ip: 10.10.89.113
      nodeName: ip-10-10-70-52.ec2.internal
      targetRef:
        kind: Pod
        name: nginx2-695b9cc764-xndrf
        namespace: production
        resourceVersion: "4604393"
        uid: e1d798dc-dd5f-47a7-bb40-31dc03986b9d
    ports:
    - name: nginx
      port: 80
      protocol: TCP
kind: List
metadata:
  resourceVersion: ""
````
