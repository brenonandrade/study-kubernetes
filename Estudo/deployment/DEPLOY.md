# Deployment (alias deploy)

Um deploy é o controller do replicaset. É através do deploy que conseguimos fazer um rollback de um aplicacão.

Toda vez que atualizamos um deploy, ele gera um novo replicaset (ou daemonset) para os novos pods que serão criados com novas especificações.

A estrutura do deployment é a mesma do replicaset. Somente copiei o arquivo do replicaset e mudei o tipo do kind para deploymnet, simples assim!

Aplicando o manifesto e conferindo o que foi criado podemos ver que agora temos a sequencia de nomes maior para as coisas.

````bash
❯ k create -f deployment.yaml 
deployment.apps/nginx created

NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   5/5     5            5           78s

# O nome do replicaset possui o deploy na frente
❯ k get rs
NAME               DESIRED   CURRENT   READY   AGE
nginx-59f8b7ff5f   5         5         5       88s

#o nome dos pods possue o deploy, depois o replicaset e um numero randomico para cada pod
❯ k get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-59f8b7ff5f-47lrm   1/1     Running   0          103s
nginx-59f8b7ff5f-829m7   1/1     Running   0          103s
nginx-59f8b7ff5f-9xqvt   1/1     Running   0          103s
nginx-59f8b7ff5f-cwr7t   1/1     Running   0          103s
nginx-59f8b7ff5f-sszc2   1/1     Running   0          103s
````

Agora vamos colocar uma versão nesse nginx e ver o que acontecem.
mude a imagem no arquivo de nginx para nginx:stable

````bash
❯ k replace -f deployment.yaml
deployment.apps/nginx replaced

# Veja que o deployment ainda tem o replicaset anterior, porem zerado (59f8b7ff5f)
# O novo replicaset já tem todos os pods com a imagem stable. Vamos conferir.
production  1.2.8 14:15:13
❯ k get rs
NAME               DESIRED   CURRENT   READY   AGE
nginx-59f8b7ff5f   0         0         0       4m32s
nginx-68df9766d4   5         5         5       8s


❯ k describe pod nginx-68df9766d4-s54wr | grep Image
    Image:          nginx:stable
    Image ID:       docker.io/library/nginx@sha256:f2dfca5620b64b8e5986c1f3e145735ce6e291a7dc3cf133e0a460dca31aaf1f
````

## Rollout

Não é necessário ir no deployment mudar a versão e aplicar novamente caso queira voltar para uma versão anterior. O deployment guarda um histórico do que foi feito vc tem a chance de voltar com um comando simples chamado `rollout`.

````bash
❯ k rollout history deployment nginx 
deployment.apps/nginx 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>

# o comando undo sem parametro volta para o anterior
# undo --to-revision=1 daria no mesmo 
❯ k rollout undo deployment nginx 
deployment.apps/nginx rolled back

❯ k get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-59f8b7ff5f-7jrk6   1/1     Running   0          10s
nginx-59f8b7ff5f-8sbbm   1/1     Running   0          10s
nginx-59f8b7ff5f-ppjwc   1/1     Running   0          12s
nginx-59f8b7ff5f-rdd2g   1/1     Running   0          12s
nginx-59f8b7ff5f-sgshv   1/1     Running   0          12s

❯ k describe pod nginx-59f8b7ff5f-ppjwc | grep Image 
    Image:          nginx
    Image ID:       docker.io/library/nginx@sha256:b95a99feebf7797479e0c5eb5ec0bdfa5d9f504bc94da550c2f58e839ea6914f

❯ k get rs
NAME               DESIRED   CURRENT   READY   AGE
nginx-59f8b7ff5f   5         5         5       15m
nginx-68df9766d4   0         0         0       11m

# Por default se não for definido quanto ele vai guardar no histórico ele sempre guarda o ultimo e o atual.
❯ k rollout history deployment nginx             
deployment.apps/nginx 
REVISION  CHANGE-CAUSE
2         <none>
3         <none>

❯ k rollout undo deployment nginx --to-revision=2
deployment.apps/nginx rolled back

❯ k rollout history deployment nginx             
deployment.apps/nginx 
REVISION  CHANGE-CAUSE
3         <none>
4         <none>

❯ k get rs                                          
NAME               DESIRED   CURRENT   READY   AGE
nginx-59f8b7ff5f   0         0         0       36m
nginx-68df9766d4   5         5         5       32m
````

> Um replicaset sem um deployment não pode sofrer rollout, mas um daemonset sim. Isso fica só de curiosidade, mas tente manter mesmo um daemonset sendo criado por um deployment.

Volte ao estudo de replicaset e agora finalize o topico extra que tem la dentro para entender um pequeno detalhe entre replicaset e daemonset

Descrevendo o nosso deployment podemos analisar algumas coisas

````bash
❯ k describe deploy nginx  
Name:                   nginx
Namespace:              production
CreationTimestamp:      Mon, 05 Sep 2022 14:10:51 -0300
Labels:                 app=nginx
#numero da revisao
Annotations:            deployment.kubernetes.io/revision: 4
Selector:               app=nginx-pod
Replicas:               5 desired | 5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
# Estratégica de rolling update é de quantos em quantos morre para subir novos
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-pod
  Containers:
   nginx:
    Image:        nginx:stable
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-68df9766d4 (5/5 replicas created)
# Os eventos quea conteceram
Events:
  Type    Reason             Age                From                   Message
  ----    ------             ----               ----                   -------
  Normal  ScalingReplicaSet  49m                deployment-controller  Scaled up replica set nginx-59f8b7ff5f to 5
  Normal  ScalingReplicaSet  35m (x9 over 35m)  deployment-controller  (combined from similar events): Scaled down replica set nginx-68df9766d4 to 0
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled up replica set nginx-68df9766d4 to 2
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled down replica set nginx-59f8b7ff5f to 4
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled up replica set nginx-68df9766d4 to 3
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled down replica set nginx-59f8b7ff5f to 3
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled up replica set nginx-68df9766d4 to 4
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled down replica set nginx-59f8b7ff5f to 1
  Normal  ScalingReplicaSet  13m (x2 over 45m)  deployment-controller  Scaled up replica set nginx-68df9766d4 to 5
  Normal  ScalingReplicaSet  13m                deployment-controller  Scaled down replica set nginx-59f8b7ff5f to 2
  Normal  ScalingReplicaSet  13m (x2 over 44m)  deployment-controller  Scaled down replica set nginx-59f8b7ff5f to 0

````

>Um deployment não consegue ser apontado de fora do cluster. Quem faz o link para o deployment é o service.


## Deploy Avançado

Vamos aproveitar o deployment básico para criar um deployment com limite de recursos e outros parâmetros mais avançados. Aproveitei essa saída e comentei o que não é necessário e dentro desse mesmo manifesto vou explicar alguns novos parâmetros. 

> É boa prática limitar os recursos dos pods por questões de segurança, uso indefido de recursos e outros coisas.

````bash
# Criando um novo deployment baseado no que esta provisionado e recursos default (que existem mesmo não sendo declarados) vão aparecer para ser explicados.
k get deploy nginx -o yaml > fulldeployment.yaml

# Deletando o deployment anterior para criar um novo.
❯ k delete -f deployment.yaml 
deployment.apps "nginx" deleted

````

A implantação de um deployment pode ficar travada por algumas razões.

- Cota insuficiente
- Erro para conseguir a imagem
- Falta de permissão
- Intevalo de limites
- Configuração incorreta para o tempo de execução do app

Para evitar que vc fique toda vida esperando, existe um parâmetro chamado `progressDeadlineSeconds` que por default é 600 segundos (10 minutos) caso não for declarado, e este muda o status do deployment caso não tenha progresso por alguma das razões acima. Esse prazo não é levado em consideração caso o deployment tenha sido concluído com sucesso.

Outro parâmetro que aparece é o `revisionHistoryLimit` que tem o valor default de 10. Isso significa que ele guarda irá guardar até 10 replicaset para ter a chance de fazer rollout (rollback).

Um outro ponto importante a se prestar atenção no deployment é o bloco `strategy` que definirá como deve ser feito um update do seu deployment

https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/

A estratégia rollingUpdate  é a implantação padrão do k8s ele substitui os pods um por um da versão anterior para a nova sem tempo de inatividade, porém essa implantação é feita lentamente. Também é possível configurar alguns parâmetros que podem adiantar, mas em troca de utilização de mais recurso.

![RollingUpdate](./rolling-deployment.jpg)

````yaml
  strategy:
    rollingUpdate:
      # Quantos pods podem ser criados acima da quantidade desejada durante uma atualização. 25% é o padrão ou pode ser definido um valor absoluto.
      maxSurge: 25%
      # Quantos pods podem ficar indisponíveis durnate o processo de update. 25% é o padrão ou pode ser definido um valor absoluto.
      maxUnavailable: 25%
    type: RollingUpdate
````

Essa estratégia é interessante pois mantém a aplicação ativa não necessitando muito recurso, em troca de um tempo maior. Em ambientes muito grande é interessante que seja assim, pois se tivesse que subir todos os pods para poder baixar os outros precisaria do dobro de recurso. Vamos fazer uns teste para ver o que acontecer com os endpoints do service.

Um outro método de implantação é o `Recreate` que primeiramente mata todos os pods para depois subir novos. Para apps que não podem sofrer downtime não é uma boa stratégia, mas talvez para um ambien te desenvolvimento pode ser uma opção.

![Recreate](./recreate-deployment.jpg)

````yaml
  strategy:
    type: Recreate
````

> Existem outras estratégias com a blue-green e a canary existem, mas não são definidas dentro do deployment, mas através de um controle um pouco mais manual de deployments e services.

Um outro parâmetro, mas agora pertendente a parte de template é o `imagePullPolicy`. Por default o parâmetro é dado como **IfNotPresent** e somente vai baixar a imagem caso não tenha ela localmente no node. Se uma imagem tem a tag latest e a nova imagem latest deve ser baixada ele não fará, pois tem as mesmas tags. Para evitar isso é bom utilizar **Always**.

Uma parte muito importante é definir o `resources` de cada um dos containers do pod (Geralemnte é 1 container). Existe o parâmetro **requests** que é recurso garantido que o kubernetes irá disponibilizar para o container e o parâmetro **limits** que é o máximo que ele pode chegar. 

> Sempre que o kubernetes precisar schedular um container ele vai levar em consideração o **requests** para ver se cabe mais um pod.

````bash
template:
    metadata:
      labels:
        app: nginx-pod
    spec:
      containers:
      - image: nginx:stable
        imagePullPolicy: Always
        name: nginx
        ports:
        - containerPort: 80
          name: web
          protocol: TCP
        resources:
          requests:
            memory: 256Mi #Valor inteiro de memória
            cpu: 250m # isso eh 1/4 de cpu
          limits:
            memory: 512Mi
            cpu: 500m # 1/2 cpu
````

Ainda temos outros parâmetros no template do pod que é o `dnsPolicy` que tem o valor default **ClusterFirst** assumindo que primeiro ele deve procurar o dns no cluster para depois ir pra fora.  Quando setado o dnsPolity para **default** ele herda a configuração de resolução de dns do node que o pod esta rodando. Ainda é possível especificar uma política, mas é pouco util no começo <https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/>.

Outro parâmetro do pod é o `restartPolicy` que é o método de reinicialização dele. Se setado como **Always** (default) ele sempre reinicia independente se terminou de forma correta ou não. Se setado como **OnFailure** ele somente irá reiniciar o pod caso ternha terminado forçadamente. **Never** nunca reiniciar o pod.


Um parâmetro a se observar é o `schedulerName:` default-scheduler. Esse é o nome do scheduler padrão do kuberntes que irá pontudar para qual node um pod irá subir. É possível ter mais de um scheduler, mas já estaremos falando de um nível mais avançado ainda de k8s. Por hora não defina esse recurso que ele já irá buscar o padrão.

O kubernetes precisa matar container, seja por que eles pararam de responder ou até mesmo para subir uma novo pod mais atualizado com a ultima versão. Para isso ele envia um signal de termino SIGTERM para o container e esse tem um período de tempo para finalizar com segurança antes que ele faça o SIGKILL e mate tudo. Para isso vc pode definir um tempo de espera chamado `terminationGracePeriodSeconds`. Por default é 30 segundos, mas pde ser ajustado de acordo com a necessidade.

````bash
❯ k apply -f fulldeployment.yaml 
deployment.apps/nginx created

❯ k describe deploy nginx     
Name:                   nginx
Namespace:              production
CreationTimestamp:      Tue, 13 Sep 2022 12:18:10 -0300
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx-pod
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-pod
  Containers:
   nginx:
    Image:      nginx:stable
    Port:       80/TCP
    Host Port:  0/TCP
    Limits:
      cpu:     500m
      memory:  512Mi
    Requests:
      cpu:        250m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-6f9ccf5886 (3/3 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  8m55s  deployment-controller  Scaled up replica set nginx-6f9ccf5886 to 3
````

Qualquer recurso é possível editar a quente com o `kubectl edit <recurso>`