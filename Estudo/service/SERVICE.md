# Service (alias svc)

Service é a camada que expõe o nosso deployment para o mundo ou redireciona os pacotes para os devidos deployments. O service também atua como load balancer escolhendo para qual pod do deployment ele irá mandar a requisição.

Quando criamos um service para expor um deployment automaticamente ele cria os endpoints para cada um dos pods daquele deployment. Nesse caso será aqueles que ele encontrar no seu selector.

````bash
❯ k get deploy
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   5/5     5            5           2m17s

❯ k describe deployment nginx   
Name:                   nginx
Namespace:              production
CreationTimestamp:      Mon, 05 Sep 2022 16:52:17 -0300
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision: 1
# Olha o selector para o app=nginx-pod que é uma label dos pod
Selector:               app=nginx-pod
Replicas:               5 desired | 5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-pod
  Containers:
   nginx:
    Image:        nginx:stable
    # ESSE POD USA A PORTA 80
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-68df9766d4 (5/5 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  10m   deployment-controller  Scaled up replica set nginx-68df9766d4 to 5

❯ k get endpoints
No resources found in production namespace.

# Quando quisermos expor esse deployment ele automaticamente cria um cluster IP e os Endpoints com a porta utilizada pelo deployment para todos os pods
❯ k expose deployment nginx  
service/nginx exposed

❯ k get svc
NAME    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
nginx   ClusterIP   172.20.254.132   <none>        80/TCP    12m

❯ k describe service nginx 
Name:              nginx
Namespace:         production
Labels:            app=nginx
Annotations:       <none>
# Observe o selector abaixo e vamos 
Selector:          app=nginx-pod
# O tipo que é cluster IP
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
# O ip ou range de IP
IP:                172.20.254.132
IPs:               172.20.254.132
Port:              <unset>  80/TCP
# A porta de destino
TargetPort:        80/TCP
# E todo mundo que responderá (o ip de cada pod)
Endpoints:         10.10.67.173:80,10.10.69.14:80,10.10.78.255:80 + 2 more...
Session Affinity:  None
Events:            <none>
❯ k get endpoints
NAME    ENDPOINTS                                                    AGE
nginx   10.10.67.173:80,10.10.69.14:80,10.10.78.255:80 + 2 more...   8s

❯ k describe endpoints nginx 
Name:         nginx
Namespace:    production
Labels:       app=nginx
Annotations:  endpoints.kubernetes.io/last-change-trigger-time: 2022-09-05T19:55:06Z
Subsets:
  Addresses:          10.10.67.173,10.10.69.14,10.10.78.255,10.10.85.161,10.10.93.190
  NotReadyAddresses:  <none>
  Ports:
    Name     Port  Protocol
    ----     ----  --------
    <unset>  80    TCP

Events:  <none>
````

Existem diferentes tipos de services que podemos falar abaixo

> Dentro do cluster o nome do service é o dns dele. Logo dentro de uma outra aplicação no cluster vc poderia chamar simplesmente <http://nginx> que iria resolver. Dessa forma costume nomear o services com o nome da aplicação para ser facil lembrar.

## ClusterIP

Todo service sempre terá um cluster IP. Quando vc quiser bater naquele deployment ele irá escutar pelo cluster IP e redirecionará como um load balancer para algum dos endpoints que são ips dos pods.

Se analisar a saída abaixo vai ver que ele ainda não consegue receber requisições externas fora do cluster pois não tem um IP Externo, mas já será falado sobre isso mais pra frente.

````bash
❯ k get svc
NAME    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
nginx   ClusterIP   172.20.254.132   <none>        80/TCP    12m
````

A partir do momento que temos um service, através do name dado ao svc ele já responderá. Não é necessário passar o ip para fazer requisição ao serviço dentro do cluster, somente o nome. O kubernetes possui um dns resolver chamado coredns.

>vale lembrar que um cluster que em alguns k8s como por exemplo no eks é necessário a instação desse addon durante a criação do cluster. Esses k8s locais para desenvolvimento já vem instalado por padrão.

````bash
❯ k get deploy --all-namespaces
NAMESPACE     NAME      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   coredns   2/2     2            2           11d
production    nginx     5/5     5            5           38m
````

Não é necessário dentro do cluster por exemplo passar o ip para o service, somente o nome.

Desse jeito fizemos somente via cli, mas ele cria um objeto certo? Podemos gerar uma saída em formato yaml, ou json no k8s utilizando o parâmetro -o.

````bash
k get svc nginx -o yaml > service.yaml
````

Essa saída tem toda a específicação do service nginx, e outras informações que são definidas depois que o objeto é criado. Essa é uma maneira fácil de aproveitar um objeto para criar outro.

Vamos fazer uma cópia desse service para outro que iremos trabalhar, só afim de preservar a saída anterior.

````bash
cp service.yaml serviceclusterip.yaml
````

Agora faça um estudo lendo os comentários dentro do [serviceclusterip.yaml](./serviceclusterip.yaml), pois já tem comentários que mostram cada um dos parâmtros.

Vos deletar o service criado através da cli e vamos criá-lo com o yaml.

````bash
❯ k delete svc nginx
service "nginx" deleted

❯ k apply -f serviceclusterip.yaml 
service/nginx created

# Veja que deu no mesmo
❯ k get svc nginx -o yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"nginx"},"name":"nginx","namespace":"production"},"spec":{"ports":[{"port":80,"protocol":"TCP","targetPort":80}],"selector":{"app":"nginx-pod"}}}
  creationTimestamp: "2022-09-12T15:11:42Z"
  labels:
    app: nginx
  name: nginx
  namespace: production
  resourceVersion: "3364633"
  uid: 935593eb-9d29-4c36-9818-81f2d3cea467
spec:
  clusterIP: 172.20.176.148
  clusterIPs:
  - 172.20.176.148
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-pod
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}

k❯ k delete -f serviceclusterip.yaml 
service "nginx" deleted
````

## NodePort

O nodeport funciona abrindo uma porta em para receber requests externas nessa porta e redirecionar para o clusterip. As portas que podem ser usadas pelo nodeport são de 30000 até 32768. Esse recurso é feito quando não se tem um loadbalancer por exemplo e precisa expor uma porta do node.

> Abre-se a mesma porta em todos os nodes redirecionando para o cluster ip que contem os endpoints dos pods> Se não fosse dessa maneira não poderíamos acessar os pods em outros nodes. Qualquer pessoa que conheça o ip de um dos nodes do seu cluster, conseguirá ter acesso a este service.

observe que **Um nodeport tem um clusterip**.

Da mesma maneira é possível criar um service via cli passando o tipo.

````bash
❯ k expose deployment nginx --type=NodePort
service/nginx exposed

❯ k describe svc nginx  
Name:                     nginx
Namespace:                production
Labels:                   app=nginx
Annotations:              <none>
Selector:                 app=nginx-pod
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       172.20.22.196
IPs:                      172.20.22.196
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  31488/TCP
Endpoints:                10.10.68.88:80,10.10.76.186:80,10.10.86.93:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

❯ k get svc nginx -o yaml           
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2022-09-12T15:22:20Z"
  labels:
    app: nginx
  name: nginx
  namespace: production
  resourceVersion: "3366040"
  uid: 71eb1a27-3990-40d3-94da-fc76f34774a0
spec:
  # Observe que mesmo sendo nodeport ele tem um clusterIP
  clusterIP: 172.20.22.196
  clusterIPs:
  - 172.20.22.196
  externalTrafficPolicy: Cluster
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - nodePort: 31488
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-pod
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
...

#Compare e 
❯ k get svc
NAME    TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
nginx   NodePort   172.20.22.196   <none>        80:31488/TCP   22m
````

O que mudou?

````yaml
  ports:
  - nodePort: 31488 # Isso. Porta do nodeport que será direcionada para o clusterIP na porta abaixo. Se vc não definir uma porta ele criará uma randomicamente entre 3000 e 32768
    port: 80 # Porta do clusterIP
    protocol: TCP
    targetPort: 80 # Porta do Pod
  selector:
    app: nginx-pod
  sessionAffinity: None
  type: NodePort #Isso
````

Em tese se o node tem um ip público e vc baster no ippublico:31488 chegará até os pods que respondem pelo nginx.

da mesma maneira vamos aplicar isso usando manifestos yaml.

````bash
❯ k delete svc nginx
service "nginx" deleted

❯ k apply -f servicenodeport.yaml 
service/nginx created

#Nesse caso definimos o nodeport para 30000
❯ k get svc nginx
NAME    TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
nginx   NodePort   172.20.214.7   <none>        80:30000/TCP   38s

❯ k delete -f servicenodeport.yaml 
service "nginx" deleted
````

## LoadBalancer

O loadbalancer é utilizado principalmente no mundo de cloud, pois ele registra um ip externo ao seu nodeport para que vc possa acessar diretamente o seu service.

>Um loadbalancer tem um nodeport que tem um clusterip.

````bash
❯ k apply -f serviceloadbalancer.yaml 
service/nginx created

❯ k get svc
NAME    TYPE           CLUSTER-IP      EXTERNAL-IP                                                               PORT(S)        AGE
nginx   LoadBalancer   172.20.106.36   a4f28c021b96e49de8c5605e7e9c2891-1971215634.us-east-1.elb.amazonaws.com   80:30010/TCP   28s

❯ k get svc nginx -o yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"name":"nginx","namespace":"production"},"spec":{"ports":[{"name":"nginx","port":80,"protocol":"TCP","targetPort":80}],"selector":{"app":"nginx-pod"},"type":"LoadBalancer"}}
  creationTimestamp: "2022-09-12T16:58:50Z"
  finalizers:
  - service.kubernetes.io/load-balancer-cleanup
  name: nginx
  namespace: production
  resourceVersion: "3378796"
  uid: 4f28c021-b96e-49de-8c56-05e7e9c28919
spec:
  allocateLoadBalancerNodePorts: true
  clusterIP: 172.20.106.36
  clusterIPs:
  - 172.20.106.36
  externalTrafficPolicy: Cluster
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: nginx
    nodePort: 30010
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-pod
  sessionAffinity: None
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
    - hostname: a4f28c021b96e49de8c5605e7e9c2891-1971215634.us-east-1.elb.amazonaws.com

❯ k describe svc nginx
Name:                     nginx
Namespace:                production
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx-pod
Type:                     LoadBalancer
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       172.20.170.72
IPs:                      172.20.170.72
LoadBalancer Ingress:     a82b7e00064364e58a2534d6a0e9d784-1065721274.us-east-1.elb.amazonaws.com
Port:                     nginx  80/TCP
TargetPort:               80/TCP
NodePort:                 nginx  30001/TCP
# Da mesma forma tem os endpoints
Endpoints:                10.10.68.88:80,10.10.76.186:80,10.10.86.93:80
Session Affinity:         None
External Traffic Policy:  Cluster
# Esses eventos foram eu testando coisas só para ver os limites e os erros
Events:
  Type     Reason                  Age   From                Message
  ----     ------                  ----  ----                -------
  Warning  SyncLoadBalancerFailed  11m   service-controller  Error syncing load balancer: failed to ensure load balancer: ValidationError: Listeners cannot be empty
           status code: 400, request id: a86d0857-2a9c-4d0c-a158-bd7531ccb59b
  Warning  SyncLoadBalancerFailed  11m  service-controller  Error syncing load balancer: failed to ensure load balancer: ValidationError: Listeners cannot be empty
           status code: 400, request id: a660bce4-7e91-4da3-9a50-8531cfcafa95
  Warning  SyncLoadBalancerFailed  11m  service-controller  Error syncing load balancer: failed to ensure load balancer: ValidationError: Listeners cannot be empty
           status code: 400, request id: d6f589c1-e75a-4cdb-88d7-16f860617f71
  Warning  SyncLoadBalancerFailed  11m  service-controller  Error syncing load balancer: failed to ensure load balancer: ValidationError: Listeners cannot be empty
           status code: 400, request id: 1180411a-f88b-47c7-a919-b01ac2d73730
  Warning  SyncLoadBalancerFailed  10m  service-controller  Error syncing load balancer: failed to ensure load balancer: ValidationError: Listeners cannot be empty
           status code: 400, request id: c58a6c7d-6bda-43f2-9df5-25b0edb3dcbc
  Normal   EnsuringLoadBalancer  9m20s (x7 over 11m)  service-controller  Ensuring load balancer
  Normal   EnsuredLoadBalancer   9m19s (x2 over 10m)  service-controller  Ensured load balancer
  # Ao fim voltou tudo certo
````

### Dica

Procure separar os arquivos de deployment e services para ficar mais organizado do que declarar tudo junto no mesmo.
