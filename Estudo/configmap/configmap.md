# configmap (alias cm)

<https://kubernetes.io/pt-br/docs/concepts/configuration/configmap/>

O configmap é muito parecido com o secret, porém não faz a encriptação com base64, apesar de que isso não vale muita coisa. Um ConfigMap nada mais é do que um par chave/valor. Dados não confidenciais de uma loja ConfigMap, o que significa que não há senhas ou chaves de API. Os ConfigMaps são usados ​​para:

Variáveis ​​de ambiente do Kubernetes ConfigMap
Argumentos de linha de comando do Kubernetes
Arquivos de configuração em um volume

A ideia com um ConfigMap é que você deseja a capacidade de passar informações em tempo de execução para seu aplicativo com a capacidade de manter um aplicativo portátil. Se estiver usando um manifesto do Kubernetes que contém uma deployment ou um service, convém usar a imagem que você está chamando em vários ambientes. Com o ConfigMaps, você tem a capacidade de criar configurações específicas do ambiente em vez de criar um manifesto do Kubernetes para cada ambiente e inserir os valores estaticamente.

Vamos criar inicialmente pelo cli. Em um unico comando foi mostrado como criar literal e de um arquivo.

````bash
❯ k create configmap cores-frutas --from-literal uva=roxa --from-file=predileta --from-file=frutas
configmap/cores-frutas created


❯ k describe cm cores-frutas                                                                            
Name:         cores-frutas
Namespace:    production
Labels:       <none>
Annotations:  <none>

Data
====
predileta:
----
laranja
uva:
----
roxa
banana:
----
amarela
limao:
----
verde
melancia:
----
verde e vermelho
morango:
----
vermelho

BinaryData
====

Events:  <none>

# que daria no mesmo se fosse criado esse yaml 
❯ k get cm cores-frutas -o yaml
apiVersion: v1
data:
  banana: amarela
  limao: verde
  melancia: verde e vermelho
  morango: vermelho
  predileta: laranja
  uva: roxa
kind: ConfigMap
metadata:
  creationTimestamp: "2022-09-20T19:07:22Z"
  name: cores-frutas
  namespace: production
  resourceVersion: "4954820"
  uid: dbdf61fe-5ccd-4b81-8836-7b6d0c31f4ca
````

Não vou perder tempo aplicando o yaml [configmap.yaml](./configmap.yaml)

Do mesmo modo que a secret podemos pegar um só valor de alguma das chaves do configmap...
Analise o yaml [podconfigmap.yaml](./podconfigmap.yaml) e veja como fazer. Até aqui muito parecido com o secrets

````bash
❯ k create -f podconfigmap.yaml 
pod/test-configmap created

❯ k exec -ti test-configmap -- sh
/ # echo $fruta
laranja
/ # 

❯ k delete pod test-configmap 
pod "test-configmap" deleted
````

Agora vamos fazer um sistema que vai todas as variaveis de uma unica vez

````bash
❯ k create -f podconfigmapall.yaml 
pod/test-configmap created

~/pessoais/study-kubernetes/Estudo/configmap develop ⇡1 !6 ?6 ⎈ us-east-1-prod-cluster/production  1.2.9 16:23:11
❯ k exec -ti test-configmap -- sh
/ # set
HISTFILE='/root/.ash_history'
HOME='/root'
HOSTNAME='test-configmap'
IFS=' 
'
KUBERNETES_PORT='tcp://172.20.0.1:443'
KUBERNETES_PORT_443_TCP='tcp://172.20.0.1:443'
KUBERNETES_PORT_443_TCP_ADDR='172.20.0.1'
KUBERNETES_PORT_443_TCP_PORT='443'
KUBERNETES_PORT_443_TCP_PROTO='tcp'
KUBERNETES_SERVICE_HOST='172.20.0.1'
KUBERNETES_SERVICE_PORT='443'
KUBERNETES_SERVICE_PORT_HTTPS='443'
LINENO=''
NGINX_PORT='tcp://172.20.82.68:80'
NGINX_PORT_80_TCP='tcp://172.20.82.68:80'
NGINX_PORT_80_TCP_ADDR='172.20.82.68'
NGINX_PORT_80_TCP_PORT='80'
NGINX_PORT_80_TCP_PROTO='tcp'
NGINX_SERVICE_HOST='172.20.82.68'
NGINX_SERVICE_PORT='80'
NGINX_SERVICE_PORT_NGINX='80'
OPTIND='1'
PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
PPID='0'
PS1='\w \$ '
PS2='> '
PS4='+ '
PWD='/'
SHLVL='1'
TERM='xterm'
banana='amarela'
limao='verde'
melancia='verde e vermelho'
morango='vermelho'
predileta='laranja'
uva='roxa'
````

Do mesmo modo que secret tb podemos montar volume com os arquivos e os valores dentro. Analise o yaml [podconfigmapvol.yaml](./podconfigmapvol.yaml) e faça uma comparação com o secret

````bash
❯ k create -f podconfigmapvol.yaml 
pod/test-configmap created

❯ k exec -ti test-configmap -- sh
/ # cd /app
/app # ls -lha
total 0      
drwxrwxrwx    3 root     root         146 Sep 21 17:58 .
drwxr-xr-x    1 root     root          74 Sep 21 17:58 ..
drwxr-xr-x    2 root     root          92 Sep 21 17:58 ..2022_09_21_17_58_36.2281126371
lrwxrwxrwx    1 root     root          32 Sep 21 17:58 ..data -> ..2022_09_21_17_58_36.2281126371
lrwxrwxrwx    1 root     root          13 Sep 21 17:58 banana -> ..data/banana
lrwxrwxrwx    1 root     root          12 Sep 21 17:58 limao -> ..data/limao
lrwxrwxrwx    1 root     root          15 Sep 21 17:58 melancia -> ..data/melancia
lrwxrwxrwx    1 root     root          14 Sep 21 17:58 morango -> ..data/morango
lrwxrwxrwx    1 root     root          16 Sep 21 17:58 predileta -> ..data/predileta
lrwxrwxrwx    1 root     root          10 Sep 21 17:58 uva -> ..data/uva
/app # cat predileta 
laranja/app # 

/app # cd ..2022_09_21_17_58_36.2281126371/
/app/..2022_09_21_17_58_36.2281126371 # ls -lha
total 24K    
drwxr-xr-x    2 root     root          92 Sep 21 17:58 .
drwxrwxrwx    3 root     root         146 Sep 21 17:58 ..
-rw-r--r--    1 root     root           7 Sep 21 17:58 banana
-rw-r--r--    1 root     root           6 Sep 21 17:58 limao
-rw-r--r--    1 root     root          16 Sep 21 17:58 melancia
-rw-r--r--    1 root     root           8 Sep 21 17:58 morango
-rw-r--r--    1 root     root           7 Sep 21 17:58 predileta
-rw-r--r--    1 root     root           4 Sep 21 17:58 uva
/app/..2022_09_21_17_58_36.2281126371 # cd ..
/app/..2022_09_21_17_58_36.2281126371 # cat uva
roxa/app/..2022_09_21_17_58_36.2281126371 # 
/app/..2022_09_21_17_58_36.2281126371 # echo -n preta > uva
sh: cant create uva: Read-only file system

❯ k delete cm cores-frutas     
configmap "cores-frutas" deleted
````

Observe por curiosidade que ele criou um link simbolico para a pasta .2022_09_21_17_58_36.2281126371 e os arquivos ali dentro são somente leitura.

> se vc fizer uma mudanca no confimap depois que o container já está rodando, isso reflete no container. Essa é uma das vantagens do configmap, mudar uma configuração em tempo de execução. O inverso não é possível, mudar uma arquivo dentro do container para refletir fora.
